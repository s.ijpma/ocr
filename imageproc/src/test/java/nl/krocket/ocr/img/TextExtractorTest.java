package nl.krocket.ocr.img;

import java.util.Locale;
import java.util.ResourceBundle;

import org.junit.Before;
import org.junit.Test;

public class TextExtractorTest {
	
	@Before
	public void init() {
		
	}
	
	@Test
	public void testProcess() throws Exception {
		TextExtractor ta = new TextExtractor();
		String fileName = "totaal_real.jpg";
		String path = ResourceBundle.getBundle("application", Locale.getDefault()).getString("ocr.config.testdata.path");

		String output_file = "totaal_real_out.jpg";
		ta.process(path, fileName, output_file);
	}

}
