package nl.krocket.ocr.img;

import static org.bytedeco.javacpp.opencv_core.CV_16U;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CHAIN_APPROX_NONE;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RETR_LIST;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.boundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.findContours;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;
import static org.bytedeco.javacpp.opencv_photo.fastNlMeansDenoising;

import org.apache.log4j.Logger;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.Scalar;

public class Borderizer {
	private static final Logger LOGGER = Logger.getLogger(Gradient.class.getCanonicalName());
	private static final boolean DEBUG = true;
	
	public static void process(String inputFile, String outputFile) {
		Mat src = imread(inputFile);
		if ( src.empty() ) { 
			throw new ResizeException("Empty image! " + inputFile);
		}
		
		
		//Grayscale matrix
		Mat greyScale = new Mat (src.size(), CV_16U);
		
		//Convert BGR to Gray
		cvtColor(src, greyScale, COLOR_BGR2GRAY);

	  //Binary image
	  		Mat binary = new Mat(greyScale.size(), greyScale.type());
	  		
	  		threshold(greyScale, binary, 142, 255, THRESH_BINARY);
	  		
	  		Mat denoise = new Mat();
	  		fastNlMeansDenoising(binary, denoise, 3, 7, 21);
		
		MatVector contours = new MatVector();
		Mat hierarchy = new Mat();
		
		findContours(denoise, contours, hierarchy, CV_RETR_LIST, CHAIN_APPROX_NONE);
		
		Rect biggest=null;
		for (int i=0; i < contours.size(); i++) {
			if (DEBUG) {
                LOGGER.info("Processing #" + i);
        	}
			//get rectangle bounding contour
        	Mat contour = contours.get(i);
        	Rect rect = boundingRect(contour);
        	if (biggest==null) {
        		biggest = rect;
        	}
        	else {
        		if (DEBUG) {
                    LOGGER.info("Height contour: " + rect.height() + " Width contour: " + rect.width());
            	}
        		if (rect.height() >= biggest.height() && rect.width() >= biggest.width()) {
        			biggest = rect;
            	}
        	}
        	rectangle(src,new Rect(rect),new Scalar(255,0,255,0));
		}
		//draw rectangle around contour on original image
        rectangle(src,new Rect(biggest),new Scalar(255,0,255,0));
        
        imwrite(outputFile, src);
	}

}
