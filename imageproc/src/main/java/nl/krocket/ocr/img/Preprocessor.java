package nl.krocket.ocr.img;

import static org.bytedeco.javacpp.opencv_core.CV_8UC1;
import static org.bytedeco.javacpp.opencv_core.countNonZero;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.CV_CHAIN_APPROX_SIMPLE;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RETR_CCOMP;
import static org.bytedeco.javacpp.opencv_imgproc.MORPH_CLOSE;
import static org.bytedeco.javacpp.opencv_imgproc.MORPH_ELLIPSE;
import static org.bytedeco.javacpp.opencv_imgproc.MORPH_GRADIENT;
import static org.bytedeco.javacpp.opencv_imgproc.MORPH_RECT;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_OTSU;
import static org.bytedeco.javacpp.opencv_imgproc.boundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.drawContours;
import static org.bytedeco.javacpp.opencv_imgproc.findContours;
import static org.bytedeco.javacpp.opencv_imgproc.getStructuringElement;
import static org.bytedeco.javacpp.opencv_imgproc.morphologyEx;
import static org.bytedeco.javacpp.opencv_imgproc.pyrDown;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatExpr;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.Size;

/**
 * From here: http://stackoverflow.com/questions/23506105/extracting-text-opencv
 * @author Sander
 *
 */
public class Preprocessor {

	public static void process(String inputFile, String outputFile) {
		Mat src = imread(inputFile);
		if ( src.empty() ) { 
			throw new ResizeException("Empty image! " + inputFile);
		}
		Mat rgb = new Mat();
	    // downsample and use it for processing
	    pyrDown(src, rgb);
	    
	    Mat small = new Mat();
	    cvtColor(rgb, small, CV_BGR2GRAY);
	    
	    // morphological gradient
	    Mat grad = new Mat();
	    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, new Size(3, 3));
	    morphologyEx(small, grad, MORPH_GRADIENT, morphKernel);
	    
	    // binarize
	    Mat bw = new Mat();
	    threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);
	    // connect horizontally oriented regions
	    Mat connected = new Mat();
	    morphKernel = getStructuringElement(MORPH_RECT, new Size(9, 1));
	    morphologyEx(bw, connected, MORPH_CLOSE, morphKernel);
	    
	 	// find contours
	    MatExpr mask = Mat.zeros(bw.size(), CV_8UC1);
	    MatVector contours = new MatVector();
	    Mat hierarchy = new Mat();
	    findContours(connected, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, new Point(0, 0));
	    // filter contours
	    for(int i=0; i < contours.size(); i++) {
	    	//get rectangle bounding contour
        	Mat contour = contours.get(i);
        	Rect rect = boundingRect(contour);
	        Mat maskROI = new Mat(mask.asMat(), rect);
	        //maskROI = new Scalar(0, 0, 0);
	        // fill the contour
	        drawContours(mask.asMat(), contours, i, new Scalar(255,255,255,0));
	        // ratio of non-zero pixels in the filled region
	        double r = (double)countNonZero(maskROI)/(rect.width()*rect.height());
	        
	        if (r > .45 /* assume at least 45% of the area is filled if it contains text */
	                && 
	                (rect.height() > 8 && rect.width() > 8) /* constraints on region size */
	                /* these two conditions alone are not very robust. better to use something 
	                like the number of significant peaks in a horizontal projection as a third condition */
	                )
	            {
	        	rectangle(src,new Rect(rect),new Scalar(255,0,255,0));
	            }
	    }
	    imwrite(outputFile, src);
	}
}
