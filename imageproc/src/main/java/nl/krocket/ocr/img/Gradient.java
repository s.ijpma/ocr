package nl.krocket.ocr.img;

import static org.bytedeco.javacpp.opencv_core.BORDER_CONSTANT;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CHAIN_APPROX_NONE;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.MORPH_CROSS;
import static org.bytedeco.javacpp.opencv_imgproc.RETR_EXTERNAL;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY_INV;
import static org.bytedeco.javacpp.opencv_imgproc.boundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.dilate;
import static org.bytedeco.javacpp.opencv_imgproc.findContours;
import static org.bytedeco.javacpp.opencv_imgproc.getStructuringElement;
import static org.bytedeco.javacpp.opencv_imgproc.morphologyDefaultBorderValue;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;

import org.apache.log4j.Logger;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.Size;

/**
 * From: http://stackoverflow.com/questions/23506105/extracting-text-opencv
 * @author Sander
 *
 */
public class Gradient {
	
	private static final Logger LOGGER = Logger.getLogger(Gradient.class.getCanonicalName());
	private static final boolean DEBUG = true;

	public static void process(String inputFile, String outputFile) {
		Mat src = imread(inputFile);
		if ( src.empty() ) { 
			throw new ResizeException("Empty image! " + inputFile);
		}
		Mat grey = new Mat();
	    cvtColor(src, grey, CV_BGR2GRAY); //grayscale
	    
	    Mat thresh = new Mat();
	    threshold(grey, thresh, 150, 255, THRESH_BINARY_INV); //threshold
	    
	    Mat kernel = getStructuringElement(MORPH_CROSS, new Size(3, 3));
	    
	    Mat dilated = new Mat();//new Mat(13);
	    dilate(thresh, dilated, kernel, new Point(-1,-1), 13, BORDER_CONSTANT, morphologyDefaultBorderValue());  //dilate
	    
	    Mat hierarchy = new Mat();
        MatVector contours = new MatVector();

        findContours(dilated, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE);
	    
        //for each contour found, draw a rectangle around it on original image
        for (int i=0; i < contours.size(); i++) {
        	if (DEBUG) {
                LOGGER.info("Processing #" + i);
        	}
        	//get rectangle bounding contour
        	Mat contour = contours.get(i);
        	Rect rect = boundingRect(contour);
        	
        	//discard areas that are too large
        	if (rect.height() > 300 && rect.width() > 300) {
        		continue;
        	}
        	//discard areas that are too small
        	if (rect.height() < 40 && rect.width() < 40) {
        		continue;
        	}
        	
        	//draw rectangle around contour on original image
            rectangle(src,new Rect(rect),new Scalar(255,0,255,0));
        	
        }
        
        imwrite(outputFile, src);
	}
}
