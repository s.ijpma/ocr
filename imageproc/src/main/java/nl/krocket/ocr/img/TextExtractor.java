package nl.krocket.ocr.img;

import static org.bytedeco.javacpp.opencv_core.BORDER_CONSTANT;
import static org.bytedeco.javacpp.opencv_core.FONT_HERSHEY_PLAIN;
import static org.bytedeco.javacpp.opencv_core.copyMakeBorder;
import static org.bytedeco.javacpp.opencv_core.merge;
import static org.bytedeco.javacpp.opencv_core.split;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CHAIN_APPROX_NONE;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.Canny;
import static org.bytedeco.javacpp.opencv_imgproc.GaussianBlur;
import static org.bytedeco.javacpp.opencv_imgproc.RETR_TREE;
import static org.bytedeco.javacpp.opencv_imgproc.adaptiveThreshold;
import static org.bytedeco.javacpp.opencv_imgproc.blur;
import static org.bytedeco.javacpp.opencv_imgproc.boundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.findContours;
import static org.bytedeco.javacpp.opencv_imgproc.putText;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.Size;


/**
 * Translated to Java from: https://github.com/jasonlfunk/ocr-text-extraction/blob/master/extract_text
 * 
 * @author Sander
 *
 */
public class TextExtractor {
	
	private static final Logger LOGGER = Logger.getLogger(TextExtractor.class.getCanonicalName());
	private static final boolean DEBUG = true;
	
	private int imgX;
	private int imgY;
	
	private Mat image;
	
	public static void main(String[] argvs) {
		
	}
	public void process(String path, String inputFile, String outputFile) {
		
		// Load the image
		Mat image_orig = imread(path + inputFile);
		if ( image_orig.empty() ) { LOGGER.error("Empty image!");}

		this.image = new Mat();
		//Add a border to the image for processing sake
		copyMakeBorder(image_orig, this.image, 50, 50, 50, 50, BORDER_CONSTANT);

		//# Calculate the width and height of the image

		this.imgY = this.image.arrayHeight();
        this.imgX = this.image.arrayWidth();
        
        if (DEBUG)
            LOGGER.info("Image is " + this.imgY + "x" + this.imgX);
      
        Mat edgesCopy = doCannyEdgeDetection(path, this.image);
       
	    imwrite(path + "edgesCopy.png", edgesCopy);
	    
        //Find the contours        
        Mat hierarchy = new Mat();
        MatVector contours = new MatVector();

        findContours(edgesCopy, contours, hierarchy, RETR_TREE, CHAIN_APPROX_NONE);
        
        Mat processed;
        Mat rejected;
        if (DEBUG) {
        	processed = new Mat();
        	rejected = new Mat();
        	edgesCopy.copyTo(processed);
        	edgesCopy.copyTo(rejected);
        }
        hierarchy = hierarchy.col(0);
        
        //These are the boxes that we are determining
        Map<Mat, Rect> keepers = new HashMap<Mat, Rect>();

        //For each contour, find the bounding rectangle and decide
        //if it's one we care about
        for (int i=0; i < contours.size(); i++) {
        	if (DEBUG) {
                LOGGER.info("Processing #" + i);
        	}
        	Mat contour = contours.get(i);
        	Rect rect = boundingRect(contour);
        	//Check the contour and it's bounding box
        	if (keep(contour) && include_box(i, hierarchy, contour)) {
        		//It's a winner!
        	    keepers.put(contour, rect);
        	    if (DEBUG) {
        	    	rectangle(processed, new Point(rect.x(),rect.y()), new Point(rect.x() + rect.width(), rect.y() + rect.height()), new Scalar(1), 100, 100, 100);
        	    	putText(processed, String.valueOf(i), new Point(rect.x(), rect.y() - 5), FONT_HERSHEY_PLAIN, 0, new Scalar(1));
        	    }
        	}
        	else {
        		if (DEBUG) {
        	    	//rectangle(rejected, new Point(rect.x(),rect.y()), new Point(rect.x() + rect.width(), rect.y() + rect.height()), new Scalar(1), 100, 100, 100);
        	    	putText(rejected, String.valueOf(i), new Point(rect.x(), rect.y() - 5), FONT_HERSHEY_PLAIN, 0, new Scalar(1));
        	    }
        	}
        }
        
        
        //Make a white copy of our image
//        Mat fill_image = new Mat();
//        edgesCopy.copyTo(fill_image);
        //Mat new_image = fill_image.fill(255);
        Mat new_image = new Mat();
        edgesCopy.copyTo(new_image);
        imwrite(path + "new_image.png", new_image);
        //boxes = []
        
        int nr=0;
        //For each box, find the foreground and background intensities
        for (Map.Entry<Mat, Rect> entry : keepers.entrySet()) {
        	//Find the average intensity of the edge pixels to
        	nr++;
            //determine the foreground intensity
            double fg_int = 0.0;
            Mat contour = entry.getKey();
            
            for (int xC=0; xC < contour.arrayWidth();xC++) {
            	for (int yC=0; yC < contour.arrayHeight();yC++) {
            		Mat p = contour.row(xC).col(yC);
            		fg_int += ii(p.row(0).col(0).arrayHeight(), p.row(0).col(1).arrayWidth());
            	}
            }
            fg_int = fg_int/contour.size().capacity();
            if (DEBUG) {
            	LOGGER.info("FG Intensity for #"+nr+" = "+fg_int+"");
            }
            
            //Find the intensity of three pixels going around the
            //outside of each corner of the bounding box to determine
            //the background intensity
            
            Rect box = entry.getValue();
            
            int height = box.height();
            int width = box.width();
            int x_ = box.x();
            int y_ = box.y();
            
            double[] bg_int = {
            		 //bottom left corner 3 pixels
                     ii(x_ - 1, y_ - 1),
                     ii(x_ - 1, y_),
                     ii(x_, y_ - 1),
                     
                     //bottom right corner 3 pixels
                     ii(x_ + width + 1, y_ - 1),
                     ii(x_ + width, y_ - 1),
                     ii(x_ + width + 1, y_),

                     //top left corner 3 pixels
                     ii(x_ - 1, y_ + height + 1),
                     ii(x_ - 1, y_ + height),
                     ii(x_, y_ + height + 1),

                     //top right corner 3 pixels
                     ii(x_ + width + 1, y_ + height + 1),
                     ii(x_ + width, y_ + height + 1),
                     ii(x_ + width + 1, y_ + height)
            };
            
            double median = findMedian(bg_int);
            
            if (DEBUG) {
            	LOGGER.info("BG Intensity for #"+nr+" = "+bg_int+"");
            }
            
            //Determine if the box should be inverted
            int fg;
            int bg;
            if (fg_int >= median) {
                fg = 255;
                bg = 0;
            }
            else {
                fg = 0;
                bg = 255;
            }
            
            //Loop through every pixel in the box and color the
            //pixel accordingly
            for (int x = x_; x < x_ + width; x++) {
            	for (int y = y_; y < y_ + height; y++) {
            		if (y >= imgY || x >= imgX) {
            			if (DEBUG) {
            				LOGGER.info("pixel out of bounds ("+x+","+y+")");
            			}
            			continue;
            		}
            		if (ii(x, y) > fg_int) {
                        new_image.cols(y).row(x).fill(bg);
                    }
                    else {
                    	new_image.cols(y).row(x).fill(fg);
                    }
            	}
            }
        }
        //blur a bit to improve ocr accuracy
        Mat blurred = new Mat();
        blur(new_image, blurred, new Size(2, 2));
        imwrite(path + outputFile, new_image);
        if (DEBUG) {
        	imwrite(path + "edges.png", edgesCopy);
        	imwrite(path + "processed.png", processed);
        	imwrite(path + "rejected.png", rejected);
        }
	}
	private Mat doCannyEdgeDetection(String path, Mat image) {
		//Split out each channel
        Mat red = new Mat();
        Mat green = new Mat();
        Mat blue = new Mat();
        
        MatVector v = new MatVector(red, green, blue);
        split(image, v);
                
        //Run canny edge detection on each channel
        Mat blue_edges = new Mat();
        Canny(v.get(0), blue_edges, 0, 250);
        Mat green_edges = new Mat();
        Canny(v.get(1), green_edges, 0, 250);
        Mat red_edges = new Mat();
        Canny(v.get(2), red_edges, 0, 250);
        
        //Join edges back into image
        Mat edges = new Mat();
        MatVector vEdges = new MatVector(red_edges, green_edges, blue_edges);
        merge(vEdges, edges);
       
        //convert to grayscale
		Mat gray = new Mat();
		cvtColor(edges, gray, COLOR_BGR2GRAY);
		
		Mat blur = new Mat();
		GaussianBlur(gray, blur, new Size(5,5), 0);
		
		Mat edgesCopy = new Mat();
		adaptiveThreshold(blur, edgesCopy, 255,1,1,11,2);
		if (DEBUG) {
			imwrite(path + "blue_edges.png", blue_edges);
			imwrite(path + "green_edges.png", green_edges);
			imwrite(path + "red_edges.png", red_edges);
			imwrite(path + "edges.png", edges);
			imwrite(path + "gray.png", gray);
			imwrite(path + "blur.png", blur);
		}
		return edgesCopy;
	}
	
	/**
	 * Find the median of the background pixels determined above
	 * @param numArray
	 * @return
	 */
	private double findMedian(double[] numArray) {
		Arrays.sort(numArray);
		double median;
		if (numArray.length % 2 == 0)
		    median = ((double)numArray[numArray.length/2] + (double)numArray[numArray.length/2 - 1])/2;
		else
		    median = (double) numArray[numArray.length/2];
		return median;
	}
	
	/**
	 * Determine pixel intensity. Apparently human eyes register colors differently.
	 * TVs use this formula to determine pixel intensity = 0.30R + 0.59G + 0.11B
	 * @param xx
	 * @param yy
	 * @return
	 */
	private double ii(int xx, int yy) {
		if (yy >= this.imgY || xx >= this.imgX) {
			LOGGER.info("pixel out of bounds ("+yy+","+xx+")");
			return 0;
		}
		Mat pixel = this.image.col(yy).row(xx);
		return 0.30 * pixel.checkVector(2) + 0.59 * pixel.checkVector(1) + 0.11 * pixel.checkVector(0);
	}

	private boolean include_box(int i, Mat hierarchy, Mat contour) {
		if (DEBUG) {
	        if (is_child(i, hierarchy)) {
	        	LOGGER.info("\tIs a child") ;
	        	LOGGER.info("\tparent " + get_parent(i, hierarchy) + " has " + 
	                count_children(get_parent(i, hierarchy).cols(), hierarchy, contour) + " children");
	        	LOGGER.info("\thas " + count_children(i, hierarchy, contour) + " children");
	        }
		}
	    if (is_child(i, hierarchy) && count_children(get_parent(i, hierarchy).cols(), hierarchy, contour) <= 2) {
	    	if (DEBUG) {
	    		LOGGER.info("\t skipping: is an interior to a letter");
	    	}
	        return false;
	    }
	    if (count_children(i, hierarchy, contour) > 2) {
	    	if (DEBUG) {
	    		LOGGER.info("\t skipping, is a container of letters");
	    	}
	        return false;
	    }
	    if (DEBUG) {
	    	LOGGER.info("\t keeping");
	    }
	    return true;
	}

	private int count_children(int i, Mat hierarchy, Mat contour) {
		int count = 0;
		//No children
	    if (hierarchy.row(i).col(2).arrayHeight() < 0) {
	        return 0;
	    }
	    else {
	        //If the first child is a contour we care about
	        //then count it, otherwise don't
	        if (keep(hierarchy.row(i).col(2))) {
	            count = 1;
	        }
	        else {
	            count = 0;
	        }
            //Also count all of the child's siblings and their children
            count += count_siblings(hierarchy.row(i).col(2).arrayHeight(), hierarchy, contour, true);
            return count;
	    }
	}
	
	/**
	 * Count the number of relevant siblings of a contour
	 * @param cols
	 * @param hierarchy
	 * @param contour
	 * @param b
	 * @return
	 */
	private int count_siblings(int index, Mat hierarchy, Mat contour, boolean incChildren) {
		int count = 0;
		//Include the children if necessary
	    if (incChildren) {
	        count = count_children(index, hierarchy, contour);
	    }
	    else {
	        count = 0;
	    }    		
	    //Look ahead
	    Mat p_ = hierarchy.row(index).col(0);
	    while (p_.arrayHeight() > 0) {
	        if (keep(p_)) {
	            count += 1;
	        }
	        if (incChildren) {
	            count += count_children(p_.arrayHeight(), hierarchy, contour);
	        }
	        p_ = hierarchy.row(p_.arrayHeight()).col(0);
	    }    		
	    //Look behind
	    Mat n = hierarchy.row(index).col(1);
	    while (p_.arrayHeight() > 0) {
	    	if (keep(n)) {
	            count += 1;
	    	}
	        if (incChildren) {
	            count += count_children(n.arrayHeight(), hierarchy, contour);
	        }
	        n = hierarchy.row(n.arrayHeight()).col(1);
	    }
	    return count;
	}
	/**
	 * Get the first parent of the contour that we care about
	 * @param i
	 * @param hierarchy
	 * @return
	 */
	private Mat get_parent(int i, Mat hierarchy) {
		Mat parent = hierarchy.row(i).col(3);
	    while (!keep(parent) && parent.arrayHeight() > 0) {
	        parent = hierarchy.row(parent.arrayHeight()).col(3);
	    }
	    return parent;
	}
	
	/**
	 * Quick check to test if the contour is a child
	 * @param i
	 * @param hierarchy
	 * @return
	 */
	private boolean is_child(int i, Mat hierarchy) {
		return get_parent(i, hierarchy).arrayHeight() > 0;
	}
	
	/**
	 * Whether we care about this contour
	 * @param contour
	 * @return
	 */
	private boolean keep(Mat contour) {
		return keepBox(contour) && connected(contour);
	}

	/**
	 * Whether we should keep the containing box of this contour based on it's shape
	 * @param contour
	 * @return
	 */
	private boolean keepBox(Mat contour) {
		Rect rect = boundingRect(contour);

	    //width and height need to be floats
	    float width = rect.width();
	    float height = rect.height();
	    LOGGER.info("heigth*width: " + height + "*" + width);
	    //Test it's shape - if it's too oblong or tall it's
	    //probably not a real character
	    if (width / height < 0.1 || width / height > 10) {
	    	if (DEBUG) {
	    		LOGGER.info("\t Rejected because of shape: (" + rect.x() + "," + rect.y() + "," + width + "," + height + ")" + width / height);
	    	}
	        return false;
	    }
	    
	    //check size of the box
	    if (((width * height) > ((this.imgX * this.imgY) / 5)) || ((width * height) < 15)) {
	    	if (DEBUG) {
	    		LOGGER.info("\t Rejected because of size");
	    	}
	        return false;
	    }

	    return true;
	}

	/**
	 * A quick test to check whether the contour is a connected shape
	 * @param contour
	 * @return
	 */
	private static boolean connected(Mat contour) {
		LOGGER.info("Contour: " + contour.arraySize());
		
		Mat first = contour.row(0).col(0);

		LOGGER.info("first: " + first.arrayHeight());
		Mat last = contour.row(contour.arrayHeight()-1).col(0);
		LOGGER.info("last: " + last.arrayHeight());
	    return (first.row(0).arrayHeight() - last.row(0).arrayHeight() <= 1) && (first.row(1).arrayHeight() - last.row(1).arrayHeight() <= 1);
	}

}
