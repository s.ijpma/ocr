package nl.krocket.ocr.img;

import static org.bytedeco.javacpp.opencv_core.CV_32FC1;
import static org.bytedeco.javacpp.opencv_core.CV_8UC1;

import static org.bytedeco.javacpp.opencv_core.meanStdDev;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.resize;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;


import static org.bytedeco.javacpp.opencv_photo.INPAINT_TELEA;
import static org.bytedeco.javacpp.opencv_photo.inpaint;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Range;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.helper.opencv_core.CvArr;


/**
 * Background Surface Thresholding
 * @author Sander
 *
 */
public class BST {
	
	public static void process(String path, String inputFile, String outputFile) {
		Mat image=imread(path + inputFile, 0);
	    Mat res = new Mat();
	    image.convertTo(image,CV_32FC1,1.0/255.0,1.0/255.0);
	    calcBlockMeanVariance(image,res); 
	    
//	    res=1.0-res;
//	    res=image+res;
	    
	    threshold(res,res,0.85,1,THRESH_BINARY);
	    Size s = new Size(res.cols()/2,res.rows()/2);
	    resize(res,res,s);
	    imwrite(path + outputFile,res); //res*255
	}
	
	private static void calcBlockMeanVariance(Mat image, Mat res) {
		int blockSide=21;
		Mat im = new Mat();
	    image.convertTo(im,CV_32FC1);
	    res=Mat.zeros(image.rows()/blockSide,image.cols()/blockSide,CV_32FC1).asMat();
	    Mat inpaintmask = new Mat();
	    Mat patch;
	    Mat smallImg = new Mat();
	    Scalar m = new Scalar();
	    Scalar s = new Scalar();

	    for(int i=0;i<image.rows()-blockSide;i+=blockSide)
	    {       
	        for (int j=0;j<image.cols()-blockSide;j+=blockSide)
	        {
	        	Range r1 = new Range(i,i+blockSide+1);
	        	Range r2 = new Range(j,j+blockSide+1);
	            patch=im.apply(r1,r2);
	            Mat mm = new Mat();
	            mm.put(m);
	            Mat sm = new Mat();
	            sm.put(s);
	            meanStdDev(patch,mm,sm);
	            if(s.get(0)>0.01) // Thresholding parameter (set smaller for lower contrast image)
	            {
	            	res.ptr(i/blockSide, j/blockSide);
	            	res.put(m);
	            }else
	            {
	            	res.ptr(i/blockSide, j/blockSide);
	            	res.put(new Mat(0));
	            }           
	        }
	    }

	    org.bytedeco.javacpp.opencv_imgproc.resize(im,smallImg,res.size());

	    threshold(res,inpaintmask,0.02,1.0,THRESH_BINARY);

	    Mat inpainted = new Mat();
	    smallImg.convertTo(smallImg,CV_8UC1,255.0, 255.0);

	    inpaintmask.convertTo(inpaintmask,CV_8UC1);

	    inpaint(smallImg, inpaintmask, inpainted, 5, INPAINT_TELEA);

	    org.bytedeco.javacpp.opencv_imgproc.resize(inpainted,res,image.size());
	    res.convertTo(res,CV_32FC1,1.0/255.0,1.0/255.0);
	}

}
