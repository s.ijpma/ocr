package nl.krocket.ocr.img;

public class ResizeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8973492360867698515L;

	public ResizeException(String message) {
		super(message);
	}
	
}
