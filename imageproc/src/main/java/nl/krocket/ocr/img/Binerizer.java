package nl.krocket.ocr.img;

import static org.bytedeco.javacpp.opencv_core.CV_16U;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;
import static org.bytedeco.javacpp.opencv_photo.fastNlMeansDenoising;

import org.bytedeco.javacpp.opencv_core.Mat;

public class Binerizer {
	
	public static Mat binerize(Mat src, int trhesh) {
		if ( src.empty() ) { 
			throw new ResizeException("Empty image! ");
		}
		//Grayscale matrix
		Mat greyScale = new Mat (src.size(), CV_16U);
		
		//Convert BGR to Gray
		cvtColor(src, greyScale, COLOR_BGR2GRAY);
		
		//Binary image
		Mat binary = new Mat(greyScale.size(), greyScale.type());
		
		threshold(greyScale, binary, trhesh, 255, THRESH_BINARY);
		
		Mat denoise = new Mat();
		fastNlMeansDenoising(binary, denoise, 3, 7, 21);

		return denoise;
	}

}
