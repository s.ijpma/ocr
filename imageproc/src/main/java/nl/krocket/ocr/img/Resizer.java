package nl.krocket.ocr.img;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import org.apache.log4j.Logger;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Size;

public class Resizer {
	
	private static final Logger LOGGER = Logger.getLogger(Resizer.class.getCanonicalName());
	
	private static final int MAX_WIDTH=960;
	private static final int MAX_HEIGHT=1280;
	
	public static void resize(String path, String resizedPath) throws ResizeException {
		LOGGER.info("Resizing source: " + path + " to " + resizedPath);
		Mat src = imread(path);
		logMatInfo(src, "Source");
		
		Mat resized = resize(src);
		logMatInfo(src, "Resized");
		//write resized image
		imwrite(resizedPath, resized);
	}
	
	public static Mat resize(Mat src) throws ResizeException {
		Size size = new Size(MAX_WIDTH,MAX_HEIGHT);
		Mat dst = new Mat();
		if ( src.empty() ) { 
			throw new ResizeException("Empty image!");
		}
		if (src.arrayHeight() >= MAX_HEIGHT || src.arrayWidth() >= MAX_WIDTH) {
			org.bytedeco.javacpp.opencv_imgproc.resize(src,dst,size); //resize image
			return dst;
		}
		else { //if no resize happened; return src
			return src;
		}
	}
	
	private static void logMatInfo(Mat src, String prefix) {
		int channels = src.arrayChannels();
		int depth = src.arrayDepth();
		int size = src.arraySize();
		int width = src.arrayWidth();
		int height = src.arrayHeight();
		StringBuffer message = new StringBuffer();
		message.append(prefix);
		message.append(" image info:");
		message.append(" channels: ").append(channels);
		message.append(" depth: ").append(depth);
		message.append(" size: ").append(size);
		message.append(" width: ").append(width);
		message.append(" height: ").append(height);
		LOGGER.info(message.toString());
	}

}
