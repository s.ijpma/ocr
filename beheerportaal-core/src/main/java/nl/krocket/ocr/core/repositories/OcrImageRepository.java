package nl.krocket.ocr.core.repositories;

import nl.krocket.ocr.core.model.OcrImage;
import nl.krocket.ocr.core.model.UserInfo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.REQUIRED)  
public interface OcrImageRepository extends CrudRepository<OcrImage, Long>{

	Iterable<OcrImage> findAllByUserInfo(UserInfo user);

}
