package nl.krocket.ocr.core.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ocr_ocr_word")
@XmlRootElement
public class OcrWord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5083854533211827909L;

	@Id
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="ocrWordId_seq")
    @SequenceGenerator(name="ocrWordId_seq", sequenceName="ocrWordId_seq", allocationSize=1)
    @Column(name = "ocrWordId")
    private Long ocrWordId;
	
	@Column(name = "word")
	private String word;
	
	@Column(name = "confidence")
	private Float confidence;
	
	@JsonIgnore
	@JoinColumn(name = "ocrWordIdRef", referencedColumnName = "baseImageId")
    @ManyToOne(optional = false)
    private OcrImage ocrImage;

	
	public OcrImage getOcrImage() {
		return ocrImage;
	}

	public void setOcrImage(OcrImage ocrImage) {
		this.ocrImage = ocrImage;
	}

	public Long getOcrWordId() {
		return ocrWordId;
	}

	public void setOcrWordId(Long ocrWordId) {
		this.ocrWordId = ocrWordId;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Float getConfidence() {
		return confidence;
	}

	public void setConfidence(Float confidence) {
		this.confidence = confidence;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((confidence == null) ? 0 : confidence.hashCode());
		result = prime * result
				+ ((ocrWordId == null) ? 0 : ocrWordId.hashCode());
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OcrWord other = (OcrWord) obj;
		if (confidence == null) {
			if (other.confidence != null)
				return false;
		} else if (!confidence.equals(other.confidence))
			return false;
		if (ocrWordId == null) {
			if (other.ocrWordId != null)
				return false;
		} else if (!ocrWordId.equals(other.ocrWordId))
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OcrWord [ocrWordId=" + ocrWordId + ", word=" + word
				+ ", confidence=" + confidence + "]";
	}
	
	
}
