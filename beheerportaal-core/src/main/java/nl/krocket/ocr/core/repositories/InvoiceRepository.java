package nl.krocket.ocr.core.repositories;

import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.model.UserInfo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.REQUIRED)  
public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

	Iterable<Invoice> findAllByUserInfo(UserInfo user);
	
}
