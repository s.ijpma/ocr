package nl.krocket.ocr.core.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ocr_ocr_image")
@PrimaryKeyJoinColumn(name="baseImageId")
@XmlRootElement
public class OcrImage extends BaseImage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Column(name = "ocrThreshold")
    private Integer ocrThreshold;
	
	@Column(name = "hOcr", columnDefinition="TEXT")
    private String hOcr;
	
	@JsonIgnore
	@Column(name = "ocrText", columnDefinition="TEXT")
    private String ocrText;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "ocrImage", orphanRemoval=true)
    private Set<OcrWord> ocrWords;
	
	public Set<OcrWord> getOcrWords() {
		if (this.ocrWords==null)
			this.ocrWords = new HashSet<OcrWord>();
		return ocrWords;
	}

	public void setOcrWords(Set<OcrWord> ocrWords) {
		if (ocrWords!=null) {
			this.ocrWords = ocrWords;
		}
		else {
			this.ocrWords.clear();
		}
			
	}

	public Integer getOcrThreshold() {
		return ocrThreshold;
	}

	public void setOcrThreshold(Integer ocrThreshold) {
		this.ocrThreshold = ocrThreshold;
	}

	public String gethOcr() {
		return hOcr;
	}

	public void sethOcr(String hOcr) {
		this.hOcr = hOcr;
	}

	public String getOcrText() {
		return ocrText;
	}

	public void setOcrText(String ocrText) {
		this.ocrText = ocrText;
	}
}
