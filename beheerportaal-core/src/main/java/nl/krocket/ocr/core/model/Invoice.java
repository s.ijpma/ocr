package nl.krocket.ocr.core.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import nl.krocket.ocr.core.util.CurrencyUtil;

@Entity
@Table(name = "ocr_invoice")
@XmlRootElement
public class Invoice extends OcrImage implements RequestForPayment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE) //leave company on delete
	private Company fromCompany;
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE) //leave company on delete
	private Company toCompany;
	
	private String invoiceNr;
	
	@Column(name = "vat")
	@Basic(optional = true)
	@Enumerated(EnumType.ORDINAL)
	private Vat vat;
	
	@Column(name = "receiptDate")
    @Temporal(TemporalType.TIMESTAMP)
	@Basic(optional = true)
    private Date receiptDate;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "baseImage", orphanRemoval=true)
    private Set<PaymentRow> paymentRows;
	
	@Transient
	private String total;

	public Company getFromCompany() {
		return fromCompany;
	}

	public void setFromCompany(Company fromCompany) {
		this.fromCompany = fromCompany;
	}

	public Company getToCompany() {
		return toCompany;
	}

	public void setToCompany(Company toCompany) {
		this.toCompany = toCompany;
	}

	public String getInvoiceNr() {
		return invoiceNr;
	}

	public void setInvoiceNr(String invoiceNr) {
		this.invoiceNr = invoiceNr;
	}

	public Vat getVat() {
		return vat;
	}

	public void setVat(Vat vat) {
		this.vat = vat;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public Set<PaymentRow> getPaymentRows() {
		if (null==this.paymentRows) {
			this.paymentRows = new HashSet<PaymentRow>();
		}
		return this.paymentRows;
	}

	public void setPaymentRows(Set<PaymentRow> paymentRows) {
		if (paymentRows != null) {
			this.paymentRows = paymentRows;
		}
		else {
			this.paymentRows.clear();
		}
	}

	public String getTotal() {
		if (null==this.total || this.total.isEmpty()) {
			this.total = CurrencyUtil.getTotalPrice(paymentRows);
		}
		return this.total;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((getBaseImageId() == null) ? 0 : getBaseImageId().hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invoice other = (Invoice) obj;
		if (getBaseImageId() == null) {
			if (other.getBaseImageId() != null)
				return false;
		} else if (!getBaseImageId().equals(other.getBaseImageId()))
			return false;
		return true;
	}

	/**
	 * Determines minimum set of necessary properties for this to be called an invoice
	 * @return
	 */
	public boolean isInvoice() {
		return !this.paymentRows.isEmpty();
	}
	
	
	
}
