package nl.krocket.ocr.core.model;

public enum SupportedCurrency {
	
	EUR("nl-NL", "EUR", "e", "E", "€"),
	USD("en-US", "USD", "$");

	private String languageCode; 
	private String currencyCode;
	private String[] currencyChars;
	
	/**
	 *  
	 * @param languageCode One of the following: http://www.lingoes.net/en/translator/langcode.htm
	 * @param currencyCode One of the following: http://www.xe.com/iso4217.php
	 * @param currencyChars
	 */
	private SupportedCurrency(String languageCode, String currencyCode, String... currencyChars) {
		this.languageCode = languageCode;
		this.currencyCode = currencyCode;
		this.currencyChars = currencyChars;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @return the currencyChars
	 */
	public String[] getCurrencyChars() {
		return currencyChars;
	}
	
	/**
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * @return the default currency
	 */
	public static SupportedCurrency getDefault() {
		return SupportedCurrency.EUR;
	}
	
}
