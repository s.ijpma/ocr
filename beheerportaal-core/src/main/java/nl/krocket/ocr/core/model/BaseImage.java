package nl.krocket.ocr.core.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name = "ocr_base_image")
@XmlRootElement
public class BaseImage implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="baseImageId_seq")
    @SequenceGenerator(name="baseImageId_seq", sequenceName="baseImageId_seq", allocationSize=1)
    @Column(name = "baseImageId")
    private Long baseImageId;
	
	@Column(name = "creationDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
	
	@Column(name = "fileName")
    private String fileName;
	
	@Transient
	private String threshFileName;
	
	@Transient
	private String dateTime;
	
	@Column(name = "fileDirectory")
    private String fileDirectory;
	
	@Column(name = "type")
    private String type;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "ocrUserIdRef", referencedColumnName = "userId")
    private UserInfo userInfo;
	
	@Column(name = "procesStatus")
	@Basic(optional = false)
	@Enumerated(EnumType.ORDINAL)
	private ProcesStatus procesStatus;

	public ProcesStatus getProcesStatus() {
		return procesStatus;
	}

	public void setProcesStatus(ProcesStatus procesStatus) {
		this.procesStatus = procesStatus;
	}
	
	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public Long getBaseImageId() {
		return baseImageId;
	}

	public void setBaseImageId(Long baseImageId) {
		this.baseImageId = baseImageId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	public String getDateTime() {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("Europe/Amsterdam"));
		return format.format(this.creationDate);
	}

	public String getFileName() {
		return fileName;
	}

	/**
	 * @return the threshFileName
	 */
	public String getThreshFileName() {
		if (null == this.threshFileName || this.threshFileName.isEmpty()) {
			this.threshFileName = this.fileName.replace(".jpg", "_thresh.jpg");
		}
		return this.threshFileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileDirectory() {
		return fileDirectory;
	}

	public void setFileDirectory(String fileDirectory) {
		this.fileDirectory = fileDirectory;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((baseImageId == null) ? 0 : baseImageId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseImage other = (BaseImage) obj;
		if (baseImageId == null) {
			if (other.baseImageId != null)
				return false;
		} else if (!baseImageId.equals(other.baseImageId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BaseImage [baseImageId=" + baseImageId + ", creationDate=" + creationDate
				+ ", fileName=" + fileName + ", fileDirectory=" + fileDirectory
				+ "]";
	}

}
