package nl.krocket.ocr.core.repositories;

import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.REQUIRED) 
public interface BaseImageRepository extends PagingAndSortingRepository<BaseImage, Long>{
	
	Iterable<BaseImage> findAllByUserInfo(UserInfo user);
	
	Iterable<BaseImage> findAllByProcesStatus(ProcesStatus procesStatus);
	
	Page<BaseImage> findAllByUserInfo(UserInfo user, Pageable pageable);
	
	@Query("select count(b) as value, b.procesStatus as label from BaseImage b where b.userInfo = :userInfo and b.type = :type group by b.procesStatus")
	Iterable<Object[]> countProcessStatusGroupByProcesStatus(@Param("userInfo") UserInfo userInfo, @Param("type") String type);
	
	Integer countAllByUserInfoAndType(UserInfo user, String type);

}
