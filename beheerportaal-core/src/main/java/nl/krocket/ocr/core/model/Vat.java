package nl.krocket.ocr.core.model;

public enum Vat {

	TWENTYONE(21),
	SIX(6);
	
	private int value;
	
	private Vat(int value) {
		this.value=value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
 
	public static Vat enumOf(int value) throws Exception {
		for (Vat vat : Vat.values()) {
			if (vat.getValue()==value) {
				return vat;
			}
		}
		throw new Exception("Enum value not found for " + value);
	}
	
	
}
