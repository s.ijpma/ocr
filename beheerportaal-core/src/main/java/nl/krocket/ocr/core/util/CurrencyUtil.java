package nl.krocket.ocr.core.util;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.SupportedCurrency;

public class CurrencyUtil {
	
	public static String getTotalPrice(Set<PaymentRow> paymentRows) {
		Set<PaymentRow> totalRows = paymentRows.stream().filter(row -> row.getName().equalsIgnoreCase("totaal")).collect(Collectors.toSet());
		if (!totalRows.isEmpty()) {
			PaymentRow row = totalRows.iterator().next();
			NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.forLanguageTag(row.getCurrency().getLanguageCode()));
			return formatter.format(row.getValue());
		}
		else {
			Set<PaymentRow> normalRows = paymentRows.stream().filter(row -> !row.getName().equalsIgnoreCase("totaal")).collect(Collectors.toSet());
 			
 			if (!normalRows.isEmpty()){
 				Double total = paymentRows.stream().filter(row -> !row.getName().equalsIgnoreCase("totaal")).mapToDouble(PaymentRow::getValue).max().getAsDouble();
	 			PaymentRow row = normalRows.iterator().next();
	 			SupportedCurrency supportedCurrency = row.getCurrency();
	 			if (supportedCurrency==null) {
	 				supportedCurrency = SupportedCurrency.getDefault();
				}
				NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.forLanguageTag(supportedCurrency.getLanguageCode()));
				return formatter.format(total);
 			}
 			else {
 				return "0";
 			}
		}
	}

	@Deprecated
	public static String toCurrency(Set<PaymentRow> paymentRows) {
		
		String languageTag = "";
		Double totalSoFar = new Double(0);
		for (PaymentRow paymentRow : paymentRows) {
			if (paymentRow.getValue() > totalSoFar) {
				totalSoFar = paymentRow.getValue();
			}
			if (languageTag.isEmpty()) {
				SupportedCurrency supportedCurrency = paymentRow.getCurrency();
				if (supportedCurrency!=null) {
					languageTag = supportedCurrency.getLanguageCode();
				}
				else {
					languageTag = SupportedCurrency.getDefault().getLanguageCode();
				}
			}
		}
		if (totalSoFar != 0) { 
			NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.forLanguageTag(languageTag));
			return formatter.format(totalSoFar);
		}
		else {
			return "0";
		}
	}

}
