package nl.krocket.ocr.core.model;

public enum ProcesStatus {
	
	UPLOADED,
	PROCESSING,
	WAITING_FOR_APPROVAL,
	COMPANY_UNKNOWN,
	ACCEPTED,
	DENIED,
	UNKNOWN

}
