package nl.krocket.ocr.core.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ocr_receipt_row")
@XmlRootElement
public class PaymentRow implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="paymentRowId_seq")
    @SequenceGenerator(name="paymentRowId_seq", sequenceName="paymentRowId_seq", allocationSize=1)
    @Column(name = "paymentRowId")
    private Long paymentRowId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "value")
	private Double value;
	
	@Column(name = "currency")
	@Enumerated(EnumType.ORDINAL)
	private SupportedCurrency currency;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "baseImageId")
    private BaseImage baseImage;

	public Long getPaymentRowId() {
		return paymentRowId;
	}

	public void setPaymentRowId(Long paymentRowId) {
		this.paymentRowId = paymentRowId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public BaseImage getBaseImage() {
		return this.baseImage;
	}

	public void setBaseImage(BaseImage baseImage) {
		this.baseImage = baseImage;
	}
	
	/**
	 * @return the currency
	 */
	public SupportedCurrency getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(SupportedCurrency currency) {
		this.currency = currency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((paymentRowId == null) ? 0 : paymentRowId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentRow other = (PaymentRow) obj;
		if (paymentRowId == null) {
			if (other.paymentRowId != null)
				return false;
		} else if (!paymentRowId.equals(other.paymentRowId))
			return false;
		if (!name.equalsIgnoreCase(other.getName())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PaymentRow [paymentRowId=" + paymentRowId + ", name=" + name
				+ ", value=" + value + ", baseImage=" + baseImage + "]";
	}
	
}
