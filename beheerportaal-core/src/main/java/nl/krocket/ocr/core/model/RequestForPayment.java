package nl.krocket.ocr.core.model;

import java.io.Serializable;

/**
 * Defines a request for payment, for a sale, ie an invoice.
 * 
 * It lists goods or services provided by the seller to the customer, 
 * along with prices, credits, discounts, taxes and total due. 
 * It may also include credit information, an invoice number, 
 * a salesperson’s name and any special sales programs. 
 * For example, many invoices allow the buyer 30 days to pay and offer a discount 
 * for paying within the first 10 days of the invoice date. An invoice includes 
 * business contact information for the seller, including business name, address, 
 * phone number, fax number and web address. It also includes contact information 
 * for the buyer and the date of the sales transaction. Invoices should not be confused 
 * with purchase orders, which are written requests from buyers to sellers authorizing 
 * the shipment or delivery of goods with agreement to pay.
 * See: http://smallbusiness.chron.com/difference-between-invoice-receipt-21275.html
 * @author Sander
 *
 */
public interface RequestForPayment extends Serializable {

}
