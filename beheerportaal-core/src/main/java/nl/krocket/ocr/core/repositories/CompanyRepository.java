package nl.krocket.ocr.core.repositories;

import nl.krocket.ocr.core.model.Company;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.REQUIRED)  
public interface CompanyRepository extends CrudRepository<Company, Long> {
	
	public Company findDistinctByCompanyNameIgnoreCase(String companyName);

	public Iterable<Company> findByCompanyNameContainingIgnoreCase(String companyName);
	
	public Iterable<Company> findByCompanyNameStartsWithIgnoreCase(String companyName);
}
