package nl.krocket.ocr.core.model;

import java.io.Serializable;

/**
 * Defines a proof of payment from a buyer to a supplier, ie a shopping receipt.
 * Documentation that payment has been made to finalize a sale. 
 * It serves as proof of ownership in most cases. It lists goods or services, 
 * prices, credits, discount, taxes, total amount paid and method of payment. 
 * Receipts generally include information about buyers and sellers, in different 
 * formats and to varying degrees. For example, point of sale purchases, such as 
 * buying gas for a car at a gas station, have complete seller’s contact 
 * information but limited buyer’s information. A receipt is the buyer’s proof of payment.
 * See: http://smallbusiness.chron.com/difference-between-invoice-receipt-21275.html
 * @author Sander
 *
 */
public interface ProofOfPayment extends Serializable {

}
