package nl.krocket.ocr.core.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import nl.krocket.ocr.core.util.CurrencyUtil;

@Entity
@Table(name = "ocr_receipt")
@XmlRootElement
@PrimaryKeyJoinColumn(name="baseImageId")
public class Receipt extends OcrImage implements RequestForPayment {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Column(name = "vat")
	@Basic(optional = true)
	@Enumerated(EnumType.ORDINAL)
	private Vat vat;
	
	@Column(name = "receiptDate")
    @Temporal(TemporalType.TIMESTAMP)
	@Basic(optional = true)
    private Date receiptDate;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE) //leave company on delete
    private Company receiptCompany;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "baseImage", orphanRemoval=true)
    private Set<PaymentRow> paymentRows;
	
	@Transient
	private String total;

	public Set<PaymentRow> getPaymentRows() {
		if (paymentRows == null) {
			paymentRows = new HashSet<PaymentRow>();
		}
		return paymentRows;
	}

	public void setPaymentRows(Set<PaymentRow> paymentRows) {
		if (paymentRows != null) {
			this.paymentRows = paymentRows;
		}
		else {
			this.paymentRows.clear();
		}
	}
	
	public Vat getVat() {
		return vat;
	}

	public void setVat(Vat vat) {
		this.vat = vat;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public Company getReceiptCompany() {
		return receiptCompany;
	}
	
	public String getTotal() {
		if (null==this.total || this.total.isEmpty()) {
			this.total = CurrencyUtil.getTotalPrice(paymentRows);
		}
		return this.total;
	}

	public void setReceiptCompany(Company receiptCompany) {
		this.receiptCompany = receiptCompany;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getBaseImageId() == null) ? 0 : getBaseImageId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Receipt other = (Receipt) obj;
		if (getBaseImageId() == null) {
			if (other.getBaseImageId() != null)
				return false;
		} else if (!getBaseImageId().equals(other.getBaseImageId()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Receipt [id=" + getBaseImageId() + ", vat=" + vat
				+ ", receiptDate=" + receiptDate + ", receiptCompany="
				+ receiptCompany + ", paymentRows="
				+ paymentRows + "]";
	}

	/**
	 * Determines minimum set of necessary properties for this to be called a receipt
	 * @return
	 */
	public boolean isReceipt() {
		return !this.paymentRows.isEmpty();
	}

}
