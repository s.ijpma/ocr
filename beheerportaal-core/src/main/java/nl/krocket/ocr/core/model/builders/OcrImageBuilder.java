// CHECKSTYLE:OFF
/**
 * Source code generated by Fluent Builders Generator
 * Do not modify this file
 * See generator home page at: http://code.google.com/p/fluent-builders-generator-eclipse-plugin/
 */

package nl.krocket.ocr.core.model.builders;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.OcrImage;
import nl.krocket.ocr.core.model.OcrWord;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;

public class OcrImageBuilder extends OcrImageBuilderBase<OcrImageBuilder> {
	public static OcrImageBuilder ocrImage() {
		return new OcrImageBuilder();
	}

	public OcrImageBuilder() {
		super(new OcrImage());
	}

	public OcrImage build() {
		return getInstance();
	}
}

class OcrImageBuilderBase<GeneratorT extends OcrImageBuilderBase<GeneratorT>> {
	private OcrImage instance;

	protected OcrImageBuilderBase(OcrImage aInstance) {
		instance = aInstance;
	}

	protected OcrImage getInstance() {
		return instance;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withOcrWords(Set<OcrWord> aValue) {
		instance.setOcrWords(aValue);

		return (GeneratorT) this;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withAddedOcrWord(OcrWord aValue) {
		if (instance.getOcrWords() == null) {
			instance.setOcrWords(new HashSet<OcrWord>());
		}

		((HashSet<OcrWord>) instance.getOcrWords()).add(aValue);

		return (GeneratorT) this;
	}

	public AddedOcrWordOcrWordBuilder withAddedOcrWord() {
		OcrWord obj = new OcrWord();

		withAddedOcrWord(obj);

		return new AddedOcrWordOcrWordBuilder(obj);
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withOcrThreshold(Integer aValue) {
		instance.setOcrThreshold(aValue);

		return (GeneratorT) this;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withHOcr(String aValue) {
		instance.sethOcr(aValue);

		return (GeneratorT) this;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withOcrText(String aValue) {
		instance.setOcrText(aValue);

		return (GeneratorT) this;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withProcesStatus(ProcesStatus aValue) {
		instance.setProcesStatus(aValue);

		return (GeneratorT) this;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withUserInfo(UserInfo aValue) {
		instance.setUserInfo(aValue);

		return (GeneratorT) this;
	}

	public UserInfoUserInfoBuilder withUserInfo() {
		UserInfo obj = new UserInfo();

		withUserInfo(obj);

		return new UserInfoUserInfoBuilder(obj);
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withBaseImageId(Long aValue) {
		instance.setBaseImageId(aValue);

		return (GeneratorT) this;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withCreationDate(Date aValue) {
		instance.setCreationDate(aValue);

		return (GeneratorT) this;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withFileName(String aValue) {
		instance.setFileName(aValue);

		return (GeneratorT) this;
	}

	@SuppressWarnings("unchecked")
	public GeneratorT withFileDirectory(String aValue) {
		instance.setFileDirectory(aValue);

		return (GeneratorT) this;
	}

	public class AddedOcrWordOcrWordBuilder extends
			OcrWordBuilderBase<AddedOcrWordOcrWordBuilder> {
		public AddedOcrWordOcrWordBuilder(OcrWord aInstance) {
			super(aInstance);
		}

		@SuppressWarnings("unchecked")
		public GeneratorT endOcrWord() {
			return (GeneratorT) OcrImageBuilderBase.this;
		}
	}

	public class UserInfoUserInfoBuilder extends
			UserInfoBuilderBase<UserInfoUserInfoBuilder> {
		public UserInfoUserInfoBuilder(UserInfo aInstance) {
			super(aInstance);
		}

		@SuppressWarnings("unchecked")
		public GeneratorT endUserInfo() {
			return (GeneratorT) OcrImageBuilderBase.this;
		}
	}

	public static class UserInfoBuilderBase<GeneratorT extends UserInfoBuilderBase<GeneratorT>> {
		private UserInfo instance;

		protected UserInfoBuilderBase(UserInfo aInstance) {
			instance = aInstance;
		}

		protected UserInfo getInstance() {
			return instance;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withBaseImage(Set<BaseImage> aValue) {
			instance.setBaseImage(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withAddedBaseImageElement(BaseImage aValue) {
			if (instance.getBaseImage() == null) {
				instance.setBaseImage(new HashSet<BaseImage>());
			}

			((HashSet<BaseImage>) instance.getBaseImage()).add(aValue);

			return (GeneratorT) this;
		}

		public AddedBaseImageElementBaseImageBuilder withAddedBaseImageElement() {
			BaseImage obj = new BaseImage();

			withAddedBaseImageElement(obj);

			return new AddedBaseImageElementBaseImageBuilder(obj);
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withUserId(Long aValue) {
			instance.setUserId(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withUserName(String aValue) {
			instance.setUserName(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withPassword(String aValue) {
			instance.setPassword(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withRole(String aValue) {
			instance.setRole(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withCreationDate(Date aValue) {
			instance.setCreationDate(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withLastLogin(Date aValue) {
			instance.setLastLogin(aValue);

			return (GeneratorT) this;
		}

		public class AddedBaseImageElementBaseImageBuilder extends
				BaseImageBuilderBase<AddedBaseImageElementBaseImageBuilder> {
			public AddedBaseImageElementBaseImageBuilder(BaseImage aInstance) {
				super(aInstance);
			}

			@SuppressWarnings("unchecked")
			public GeneratorT endBaseImageElement() {
				return (GeneratorT) UserInfoBuilderBase.this;
			}
		}
	}

	public static class BaseImageBuilderBase<GeneratorT extends BaseImageBuilderBase<GeneratorT>> {
		private BaseImage instance;

		protected BaseImageBuilderBase(BaseImage aInstance) {
			instance = aInstance;
		}

		protected BaseImage getInstance() {
			return instance;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withProcessStatus(ProcesStatus aValue) {
			instance.setProcesStatus(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withUserInfo(UserInfo aValue) {
			instance.setUserInfo(aValue);

			return (GeneratorT) this;
		}

		public UserInfoUserInfoBuilder withUserInfo() {
			UserInfo obj = new UserInfo();

			withUserInfo(obj);

			return new UserInfoUserInfoBuilder(obj);
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withBaseImageId(Long aValue) {
			instance.setBaseImageId(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withCreationDate(Date aValue) {
			instance.setCreationDate(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withFileName(String aValue) {
			instance.setFileName(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withFileDirectory(String aValue) {
			instance.setFileDirectory(aValue);

			return (GeneratorT) this;
		}

		public class UserInfoUserInfoBuilder extends
				UserInfoBuilderBase<UserInfoUserInfoBuilder> {
			public UserInfoUserInfoBuilder(UserInfo aInstance) {
				super(aInstance);
			}

			@SuppressWarnings("unchecked")
			public GeneratorT endUserInfo() {
				return (GeneratorT) BaseImageBuilderBase.this;
			}
		}
	}

	public static class OcrWordBuilderBase<GeneratorT extends OcrWordBuilderBase<GeneratorT>> {
		private OcrWord instance;

		protected OcrWordBuilderBase(OcrWord aInstance) {
			instance = aInstance;
		}

		protected OcrWord getInstance() {
			return instance;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withOcrImage(OcrImage aValue) {
			instance.setOcrImage(aValue);

			return (GeneratorT) this;
		}

		public OcrImageOcrImageBuilder withOcrImage() {
			OcrImage obj = new OcrImage();

			withOcrImage(obj);

			return new OcrImageOcrImageBuilder(obj);
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withOcrWordId(Long aValue) {
			instance.setOcrWordId(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withWord(String aValue) {
			instance.setWord(aValue);

			return (GeneratorT) this;
		}

		@SuppressWarnings("unchecked")
		public GeneratorT withConfidence(Float aValue) {
			instance.setConfidence(aValue);

			return (GeneratorT) this;
		}

		public class OcrImageOcrImageBuilder extends
				OcrImageBuilderBase<OcrImageOcrImageBuilder> {
			public OcrImageOcrImageBuilder(OcrImage aInstance) {
				super(aInstance);
			}

			@SuppressWarnings("unchecked")
			public GeneratorT endOcrImage() {
				return (GeneratorT) OcrWordBuilderBase.this;
			}
		}
	}
}
