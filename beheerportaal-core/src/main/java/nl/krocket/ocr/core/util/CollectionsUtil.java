package nl.krocket.ocr.core.util;

import java.util.Collection;
import java.util.Iterator;

public class CollectionsUtil {

	public static int getSize(Iterable<?> iterable) {
		int result = 0;
		if (iterable instanceof Collection<?>) {
			result = ((Collection<?>) iterable).size();
		}
		else {
			Iterator<?> it = iterable.iterator();
			while (it.hasNext()) {
			  it.next();
			  result++;
			}
		}
		return result;
	}
	
}
