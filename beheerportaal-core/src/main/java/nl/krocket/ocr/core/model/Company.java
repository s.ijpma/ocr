package nl.krocket.ocr.core.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ocr_company", uniqueConstraints=
@UniqueConstraint(columnNames = {"companyName"}))
@XmlRootElement
public class Company implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="companyId_seq")
    @SequenceGenerator(name="companyId_seq", sequenceName="companyId_seq", allocationSize=1)
    @Column(name = "companyId")
    private Long companyId;
	
	@Column(name = "companyName")
	private String companyName;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "company", orphanRemoval=true)
	private Set<Address> addressSet;
	
	private boolean accepted;
	
	private String vatNumber;
	private String kvkNumber;
	private String url;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BaseImage> baseImage;
	
	/**
	 * @return the baseImage
	 */
	public Set<BaseImage> getBaseImage() {
		if (null==this.baseImage) {
			this.baseImage = new HashSet<BaseImage>();
		}
		return this.baseImage;
	}
	/**
	 * @param baseImage the baseImage to set
	 */
	public void setBaseImage(Set<BaseImage> baseImage) {
		if (null!=baseImage) {
			this.baseImage = baseImage;
		}
		else {
			this.baseImage.clear();
		}
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Set<Address> getAddressSet() {
		if (null==this.addressSet) {
			this.addressSet = new HashSet<Address>();
		}
		return addressSet;
	}
	public void setAddressSet(Set<Address> addressSet) {
		if (null != addressSet) {
			this.addressSet = addressSet;
		}
		else {
			this.addressSet.clear();
		}
	}
	public boolean isAccepted() {
		return accepted;
	}
	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getKvkNumber() {
		return kvkNumber;
	}
	public void setKvkNumber(String kvkNumber) {
		this.kvkNumber = kvkNumber;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((companyId == null) ? 0 : companyId.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (companyId == null) {
			if (other.companyId != null)
				return false;
		} else if (!companyId.equals(other.companyId))
			return false;
		return true;
	}
	
	
}
