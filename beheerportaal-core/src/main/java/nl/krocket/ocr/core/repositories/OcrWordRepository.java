package nl.krocket.ocr.core.repositories;

import nl.krocket.ocr.core.model.OcrWord;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.REQUIRED) 
public interface OcrWordRepository extends CrudRepository<OcrWord, Long> {

}
