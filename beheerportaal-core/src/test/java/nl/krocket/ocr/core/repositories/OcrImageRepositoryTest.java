package nl.krocket.ocr.core.repositories;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import nl.krocket.ocr.core.config.DataAccessConfiguration;
import nl.krocket.ocr.core.config.JpaTestConfiguration;
import nl.krocket.ocr.core.model.OcrImage;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.OcrImageBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.core.util.CollectionsUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DataAccessConfiguration.class, JpaTestConfiguration.class})
public class OcrImageRepositoryTest {
	
	@Autowired private OcrImageRepository ocrImageRepository;
	@Autowired private UserInfoRepository userInfoRepository;
	private UserInfo userInfo;
	private OcrImage ocrImage;
	private OcrImage ocrImage2;
	
	@Before
	public void setUp() throws Exception {
		
		userInfo = new UserInfoBuilder()
			.withCreationDate(new Date())
			.withPassword("pass")
			.withRole("role")
			.withUserName("userName")
			.build();
		
		userInfo = userInfoRepository.save(userInfo);
		
		ocrImage = getOcrImage();
		ocrImage2 = getOcrImage();
		
		ocrImage = ocrImageRepository.save(ocrImage);
		ocrImage2 = ocrImageRepository.save(ocrImage2);
	}

	
	
	@Test
	public void testFindeOne() {
		assertEquals(ocrImage, ocrImageRepository.findOne(ocrImage.getBaseImageId()));
	}
	
	@Test
	public void testAddOne() {
		assertEquals(ocrImage2, ocrImageRepository.findOne(ocrImage2.getBaseImageId()));
	}
	
	@Test
	public void testDelete() {
		ocrImageRepository.delete(ocrImage2);
		assertEquals(1, ocrImageRepository.count());
	}
	
	@Test
	public void testFindAllByUserInfo() {
		Iterable<OcrImage> all = ocrImageRepository.findAllByUserInfo(userInfo);
		assertEquals(2, CollectionsUtil.getSize(all));
	}

	private OcrImage getOcrImage() {
		OcrImage ocrImage = new OcrImageBuilder()
			.withCreationDate(new Date())
			.withFileDirectory("fileDir")
			.withFileName("fileName")
			.withHOcr("hocr")
			.withOcrText("ocr")
			.withOcrThreshold(1)
			.withUserInfo(userInfo)
			.withProcesStatus(ProcesStatus.ACCEPTED)
			.build();
		return ocrImage;
	}
}
