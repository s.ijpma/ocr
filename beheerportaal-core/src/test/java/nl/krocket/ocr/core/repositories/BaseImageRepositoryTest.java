package nl.krocket.ocr.core.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import nl.krocket.ocr.core.config.DataAccessConfiguration;
import nl.krocket.ocr.core.config.JpaTestConfiguration;
import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.BaseImageBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.core.util.CollectionsUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DataAccessConfiguration.class, JpaTestConfiguration.class})
public class BaseImageRepositoryTest {

	@Autowired BaseImageRepository baseImageRepository;
	
	@Autowired UserInfoRepository userInfoRepository;
	
	BaseImage baseImage1;
	BaseImage baseImage2;
		
	UserInfo userInfo;
	
	@Before
	public void setUp() throws Exception {
		userInfo = new UserInfoBuilder()
					.withCreationDate(new Date())
					.withPassword("pass")
					.withRole("role")
					.withUserName("username")
					.build();

		userInfo = userInfoRepository.save(userInfo);
		
		
		
		baseImage1 = new BaseImageBuilder()
					.withCreationDate(new Date())
					.withFileDirectory("fileDir")
					.withFileName("fileName")
					.withUserInfo(userInfo)
					.withType("invoices")
					.withProcesStatus(ProcesStatus.ACCEPTED)
					.build();
		
		baseImage2 = new BaseImageBuilder()
					.withCreationDate(new Date())
					.withFileDirectory("fileDir2")
					.withFileName("fileName2")
					.withProcesStatus(ProcesStatus.ACCEPTED)
					.withUserInfo(userInfo)
					.withType("receipts")
					.build();
		
		baseImage1 = baseImageRepository.save(baseImage1);
		
		
		baseImage2 = baseImageRepository.save(baseImage2);
		
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testFindOne() {
		assertEquals(baseImage1, baseImageRepository.findOne(baseImage1.getBaseImageId()));
	}
	
	
	@Test
	public void testCountProcessStatusGroupByProcesStatusReceipts() {
		Iterable<Object[]> result = baseImageRepository.countProcessStatusGroupByProcesStatus(userInfo, "receipts");
		assertTrue(CollectionsUtil.getSize(result)==1);
	}
	
	@Test
	public void testCountProcessStatusGroupByProcesStatusInvoices() {
		Iterable<Object[]> result = baseImageRepository.countProcessStatusGroupByProcesStatus(userInfo, "invoices");
		assertTrue(CollectionsUtil.getSize(result)==1);
	}
	
	@Test
	public void testCountAllByUserInfoAndTypeIsInvoices() {
		Integer result = baseImageRepository.countAllByUserInfoAndType(userInfo, "invoices");
		assertEquals(new Integer(1), result);
	}
	
	@Test
	public void testCountAllByUserInfoAndTypeIsReceipts() {
		Integer result = baseImageRepository.countAllByUserInfoAndType(userInfo, "receipts");
		assertEquals(new Integer(1), result);
	}
	
	@Test
	public void testFindeTwo() {
		
		assertEquals(baseImage2, baseImageRepository.findOne(baseImage2.getBaseImageId()));
	}
	
	@Test
	public void testFindAll() {
		Iterable<BaseImage> all = baseImageRepository.findAll();
		
		assertEquals(2, CollectionsUtil.getSize(all));
	}

	@Test
	public void testFindAllByUserInfo() {
		Iterable<BaseImage> all = baseImageRepository.findAllByUserInfo(userInfo);
		assertEquals(2, CollectionsUtil.getSize(all));
	}
	
	@Test
	public void testFindAllByUserInfoPageable() {
		Iterable<BaseImage> all = baseImageRepository.findAllByUserInfo(userInfo, new PageRequest(0, 10));
		assertEquals(2, CollectionsUtil.getSize(all));
	}
	
	@Test
	public void testDeleteOne() {
		baseImageRepository.delete(baseImage2);
		assertEquals(1, baseImageRepository.count());
	}
	
	
	
	
}
