package nl.krocket.ocr.core.repositories;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import nl.krocket.ocr.core.config.DataAccessConfiguration;
import nl.krocket.ocr.core.config.JpaTestConfiguration;
import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.model.OcrImage;
import nl.krocket.ocr.core.model.OcrWord;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.Vat;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.core.model.builders.InvoiceBuilder;
import nl.krocket.ocr.core.model.builders.OcrImageBuilder;
import nl.krocket.ocr.core.model.builders.OcrWordBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.core.util.CollectionsUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DataAccessConfiguration.class, JpaTestConfiguration.class})
public class InvoiceRepositoryTest {
	
	@Autowired private UserInfoRepository userInfoRepository;
	@Autowired private OcrImageRepository ocrImageRepository;
	@Autowired private InvoiceRepository invoiceRepository;
	@Autowired private CompanyRepository companyRepository;
	@Autowired private AddressRepository addressRepository;
	
	private UserInfo userInfo;
	private OcrImage ocrImage;
	private OcrWord ocrWord;
	private Invoice invoice;
	private Company company;
	private Address address;
	
	@Before
	public void setUp() throws Exception {
		userInfo = new UserInfoBuilder()
					.withCreationDate(new Date())
					.withPassword("pass")
					.withRole("role")
					.withUserName("username")
					.build();
		
		userInfo = userInfoRepository.save(userInfo);
		
		ocrImage = new OcrImageBuilder()
					.withCreationDate(new Date())
					.withFileDirectory("fileDir")
					.withFileName("fileName")
					.withHOcr("hocr")
					.withOcrText("ocr")
					.withOcrThreshold(1)
					.withUserInfo(userInfo)
					.withProcesStatus(ProcesStatus.ACCEPTED)
					.build();
	
		ocrImage = ocrImageRepository.save(ocrImage);
		
		ocrWord = new OcrWordBuilder()
					.withConfidence(new Float(80))
					.withWord("word")
					.withOcrImage(ocrImage)
					.build();

		
		
		company = new CompanyBuilder()
					.withAccepted(false)
					.withCompanyName("companyName")
					.withKvkNumber("kvk")
					.withUrl("url")
					.withVatNumber("vat")
					.build();
		
		company = companyRepository.save(company);
		
		address = new AddressBuilder()
					.withAccepted(false)
					.withCompany(company)
					.withCity("city")
					.withCountryCode("nl")
					.withHouseNr("1")
					.withPostcode("postcode")
					.withStreetName("street")
					.build();
		
		address = addressRepository.save(address);
		
		company.getAddressSet().add(address);
		
		company = companyRepository.save(company);
		
		invoice = new InvoiceBuilder()
					.withBaseImageId(ocrImage.getBaseImageId())
					.withCreationDate(new Date())
					.withFileDirectory("fileDir")
					.withFileName("fileName")
					
					.withAddedPaymentRow().withOcrImage(ocrImage).withName("name").withValue(new Double(0)).endPaymentRow()
					.withAddedOcrWord(ocrWord)
					.withFromCompany(company)

					.withToCompany(company)
						
					.withHOcr("hocr")
					.withOcrText("ocrText")
					.withOcrThreshold(0)
					.withProcesStatus(ProcesStatus.ACCEPTED)
					.withType("type")
					.withUserInfo(userInfo)
					.withVat(Vat.SIX)
					.build();
		
		invoice = invoiceRepository.save(invoice);
	}
	
	@Test
	public void testFindOne() {
		assertEquals(invoice, invoiceRepository.findOne(invoice.getBaseImageId()));
	}
	
	@Test
	public void testFindCompany() {
		Invoice in = invoiceRepository.findOne(invoice.getBaseImageId());
		assertEquals("companyName", in.getFromCompany().getCompanyName());
	}
	
	@Test
	public void testFindCompanyAddress() {
		Invoice in = invoiceRepository.findOne(invoice.getBaseImageId());
		assertEquals("street", in.getFromCompany().getAddressSet().iterator().next().getStreetName());
	}
	
	@Test
	public void testFindWord() {
		Invoice in = invoiceRepository.findOne(invoice.getBaseImageId());
		assertEquals("word", in.getOcrWords().iterator().next().getWord());
	}
	
	@Test
	public void testFindAll() {
		Iterable<Invoice> all = invoiceRepository.findAll();
		assertEquals(1, CollectionsUtil.getSize(all));
	}
	
	@Test
	public void testFindAllByUserInfo() {
		Iterable<Invoice> all = invoiceRepository.findAllByUserInfo(userInfo);
		assertEquals(1, CollectionsUtil.getSize(all));
	}
	
	@Test
	public void testDelete() {
		invoiceRepository.delete(invoice);
		assertEquals(0, invoiceRepository.count());
	}

}
