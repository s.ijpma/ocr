package nl.krocket.ocr.core.repositories;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import nl.krocket.ocr.core.config.DataAccessConfiguration;
import nl.krocket.ocr.core.config.JpaTestConfiguration;
import nl.krocket.ocr.core.model.OcrImage;
import nl.krocket.ocr.core.model.OcrWord;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.OcrImageBuilder;
import nl.krocket.ocr.core.model.builders.OcrWordBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DataAccessConfiguration.class, JpaTestConfiguration.class})
public class OcrWordRepositoryTest {

	@Autowired private OcrWordRepository wordRepository;
	@Autowired private OcrImageRepository ocrImageRepository;
	@Autowired private UserInfoRepository userInfoRepository;
	private UserInfo userInfo;
	private OcrWord ocrWord;
	private OcrWord ocrWord2;
	private OcrImage ocrImage;
	
	@Before
	public void setUp() throws Exception {
		
		userInfo = new UserInfoBuilder()
			.withCreationDate(new Date())
			.withPassword("pass")
			.withRole("role")
			.withUserName("userName")
			.build();
		
		userInfo = userInfoRepository.save(userInfo);
		
		ocrImage = new OcrImageBuilder()
			.withCreationDate(new Date())
			.withFileDirectory("fileDir")
			.withFileName("fileName")
			.withHOcr("hocr")
			.withOcrText("ocr")
			.withOcrThreshold(1)
			.withUserInfo(userInfo)
			.withProcesStatus(ProcesStatus.ACCEPTED)
			.build();
		
		ocrImage = ocrImageRepository.save(ocrImage);
		
		ocrWord = new OcrWordBuilder()
			.withConfidence(new Float(80))
			.withWord("word")
			.withOcrImage(ocrImage)
			.build();
		
		ocrWord2 = new OcrWordBuilder()
		.withConfidence(new Float(80))
		.withWord("word2")
		.withOcrImage(ocrImage)
		.build();
		
		ocrWord = wordRepository.save(ocrWord);
		
		ocrImage.getOcrWords().add(ocrWord);
		
		ocrImage = ocrImageRepository.save(ocrImage);
	}
	
	@Test
	public void testFindWord() {
		assertEquals(ocrWord, wordRepository.findOne(ocrWord.getOcrWordId()));
	}
	
	@Test
	public void testAddWord() {
		ocrWord2 = wordRepository.save(ocrWord2);
		ocrImage.getOcrWords().add(ocrWord2);
		ocrImage = ocrImageRepository.save(ocrImage);
		assertEquals(ocrWord2, wordRepository.findOne(ocrWord2.getOcrWordId()));
	}
	
	@Test
	public void deleteWord() {
		wordRepository.delete(ocrWord2);
		ocrImage.getOcrWords().remove(ocrWord2);
		ocrImage = ocrImageRepository.save(ocrImage);
		assertEquals(1, wordRepository.count());
	}
}
