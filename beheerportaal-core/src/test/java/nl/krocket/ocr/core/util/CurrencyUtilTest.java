package nl.krocket.ocr.core.util;

import java.util.HashSet;
import java.util.Set;

import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.SupportedCurrency;
import nl.krocket.ocr.core.model.builders.PaymentRowBuilder;

import org.junit.Test;

import static org.junit.Assert.*;

public class CurrencyUtilTest {
	
	@Test
	public void testCurrencyEuros() {
		PaymentRow row = getPaymentRow(SupportedCurrency.EUR, "name", new Double(10.12));
		Set<PaymentRow> rows = new HashSet<PaymentRow>();
		rows.add(row);
		String actual = CurrencyUtil.getTotalPrice(rows);
		String expected = "€ 10,12";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testCurrencyDefault() {
		PaymentRow row = getPaymentRow(null, "name", new Double(10.12));
		Set<PaymentRow> rows = new HashSet<PaymentRow>();
		rows.add(row);
		String actual = CurrencyUtil.getTotalPrice(rows);
		String expected = "€ 10,12";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testCurrencyDollars() {
		PaymentRow row = getPaymentRow(SupportedCurrency.USD, "name", new Double(10.12));
		Set<PaymentRow> rows = new HashSet<PaymentRow>();
		rows.add(row);
		String actual = CurrencyUtil.getTotalPrice(rows);
		String expected = "$10.12";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testTotal() {
		PaymentRow rowTotal = getPaymentRow(SupportedCurrency.EUR, "Totaal", new Double(10.12));
		PaymentRow rowNormal1 = getPaymentRow(SupportedCurrency.EUR, "item 1", new Double(2.12));
		PaymentRow rowNormal2 = getPaymentRow(SupportedCurrency.EUR, "item 2", new Double(8.12));
		Set<PaymentRow> rows = new HashSet<PaymentRow>();
		rows.add(rowTotal);
		rows.add(rowNormal1);
		rows.add(rowNormal2);
		String actual = CurrencyUtil.getTotalPrice(rows);
		String expected = "€ 10,12";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testNormalAdd() {
		PaymentRow rowNormal1 = getPaymentRow(SupportedCurrency.EUR, "item 1", new Double(2.12));
		PaymentRow rowNormal2 = getPaymentRow(SupportedCurrency.EUR, "item 2", new Double(8.00));
		Set<PaymentRow> rows = new HashSet<PaymentRow>();
		rows.add(rowNormal1);
		rows.add(rowNormal2);
		String actual = CurrencyUtil.getTotalPrice(rows);
		String expected = "€ 8,00";
		assertEquals(expected, actual);
	}
	
	@Test
	public void testNoPaymentRows() {
		Set<PaymentRow> rows = new HashSet<PaymentRow>();
		String actual = CurrencyUtil.getTotalPrice(rows);
		String expected = "0";
		assertEquals(expected, actual);
	}

	private PaymentRow getPaymentRow(SupportedCurrency supportedCurrenct, String name, Double price) {
		return PaymentRowBuilder
				.paymentRow()
				.withCurrency(supportedCurrenct)
				.withName(name)
				.withValue(price)
				.build();
	}
	
}
