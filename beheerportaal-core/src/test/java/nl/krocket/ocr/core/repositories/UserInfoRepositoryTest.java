package nl.krocket.ocr.core.repositories;

import java.util.Date;

import nl.krocket.ocr.core.config.DataAccessConfiguration;
import nl.krocket.ocr.core.config.JpaTestConfiguration;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DataAccessConfiguration.class, JpaTestConfiguration.class})
public class UserInfoRepositoryTest {

	@Autowired private UserInfoRepository userInfoRepository;
	private UserInfo userInfo;
	private UserInfo userInfo2;
	
	@Before
	public void setUp() throws Exception {
		userInfo = getUserInfo("userName");
		userInfo2 = getUserInfo("userName2");
		userInfoRepository.save(userInfo);
	}
	
	@Test
	public void testUserInfoById() {
		UserInfo actual = userInfoRepository.findOne(userInfo.getUserId());
		assertEquals(userInfo, actual);
	}
	
	@Test
	public void testUserInfoByUserName() {
		UserInfo actual = userInfoRepository.findByUserName(userInfo.getUserName());
		assertEquals(userInfo, actual);
	}
	
	@Test
	public void addUserInfo() {
		userInfo2 = userInfoRepository.save(userInfo2);
		UserInfo actual = userInfoRepository.findOne(userInfo2.getUserId());
		assertEquals(userInfo2, actual);
	}
	
	@Test
	public void deleteUserInfo() {
		userInfoRepository.delete(userInfo2);
		assertEquals(1, userInfoRepository.count());
	}
	
	private UserInfo getUserInfo(String userName) {
		UserInfo userInfo = new UserInfoBuilder()
		.withCreationDate(new Date())
		.withPassword("pass")
		.withRole("role")
		.withUserName(userName)
		.build();
		return userInfo;
	}
}
