package nl.krocket.ocr.core.repositories;

import static org.junit.Assert.assertEquals;
import nl.krocket.ocr.core.config.DataAccessConfiguration;
import nl.krocket.ocr.core.config.JpaTestConfiguration;
import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.core.util.CollectionsUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DataAccessConfiguration.class, JpaTestConfiguration.class})
public class CompanyRepositoryTest {

	@Autowired private CompanyRepository companyRepository;
	@Autowired private AddressRepository addressRepository;
	
	private Company company;
	private Address address;
	
	@Before
	public void setUp() throws Exception {
		company = new CompanyBuilder()
				.withAccepted(false)
				.withCompanyName("companyName")
				.withKvkNumber("kvk")
				.withUrl("url")
				.withVatNumber("vat")
				.build();

		company = companyRepository.save(company);
		
		address = new AddressBuilder()
				.withAccepted(false)
				.withCompany(company)
				.withCity("city")
				.withCountryCode("nl")
				.withHouseNr("1")
				.withPostcode("postcode")
				.withStreetName("street")
				.build();
		
		address = addressRepository.save(address);
		
		company.getAddressSet().add(address);
		
		company = companyRepository.save(company);
	}
	
	@Test
	public void testFindOne() {
		assertEquals(company, companyRepository.findOne(company.getCompanyId()));
	}
	
	@Test
	public void testFindAll() {
		Iterable<Company> all = companyRepository.findAll();
		assertEquals(1, CollectionsUtil.getSize(all));
	}
	
	@Test
	public void testFindByCompanyNameContainingIgnoreCase() {
		Iterable<Company> all = companyRepository.findByCompanyNameContainingIgnoreCase("companyname");
		assertEquals(1, CollectionsUtil.getSize(all));
	}
	
	@Test
	public void testFindByCompanyNameStartsWith() {
		Iterable<Company> all = companyRepository.findByCompanyNameStartsWithIgnoreCase("companyNa");
		assertEquals(1, CollectionsUtil.getSize(all));
	}
	
	@Test
	public void testFindDistinctByCompanyName1() {
		assertEquals(company, companyRepository.findDistinctByCompanyNameIgnoreCase("companyName"));
	}
	
	@Test
	public void testFindDistinctByCompanyName2() {
		assertEquals(company, companyRepository.findDistinctByCompanyNameIgnoreCase("ComPanyName"));
	}
	
	@Test
	public void testDelete() {
		companyRepository.delete(company);
		assertEquals(0, companyRepository.count());
	}
}
