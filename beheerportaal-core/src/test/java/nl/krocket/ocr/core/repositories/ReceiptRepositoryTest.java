package nl.krocket.ocr.core.repositories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import nl.krocket.ocr.core.config.DataAccessConfiguration;
import nl.krocket.ocr.core.config.JpaTestConfiguration;
import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.Vat;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.core.model.builders.PaymentRowBuilder;
import nl.krocket.ocr.core.model.builders.ReceiptBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.core.util.CollectionsUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = {DataAccessConfiguration.class, JpaTestConfiguration.class})
public class ReceiptRepositoryTest {

	
	@Autowired private ReceiptRepository receiptRepository;
	@Autowired private PaymentRowRepository receiptRowrepository;
	@Autowired private UserInfoRepository userInfoRepository;
	@Autowired private CompanyRepository companyRepository;
	@Autowired private AddressRepository addressRepository;

	private Receipt receipt;
	private Receipt receipt2;
	private UserInfo userInfo;
	private Company company;
	
	@Before
	public void setUp() throws Exception {
		userInfo = new UserInfoBuilder()
					.withCreationDate(new Date())
					.withPassword("pass")
					.withRole("role")
					.withUserName("username")
					.build();
		
		userInfo = userInfoRepository.save(userInfo);
		
		receipt = getReceipt();
		receipt2 = getReceipt();
		
		receipt = receiptRepository.save(receipt);
		
		receipt2 = receiptRepository.save(receipt2);
	}

	
	
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindOne() {
		assertEquals(receipt, receiptRepository.findOne(receipt.getBaseImageId()));
	}
	
	@Test
	public void testAddOne() {
		assertEquals(receipt2, receiptRepository.findOne(receipt2.getBaseImageId()));
	}
	
	@Test
	public void testDelete() {
		receiptRepository.delete(receipt2);
		assertEquals(1, receiptRepository.count());
	}
	
	@Test
	public void testFindAllByUserInfo() {
		Iterable<Receipt> all = receiptRepository.findAllByUserInfo(userInfo);
		assertEquals(2, CollectionsUtil.getSize(all));
	}
	
	@Test
	public void testAddPaymentRow() {
		PaymentRow paymentRow = new PaymentRowBuilder()
					.withName("name")
					.withOcrImage(receipt)
					.withValue(new Double(0))
					.build();
		paymentRow = receiptRowrepository.save(paymentRow);
		
		receipt.getPaymentRows().add(paymentRow);
		
		receipt = receiptRepository.save(receipt);
		
		receipt = receiptRepository.findOne(receipt.getBaseImageId());
		
		assertTrue(receipt.getPaymentRows().contains(paymentRow));
	}
	
	
	
	private Receipt getReceipt() {
		if (this.company == null) {
			this.company = getCompany();
		}
		Receipt receipt = new ReceiptBuilder()
					.withCreationDate(new Date())
					.withFileDirectory("fileDir")
					.withFileName("fileName")
					.withHOcr("hocr")
					.withOcrText("ocrText")
					.withReceiptCompany(this.company)
					.withReceiptDate(new Date())
					.withVat(Vat.TWENTYONE)
					.withUserInfo(userInfo)
					.withProcesStatus(ProcesStatus.ACCEPTED)
					.build();
		return receipt;
	}
	
	private Company getCompany() {
		Company company = new CompanyBuilder()
		.withAccepted(false)
		.withCompanyName("companyName")
		.withKvkNumber("kvk")
		.withUrl("url")
		.withVatNumber("vat")
		.build();

		company = companyRepository.save(company);
		
		Address address = new AddressBuilder()
				.withAccepted(false)
				.withCompany(company)
				.withCity("city")
				.withCountryCode("nl")
				.withHouseNr("1")
				.withPostcode("postcode")
				.withStreetName("street")
				.build();
		
		address = addressRepository.save(address);
		
		company.getAddressSet().add(address);
		
		company = companyRepository.save(company);
		return company;
	}
}
