package nl.krocket.ocr.web.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.builders.BaseImageBuilder;
import nl.krocket.ocr.core.util.CollectionsUtil;
import nl.krocket.ocr.web.backing.ActiveUser;
import nl.krocket.ocr.web.backing.CurrentUser;
import nl.krocket.ocr.web.backing.TotalWrapper;
import nl.krocket.ocr.web.service.db.BaseImageService;

@Controller
public class BaseImageController {

	private static final String DEFAULT_ORDER = "creationDate";

	@Autowired
    private BaseImageService baseImageService;
	
	@RequestMapping(value="/get/baseimages", method=RequestMethod.GET)
    public @ResponseBody TotalWrapper getBaseImage(@CurrentUser ActiveUser user) {
		Iterable<BaseImage> result = baseImageService.findAllByUserInfo(user);
        return new TotalWrapper(result);
    }
	
	@RequestMapping(value="/get/baseimages/total", method=RequestMethod.GET)
    public @ResponseBody TotalWrapper getReceiptOrderedAndPaginatedList(
    		@CurrentUser ActiveUser user, 
    		@RequestParam(value="sort", required=false) String sort, 
    		@RequestParam(value="order", required=true) String order,
    		@RequestParam(value="limit", required=true) int limit,
    		@RequestParam(value="offset", required=true) int offset) {
		if (null==sort || sort.isEmpty()) {
			sort = DEFAULT_ORDER;
		}
		offset = offset / limit;
		Iterable<BaseImage> receiptsTotal = baseImageService.findAllByUserInfo(user);
		int totalSize = CollectionsUtil.getSize(receiptsTotal);
		Iterable<BaseImage> receiptsPaginated = baseImageService.findAllByUserInfo(user, sort, order, limit, offset);
        return new TotalWrapper(receiptsPaginated, totalSize);
    }
	
	@RequestMapping(value="/get/baseimage/{id}", method=RequestMethod.GET)
    public ModelAndView getBaseImage(@PathVariable("id") String id) {
		BaseImage receipt = baseImageService.get(Long.parseLong(id));
		ModelAndView model = new ModelAndView("receipt", "model", receipt);
        return model;
    }
	
	@RequestMapping(value="/save/baseimage/", method=RequestMethod.POST)
	public void postBaseImage(@CurrentUser ActiveUser user, @RequestBody ScannedImage image) {
		BaseImage baseImage = BaseImageBuilder.baseImage()
        		.withCreationDate(new Date())
        		.withFileDirectory(image.getDir())
        		.withFileName(image.getFileName())
        		.withUserInfo(user)
        		.withProcesStatus(ProcesStatus.UPLOADED)
        		.build();
		baseImage = baseImageService.save(baseImage);
	}
}
