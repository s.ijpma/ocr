package nl.krocket.ocr.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.backing.Result;
import nl.krocket.ocr.web.backing.UploadTypeEnum;
import nl.krocket.ocr.web.service.db.BaseImageService;
import nl.krocket.ocr.web.service.db.InvoiceService;
import nl.krocket.ocr.web.service.db.ReceiptService;
import nl.krocket.ocr.web.service.other.PropertyService;
import nl.krocket.ocr.web.util.FolderUtil;
import nl.krocket.ocr.web.util.InvoiceUtil;
import nl.krocket.ocr.web.util.ReceiptUtil;

@Controller
public class ReceiptController {

	@Autowired
    private ReceiptService receiptService;
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private BaseImageService baseImageService;
	@Autowired
	private PropertyService propertyService;
	
	@RequestMapping(value="/get/receipt/{id}", method=RequestMethod.GET)
    public @ResponseBody Receipt getReceipt(@PathVariable("id") String id) {
		Receipt receipt = receiptService.get(Long.parseLong(id));
		return receipt;
    }
	
	@RequestMapping(value="/get/receipt_model/{id}", method=RequestMethod.GET)
	public ModelAndView getReceiptModel(@PathVariable("id") String id) {
		Receipt receipt = receiptService.get(Long.parseLong(id));
		ModelAndView model = new ModelAndView("receipt", "model", receipt);
        return model;
	}
	
	@RequestMapping(value="/delete/receipt/{id}", method=RequestMethod.POST)
    public @ResponseBody Result deleteReceipt(@PathVariable("id") String id) {
		BaseImage image = baseImageService.get(Long.parseLong(id));
		String fileDir = "";
		if (image.getType().equalsIgnoreCase("receipts")) {
			fileDir = ReceiptUtil.delete(id, receiptService);
		}
		else {
			fileDir = InvoiceUtil.delete(id, invoiceService);
		}
        if (!fileDir.isEmpty()) {
        	String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_UPLOAD_PATH);
			FolderUtil.deleteFiles(path, UploadTypeEnum.RECEIPT.toString(), image);
		}
        Result r = new Result();
        r.setMessage("Succesfully deleted: " + id);
        return r;
    }

}
