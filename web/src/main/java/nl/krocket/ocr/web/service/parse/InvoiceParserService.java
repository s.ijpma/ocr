package nl.krocket.ocr.web.service.parse;

import java.util.Date;
import java.util.Map;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.model.builders.InvoiceBuilder;
import nl.krocket.ocr.web.backing.UploadTypeEnum;
import nl.krocket.ocr.web.util.ParserUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceParserService {

	
	@Autowired
	private CompanyParserService companyParserService;
	
	public Invoice parse(ScannedImage image) throws Exception {
		
		String scannedText = image.getScannedText();
		Map<String, Company> invoiceCompanies = companyParserService.parseForInvoice(scannedText);
		
		Invoice invoice = new InvoiceBuilder()
				.withCreationDate(new Date())
				.withFileDirectory(image.getDir())
				.withFileName(image.getFileName())
				.withType(UploadTypeEnum.INVOICE.toString())
				.withHOcr(image.gethOCRText())
				.withOcrThreshold(image.getThresh())
				.withOcrText(scannedText)
				.withOcrWords(ParserUtil.getOcrWords(image.getWords()))
				.withFromCompany(invoiceCompanies.get("fromCompany"))
				.withPaymentRows(ParserUtil.findPaymentRows(scannedText))
				.withToCompany(invoiceCompanies.get("toCompany"))
				.withVat(ParserUtil.getVat(scannedText))
				.build();
		return invoice;
	}

	

}
