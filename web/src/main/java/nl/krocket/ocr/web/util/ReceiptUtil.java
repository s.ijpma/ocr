package nl.krocket.ocr.web.util;

import java.util.HashSet;
import java.util.Set;

import nl.krocket.ocr.core.model.OcrWord;
import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.web.service.db.OcrWordService;
import nl.krocket.ocr.web.service.db.PaymentRowService;
import nl.krocket.ocr.web.service.db.ReceiptService;

import org.apache.log4j.Logger;

public class ReceiptUtil {
	
	private static final Logger LOGGER = Logger.getLogger(ReceiptUtil.class.getCanonicalName());
	
	/**
	 * Only static, no instantiation
	 */
	private ReceiptUtil() {
	}
	
	public static Receipt save(Receipt receipt, ReceiptService receiptService, PaymentRowService receiptRowService, OcrWordService ocrWordService) {
		LOGGER.info("Saving receipt: " + receipt.getFileDirectory());
		Set<PaymentRow> paymentRows = new HashSet<PaymentRow>(receipt.getPaymentRows());
		Set<OcrWord> wordRows = new HashSet<OcrWord>(receipt.getOcrWords());
		receipt.setPaymentRows(null);
		receipt.setOcrWords(null);
		//save to database 
		
		//first save receipt
		receipt = receiptService.save(receipt);

		//save paymentrows with receipt constraint
		for (PaymentRow paymentRow : paymentRows) {
			//set constraint to row
			paymentRow.setBaseImage(receipt);
			//save row
			paymentRow = receiptRowService.save(paymentRow);
			//add saved row to receipt
			receipt.getPaymentRows().add(paymentRow);
		}
		//save receipt
		receipt = receiptService.save(receipt);
		
		for (OcrWord ocrWord : wordRows) {
			ocrWord.setOcrImage(receipt);
			ocrWord = ocrWordService.save(ocrWord);
			receipt.getOcrWords().add(ocrWord);
		}
		receipt = receiptService.save(receipt);
		return receipt;
	}

	public static String delete(String id, ReceiptService receiptService) {
		long baseImageId = Long.parseLong(id);
		String fileDir = receiptService.get(baseImageId).getFileDirectory();
		receiptService.delete(baseImageId);
		return fileDir;
	}
	
}
