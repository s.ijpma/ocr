package nl.krocket.ocr.web.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.krocket.ocr.core.model.OcrImage;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.repositories.OcrImageRepository;

@Service
public class OcrImageServiceImpl implements OcrImageService {
	
	@Autowired 
	OcrImageRepository repository;

	@Override
	public Iterable<OcrImage> findAll() {
		return repository.findAll();
	}

	@Override
	public Iterable<OcrImage> findAllByUserInfo(UserInfo userInfo) {
		return repository.findAllByUserInfo(userInfo);
	}

	@Override
	public OcrImage get(Long baseImageId) {
		return repository.findOne(baseImageId);
	}

	@Override
	public OcrImage save(OcrImage ocrImage) {
		return repository.save(ocrImage);
	}

	@Override
	public void delete(OcrImage ocrImage) {
		repository.delete(ocrImage);
	}

	@Override
	public Long getCount() {
		return repository.count();
	}

}
