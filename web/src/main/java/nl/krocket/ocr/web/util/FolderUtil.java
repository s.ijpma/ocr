package nl.krocket.ocr.web.util;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import nl.krocket.ocr.core.model.BaseImage;

import org.apache.log4j.Logger;

public class FolderUtil {
	
	private static final Logger LOGGER = Logger.getLogger(FolderUtil.class.getCanonicalName());
	
	 public static void deleteFiles(String path, String type, BaseImage baseImage) {
		 Path p = Paths.get(path, baseImage.getUserInfo().getUserId().toString(), type, baseImage.getFileDirectory());
		 File f = p.toFile();
		 if (f.exists() && f.isDirectory()) {
			 File[] files = f.listFiles();
			 for (File file : files) {
				 deleteFile(file);
			}
			 deleteFile(f);
		 }
	}

		private static void deleteFile(File f) {

			if (f.exists()) {
				LOGGER.info("Delete file: " + f.getPath());
	    		boolean success = f.delete();
	    		if (!success) {
	    			LOGGER.error("Failed to delete file: " + f.getPath());
	    		}
	    	}
		}
}
