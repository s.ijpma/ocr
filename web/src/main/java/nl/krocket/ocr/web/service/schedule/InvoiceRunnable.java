package nl.krocket.ocr.web.service.schedule;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.scan.ReceiptScanner;
import nl.krocket.ocr.service.FileUtil;
import nl.krocket.ocr.web.backing.ProcessState;
import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.backing.Result;
import nl.krocket.ocr.web.backing.UploadTypeEnum;
import nl.krocket.ocr.web.service.db.InvoiceService;
import nl.krocket.ocr.web.service.parse.InvoiceParserService;
import nl.krocket.ocr.web.util.InvoiceUtil;

public class InvoiceRunnable extends BaseImageRunnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(InvoiceRunnable.class.getCanonicalName());

	InvoiceService invoiceService;
	InvoiceParserService invoiceParserService;

	@Override
	public void run(BaseImage nextImage) {
		state = ProcessState.STATE_BUSY; //set to busy instantly
		updateStatus(5, "Done", "Initilizing...");
		String scriptPath = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_PATH);
		String pdfConvert = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_PDFCONVERT);
		String textclean = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_TEXTCLEAN);
    	String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_UPLOAD_PATH);
    	LOGGER.info("Processing invoice: " + nextImage.getFileDirectory());
    	nextImage = updateProcesStatus(nextImage, ProcesStatus.PROCESSING);
    	try {
    		Path sourcePath = Paths.get(path, nextImage.getUserInfo().getUserId().toString(), UploadTypeEnum.INVOICE.toString(), nextImage.getFileDirectory());
			Path sourceFile = Paths.get(sourcePath + File.separator + nextImage.getFileName());
			
			String convertedFileName = FileUtil.renameFile(nextImage.getFileName(), "jpg", "_converted");
			Path convertedFilePath = Paths.get(sourcePath + File.separator + convertedFileName);
			updateStatus(5, "Done", "Converting pdf...");
			LOGGER.info("Do pdf convert on: " + sourceFile.toString());
    		Result r = scriptService.runScript(scriptPath + pdfConvert + " " + sourceFile.toString() + " " + convertedFilePath.toString());
    		LOGGER.info("Info from scriptservice: " + r.getMessage() + ", errors?:" + r.getError());
    		updateStatus(20, "Done", "Image processing...");
			//do imagemagic
			LOGGER.info("Do imagemagic stuff");
			String magicFileName = FileUtil.renameFile(convertedFileName, "", "_magic");
		
			Path magicFilePath = Paths.get(sourcePath + File.separator + magicFileName);
			
			Result r3 = scriptService.runScript(scriptPath + textclean + " " + convertedFilePath.toString() + " " + magicFilePath.toString());
			LOGGER.info("Info from scriptservice: " + r3.getMessage() + ", errors?:" + r3.getError()); 
			LOGGER.info("New fileName: " + magicFileName);
    		nextImage = updateFileName(nextImage, magicFileName);
    		updateStatus(20, "Done", "Image ocr-ing...");
    		//do ocr scan
			ScannedImage scannedImage = scan(nextImage, UploadTypeEnum.INVOICE);
			LOGGER.info("Ocred invoice: " + scannedImage.getScannedText());
			//set dir to original dir!!
			scannedImage.setDir(nextImage.getFileDirectory());
    		//do parsing
			updateStatus(20, "Done", "Parsing...");
			Invoice invoice = invoiceParserService.parse(scannedImage);
			//if is receipt
			if (invoice.isInvoice()) {
				if (null == invoice.getToCompany() || null == invoice.getFromCompany()) { //if toCompany not equal to ownCompany then to could be from or the other way round
					invoice.setProcesStatus(ProcesStatus.COMPANY_UNKNOWN);
				}
				else {
					invoice.setProcesStatus(ProcesStatus.WAITING_FOR_APPROVAL);
				}
			}
			else {
				invoice.setProcesStatus(ProcesStatus.DENIED);
			}
			//set id & userinfo before saving
			invoice.setBaseImageId(nextImage.getBaseImageId());
			invoice.setUserInfo(nextImage.getUserInfo());
			//do save
			updateStatus(20, "Done", "Save to database...");
			InvoiceUtil.save(invoice, invoiceService, companyService, addressService, paymentRowService, ocrWordService);
			this.state=ProcessState.STATE_FINISHED;
			updateStatus(10, "Done", "Finished");
    	}
    	catch (Exception e) {
			LOGGER.error("Something went wrong scanning invoice " + nextImage.getFileDirectory(), e);
			updateProcesStatus(nextImage, ProcesStatus.UNKNOWN);
			this.state=ProcessState.STATE_FINISHED;
			updateStatus(0, "Done", "Something went wrong...");
		}
	}

	/**
	 * @return the invoiceService
	 */
	public InvoiceService getInvoiceService() {
		return invoiceService;
	}

	/**
	 * @param invoiceService the invoiceService to set
	 */
	public void setInvoiceService(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	/**
	 * @return the invoiceParserService
	 */
	public InvoiceParserService getInvoiceParserService() {
		return invoiceParserService;
	}

	/**
	 * @param invoiceParserService the invoiceParserService to set
	 */
	public void setInvoiceParserService(InvoiceParserService invoiceParserService) {
		this.invoiceParserService = invoiceParserService;
	}

	public ScannedImage scan(BaseImage baseImage, UploadTypeEnum type) throws Exception {
		String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_UPLOAD_PATH);
   		return ReceiptScanner.scanInvoice(path, baseImage.getUserInfo().getUserId() + File.separator + type.toString() + File.separator + baseImage.getFileDirectory(), baseImage.getFileName());
    }
	
	
}
