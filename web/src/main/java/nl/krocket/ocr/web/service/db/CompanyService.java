package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.Company;

public interface CompanyService {

	/**
	 * Finds all companies
	 * @return
	 */
	public Iterable<Company> findAll();
	
	/**
	 * Gets receipt based on given companyId
	 * @param receiptId
	 * @return
	 */
	public Company get(Long companyId);
	
	/**
	 * Saves given company object
	 * @param receipt
	 * @return
	 */
	public Company save(Company company);
	
	/**
	 * Deletes given company object
	 * @param receipt
	 */
	public void delete(Company company);
	
	/**
	 * Gets count of rows
	 * @return
	 */
	public Long getCount();
	
	/**
	 * Finds distinct company by company name
	 * @param comapnyName
	 * @return
	 */
	public Company findDistinctByCompanyNameIgnoreCase(String companyName);
	
	/**
	 * Finds companies that start with comapany name
	 * @param companyName
	 * @return
	 */
	public Iterable<Company> findByCompanyNameStartsWithIgnoreCase(String companyName);
}
