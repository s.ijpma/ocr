package nl.krocket.ocr.web.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.model.builders.InvoiceBuilder;
import nl.krocket.ocr.core.model.builders.ReceiptBuilder;
import nl.krocket.ocr.web.backing.ActiveUser;
import nl.krocket.ocr.web.backing.CurrentUser;
import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.backing.Result;
import nl.krocket.ocr.web.backing.UploadException;
import nl.krocket.ocr.web.backing.UploadTypeEnum;
import nl.krocket.ocr.web.service.db.InvoiceService;
import nl.krocket.ocr.web.service.db.ReceiptService;
import nl.krocket.ocr.web.service.other.PropertyService;
import nl.krocket.ocr.web.service.script.ScriptService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {

	private static final Logger LOGGER = Logger.getLogger(FileUploadController.class.getCanonicalName());
	
	@Autowired
	private PropertyService propertyService;
	@Autowired
	private ScriptService scriptService;
	
	@Autowired
	private ReceiptService receiptService;
	@Autowired
	private InvoiceService invoiceService;
	
	@RequestMapping(value="/snapupload", method=RequestMethod.GET)
    public @ResponseBody String provideReceiptUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }
	
	@RequestMapping(value="/snapupload", method=RequestMethod.POST)
    public @ResponseBody Result handleReceiptUpload(@CurrentUser ActiveUser user, @RequestParam("cam") MultipartFile cam) throws Exception {
    	Result r = handleFileUpload(user, cam, UploadTypeEnum.RECEIPT);
    	if (null!=r.getError() || r.getError().isEmpty()) {
	    	Receipt receipt = ReceiptBuilder.receipt()
	        		.withCreationDate(new Date())
	        		.withFileDirectory(r.getDirId())
	        		.withFileName(r.getFileName())
	        		.withUserInfo(user)
	        		.withType(UploadTypeEnum.RECEIPT.toString())
	        		.withProcesStatus(ProcesStatus.UPLOADED)
	        		.build();
	        receipt = receiptService.save(receipt);
    	}
    	return r;
    }

    @RequestMapping(value="/invoiceupload", method=RequestMethod.GET)
    public @ResponseBody String provideInvoiceUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }
    
    @RequestMapping(value="/invoiceupload", method=RequestMethod.POST)
    public @ResponseBody Result handleInvoiceUpload(@CurrentUser ActiveUser user, @RequestParam("cam") MultipartFile cam) throws Exception {
    	Result r = handleFileUpload(user, cam, UploadTypeEnum.INVOICE);
    	if (null!=r.getError() || r.getError().isEmpty()) {
    		//save invoice
    		Invoice invoice = InvoiceBuilder.invoice()
    				.withCreationDate(new Date())
	        		.withFileDirectory(r.getDirId())
	        		.withFileName(r.getFileName())
	        		.withUserInfo(user)
	        		.withType(UploadTypeEnum.INVOICE.toString())
	        		.withProcesStatus(ProcesStatus.UPLOADED)
	        		.build();
    		invoice = invoiceService.save(invoice);
    	}
    	return r;
    }
    
    private Result handleFileUpload(ActiveUser user, MultipartFile cam, UploadTypeEnum uploadType) throws Exception {
    	if (null == cam || cam.isEmpty()) {
    		throw new UploadException("You failed to upload");
    	}
    	String name = cam.getOriginalFilename().replaceAll(" ", "_"); //no spaces in fileName
    	String contentType = cam.getContentType();
    	Result r;
    	byte[] bytes = null;
		try {
			bytes = cam.getBytes();
		} catch (IOException e1) {
			LOGGER.error("Error fetching multipartfile", e1);
		}
        if (null == bytes || 0 == bytes.length) {
        	throw new UploadException("You failed to upload " + name + ": no size.");
        }
        else if (uploadType.getMaxUploadLimit() <= bytes.length) {
        	throw new UploadException("You failed to upload " + name + ": exceeds max size.");
        }

    	if(Arrays.asList(uploadType.getSupportedMediaTypes()).contains(contentType)) {
	    	String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_UPLOAD_PATH);
	    	LOGGER.info("fileName: " + name);
	    	String uid = UUID.randomUUID().toString();
	    	String userDir = user.getUserId() + File.separator + uploadType.toString() + File.separator + uid;
	    	Path userPath = Paths.get(path + File.separator + userDir);
	    	try {
	    		LOGGER.info("userPath: " + userPath);
	    		userPath = Files.createDirectories(userPath);
			} catch (IOException e) {
				LOGGER.error("Error creating userDir: " + userPath.getFileName(), e);
				throw new UploadException("Something went wrong.");
			}

	            try {
	                
	                Path filePath = Paths.get(userPath + File.separator + name);
	                Files.write(filePath, bytes, StandardOpenOption.CREATE);
	                if (Arrays.asList(uploadType.getSupportedMediaTypes()).contains(Files.probeContentType(filePath))) {
	                	r = new Result();
		                r.setMessage("Upload success!!");
		                r.setDirId(uid);
		                r.setFileName(name);
		                r.setError("");
		                return r;
	                }
	                else {
	                	throw new UploadException("You failed to upload " + name + " because the file type is not supported (indepth check)");
	                }
	                
	            } catch (Exception e) {
	            	throw new UploadException("You failed to upload " + name + " ->" + e.getMessage());
	            }
	        }

    	else {
    		throw new UploadException("You failed to upload " + name + " because the file type " + contentType + " is not supported.");
    	}
    }

    @RequestMapping(value="/invoiceupload_test", method=RequestMethod.GET)
    public @ResponseBody Result test() {
    	String scriptPath = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_PATH);
    	String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_UPLOAD_PATH);
        return scriptService.runScript(scriptPath + "pdftotiff.sh " + path + "/testpdf/pdf-test.pdf");
    }

}
