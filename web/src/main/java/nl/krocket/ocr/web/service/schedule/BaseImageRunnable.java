package nl.krocket.ocr.web.service.schedule;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.messaging.simp.SimpMessagingTemplate;

import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.web.backing.ProcessState;
import nl.krocket.ocr.web.backing.ProcessStatus;
import nl.krocket.ocr.web.service.db.AddressService;
import nl.krocket.ocr.web.service.db.BaseImageService;
import nl.krocket.ocr.web.service.db.CompanyService;
import nl.krocket.ocr.web.service.db.OcrWordService;
import nl.krocket.ocr.web.service.db.PaymentRowService;
import nl.krocket.ocr.web.service.other.PropertyService;
import nl.krocket.ocr.web.service.script.ScriptService;

public abstract class BaseImageRunnable implements Runnable, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	BaseImage nextImage;
	BaseImageService baseImageService;
	PropertyService propertyService;
	ScriptService scriptService;
	CompanyService companyService;
	AddressService addressService;
	PaymentRowService paymentRowService;
	OcrWordService ocrWordService;
	
	SimpMessagingTemplate template;
	
	Integer state = ProcessState.STATE_NOT_STARTED;
	private AtomicInteger status = new AtomicInteger(0);
	
	/**
	 * Updates status/progress to registered clients.
	 * Javascript client code for subscribing to serverside messages:
	 * stompClient.subscribe('/topic/progress', callBackFuntion);
	 * 
	 * @param updateBy
	 * @param message
	 */
	public void updateStatus(int updateBy, String message, String label) {
		if (0==updateBy) {
			status.set(0);
		}
		ProcessStatus processStatus = new ProcessStatus();
		processStatus.setMessage(message);
		processStatus.setPrecentComplete(status.addAndGet(updateBy));
		processStatus.setId(this.getProcessId());
		processStatus.setLabel(label);
		template.convertAndSend("/topic/progress/"+nextImage.getUserInfo().getUserId(), processStatus);
	}
	
	@Override
	public void run() {
		this.run(nextImage);
	}
	
	public Long getProcessId() {
		return nextImage.getBaseImageId();
	}
	
	public Integer getState() {
		return state;
	}
	
	public void setState(Integer state) {
		this.state = state;
	}

	public void setTemplate(SimpMessagingTemplate template) {
		this.template = template;
	}
	
	public abstract void run(BaseImage nextImage);
	
	public BaseImage updateProcesStatus(BaseImage image, ProcesStatus status) {
		image.setProcesStatus(status);
		image = baseImageService.save(image);
		return image;
	}
	
	public BaseImage updateFileName(BaseImage image, String fileName) {
		image.setFileName(fileName);
		image = baseImageService.save(image);
		return image;
	}
	

	/**
	 * @return the nextImage
	 */
	public BaseImage getNextImage() {
		return nextImage;
	}

	/**
	 * @param nextImage the nextImage to set
	 */
	public void setNextImage(BaseImage nextImage) {
		this.nextImage = nextImage;
	}

	/**
	 * @return the baseImageService
	 */
	public BaseImageService getBaseImageService() {
		return baseImageService;
	}

	/**
	 * @param baseImageService the baseImageService to set
	 */
	public void setBaseImageService(BaseImageService baseImageService) {
		this.baseImageService = baseImageService;
	}

	/**
	 * @return the propertyService
	 */
	public PropertyService getPropertyService() {
		return propertyService;
	}

	/**
	 * @param propertyService the propertyService to set
	 */
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

	/**
	 * @return the scriptService
	 */
	public ScriptService getScriptService() {
		return scriptService;
	}

	/**
	 * @param scriptService the scriptService to set
	 */
	public void setScriptService(ScriptService scriptService) {
		this.scriptService = scriptService;
	}

	/**
	 * @return the companyService
	 */
	public CompanyService getCompanyService() {
		return companyService;
	}

	/**
	 * @param companyService the companyService to set
	 */
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	/**
	 * @return the addressService
	 */
	public AddressService getAddressService() {
		return addressService;
	}

	/**
	 * @param addressService the addressService to set
	 */
	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}

	/**
	 * @return the paymentRowService
	 */
	public PaymentRowService getPaymentRowService() {
		return paymentRowService;
	}

	/**
	 * @param paymentRowService the paymentRowService to set
	 */
	public void setPaymentRowService(PaymentRowService paymentRowService) {
		this.paymentRowService = paymentRowService;
	}

	/**
	 * @return the ocrWordService
	 */
	public OcrWordService getOcrWordService() {
		return ocrWordService;
	}

	/**
	 * @param ocrWordService the ocrWordService to set
	 */
	public void setOcrWordService(OcrWordService ocrWordService) {
		this.ocrWordService = ocrWordService;
	}
	
	

}
