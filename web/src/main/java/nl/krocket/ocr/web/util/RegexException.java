package nl.krocket.ocr.web.util;

public class RegexException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public RegexException(String message) {
		super(message);
	}
	
	

}
