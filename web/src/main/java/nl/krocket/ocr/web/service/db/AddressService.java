package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.Address;

public interface AddressService {

	/**
	 * Finds all addresses
	 * @return
	 */
	public Iterable<Address> findAll();
	
	/**
	 * Gets receipt based on given addressId
	 * @param receiptId
	 * @return
	 */
	public Address get(Long addressId);
	
	/**
	 * Saves given address object
	 * @param receipt
	 * @return
	 */
	public Address save(Address address);
	
	/**
	 * Deletes given address object
	 * @param receipt
	 */
	public void delete(Address address);
	
	/**
	 * Gets count of rows
	 * @return
	 */
	public Long getCount();
	
}
