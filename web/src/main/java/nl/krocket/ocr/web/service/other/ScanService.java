package nl.krocket.ocr.web.service.other;

import java.io.File;
import java.net.URI;
import java.util.Base64;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

@Service
public class ScanService {
	
	private static String proxy_url = "proxy01.lnv.intern";
	private static int proxy_port = 8080;
	private static String application_id = "scan_receipts";
	private static String application_pass = "NZCglwSJb1i8nnh9CiHq5Y3S";
	
	public String scan(File file) throws Exception  {
		String result = "";
		if (file.exists()) {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {

            HttpHost proxy = new HttpHost(proxy_url, proxy_port, "http");

            RequestConfig config = RequestConfig.custom()
                    .setProxy(proxy)
                    .build();
            
            HttpEntity entity = MultipartEntityBuilder.create()
            		.addBinaryBody("name", file, ContentType.APPLICATION_OCTET_STREAM, file.getName()).build();
            
            HttpUriRequest postImage = RequestBuilder.post()
                    .setUri(new URI("https://cloud.ocrsdk.com/processImage"))
                    .addHeader("Authorization", getAuth())
                    .addParameter("language", "Dutch")
                    .addParameter("profile", "textExtraction")
                    .addParameter("textType", "normal")
                    .addParameter("imageSource", "photo")
                    .addParameter("correctOrientation", "true")
                    .addParameter("correctSkew", "true")
                    .addParameter("readBarcodes", "false")
                    .addParameter("exportFormat", "txt")
                    .setEntity(entity)
                    .setConfig(config)
                    .build();
            

            System.out.println("Executing request " + postImage.getRequestLine() + " to https://cloud.ocrsdk.com/processImage via " + proxy);

            CloseableHttpResponse response = httpclient.execute(postImage);
            try {
                System.out.println("----------------------------------------");
                System.out.println(response.getStatusLine());
                result = EntityUtils.toString(response.getEntity());
                System.out.println();
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
		}
		return result;
	}

	public String getAuth() throws Exception {
		String toEncode = application_id + ":" + application_pass;
		byte[] isoEncoded = toEncode.getBytes("iso-8859-1");
		String baseEncoded = Base64.getEncoder().encodeToString(isoEncoded);
		return "Basic " + baseEncoded;		
	}
	
}
