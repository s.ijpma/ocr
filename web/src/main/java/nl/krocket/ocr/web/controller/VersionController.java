package nl.krocket.ocr.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.servlet.http.HttpServletRequest;

import nl.krocket.ocr.web.backing.NameValuePair;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller returns version information obtained from MANIFEST file
 * @author Sander
 *
 */
@Controller
public class VersionController {
	
	private static final Logger LOGGER = Logger.getLogger(VersionController.class);
	
	@Autowired 
	private HttpServletRequest httpServletRequest;
	private Manifest manifest = null;

    private static final String MANIFEST_FILE = "/META-INF/MANIFEST.MF";
	
	@RequestMapping(value = "/get/version", method = RequestMethod.GET)
	public @ResponseBody List<NameValuePair> getManifest() {
		if(manifest == null) {
			initManifest();
		}
		Attributes attributes = manifest.getMainAttributes();
		Iterator<Entry<Object, Object>> it = attributes.entrySet().iterator();
		List<NameValuePair> lst = new ArrayList<NameValuePair>();
		while (it.hasNext()) { 
			NameValuePair nvp = new NameValuePair();
			Entry<Object, Object> entry = it.next();
			nvp.setName(entry.getKey().toString());
			nvp.setValue(entry.getValue().toString());
			lst.add(nvp);
		}

		return lst;
	}

	/**
	 * Specify a custom manifest file for unit testing.
	 */
	public void setManifest(Manifest manifest) {
		this.manifest = manifest;
	}

	/**
	 * Initialize manifest file using the manifest from the servlet context.
	 */
	private void initManifest(){
		try(InputStream is = httpServletRequest.getServletContext().getResourceAsStream(MANIFEST_FILE)) {
			this.manifest = new Manifest(is);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}

}

