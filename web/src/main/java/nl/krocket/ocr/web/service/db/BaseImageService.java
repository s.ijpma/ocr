package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;

import org.springframework.data.domain.Page;

public interface BaseImageService {

	/**
	 * Finds all receipts for given user
	 * @param userInfo
	 * @return
	 */
	public Iterable<BaseImage> findAllByUserInfo(UserInfo userInfo);
	
	/**
	 * Finds all receipts for given user
	 * @param user
	 * @param sort
	 * @param order
	 * @param limit
	 * @param offset
	 * @return
	 */
	public Page<BaseImage> findAllByUserInfo(UserInfo user, String sort, String order, int limit, int offset);
	
	/**
	 * Finds all receipts for given procesStatus
	 * @param status
	 * @return
	 */
	public Iterable<BaseImage> findAllByProcesStatus(ProcesStatus status);
	
	/**
	 * Finds all receipts
	 * @return
	 */
	public Iterable<BaseImage> findAll();
	
	/**
	 * Gets receipt based on given receiptId
	 * @param receiptId
	 * @return
	 */
	public BaseImage get(Long receiptId);
	
	/**
	 * Saves given receipt object
	 * @param receipt
	 * @return
	 */
	public BaseImage save(BaseImage receipt);
	
	/**
	 * Deletes given receipt object
	 * @param receipt
	 */
	public void delete(BaseImage receipt);
	
	/**
	 * Gets count of rows
	 * @return
	 */
	public Long getCount();
}
