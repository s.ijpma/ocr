package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.web.backing.ActiveUser;

public interface InvoiceService {

	/**
	 * Finds all invoices
	 * @param user 
	 * @return
	 */
	public Iterable<Invoice> findAll();
	
	/**
	 * Gets invoice based on given invoiceId
	 * @param invoiceId
	 * @return
	 */
	public Invoice get(Long invoiceId);
	
	/**
	 * Saves given invoice object
	 * @param invoice
	 * @return
	 */
	public Invoice save(Invoice invoice);
	
	/**
	 * Deletes given invoice object
	 * @param invoice
	 */
	public void delete(Invoice invoice);

	/**
	 * Deletes invoice object based on given baseImageId
	 * @param baseImageId
	 */
	public void delete(Long baseImageId);
	
	/**
	 * Finds all invoices for given user
	 * @param user
	 * @return
	 */
	public Iterable<Invoice> findAllByUserInfo(ActiveUser user);

	/**
	 * Gets count of rows
	 * @return
	 */
	public Long getCount();
	
}
