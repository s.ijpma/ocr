package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.repositories.AddressRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {
	
	@Autowired
	private AddressRepository repository;

	@Override
	public Iterable<Address> findAll() {
		return repository.findAll();
	}

	@Override
	public Address get(Long addressId) {
		return repository.findOne(addressId);
	}

	@Override
	public Address save(Address address) {
		return repository.save(address);
	}

	@Override
	public void delete(Address address) {
		repository.delete(address);
	}

	@Override
	public Long getCount() {
		return repository.count();
	}

}
