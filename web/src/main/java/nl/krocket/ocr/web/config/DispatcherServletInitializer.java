package nl.krocket.ocr.web.config;

import javax.servlet.Filter;
import javax.servlet.ServletRegistration;

import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import nl.krocket.ocr.web.config.websocket.WebSocketConfiguration;
import nl.krocket.ocr.web.config.websocket.WebSocketSecurityConfig;

public class DispatcherServletInitializer extends
		AbstractAnnotationConfigDispatcherServletInitializer {
	
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { SecurityConfiguration.class, WebMvcConfiguration.class, WebSocketConfiguration.class, WebSocketSecurityConfig.class, JpaConfiguration.class, DataAccessConfiguration.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
	
	@Override
	protected Filter[] getServletFilters() {
		return new Filter[] { new HiddenHttpMethodFilter()};
	}

	@Override
	protected void customizeRegistration(ServletRegistration.Dynamic registration) {
		registration.setInitParameter("throwExceptionIfNoHandlerFound","true");
		registration.setInitParameter("dispatchOptionsRequest", "true");
	    registration.setAsyncSupported(true);
	}

}
