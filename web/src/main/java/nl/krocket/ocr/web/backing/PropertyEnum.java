package nl.krocket.ocr.web.backing;

public enum PropertyEnum {
	//system property names
	PROPERTY_FILE_LOCATION("krocket.propertyfile.location"),
	PROPERTY_FILE_NAME("application.properties"),
	PROPERTY_DB_FILE_NAME("database.properties"),
	//application property names
	APPLICATION_AUTHENTICATION_TYPE("ocr.web.config.authentication.type"),
	APPLICATION_AUTHENTICATION_IN_MEM("ocr.web.config.authentication.inmem"),
	APPLICATION_AUTHENTICATION_SERVICE("ocr.web.config.authentication.service"),
	APPLICATION_UPLOAD_PATH("ocr.web.config.upload.path"),
	APPLICATION_SCRIPT_PATH("ocr.web.config.script.path"),
	APPLICATION_SCRIPT_TEXTCLEAN("ocr.web.config.script.textcleaner"),
	APPLICATION_SCRIPT_PDFCONVERT("ocr.web.config.script.pdfconverter"),
	APPLICATION_SCRIPT_RESIZE("ocr.web.config.script.resize"),
	APPLICATION_SCRIPT_ORIENTATION("ocr.web.config.script.orientation"),
	APPLICATION_SCRIPT_ROTATION("ocr.web.config.script.rotation"),
	//customer property names
	CUSTOMER_COMPANY_NAME("ocr.web.config.customer.company.name"),
	CUSTOMER_COMPANY_ADDRESS("ocr.web.config.customer.company.address"),
	//db property names
	DATABASE_DRIVER("db.driver"),
	DATABASE_URL("db.url"),
	DATABASE_USERNAME("db.username"),
	DATABASE_PASSWORD("db.password"),
	//hibernate property names
	HIBERNATE_DIALECT("hibernate.dialect"),
	HIBERNATE_SESSION("hibernate.hbm2ddl.auto"),
	HIBERNATE_NAMING_STRATEGY("hibernate.ejb.naming_strategy"),
	HIBERNATE_SHOW_SQL("hibernate.show_sql"),
	HIBERNATE_FORMAT_SQL("hibernate.format_sql"); 
	
	
	private String value;
	
	PropertyEnum(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return this.value;
	}

}
