package nl.krocket.ocr.web.service.db;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.repositories.BaseImageRepository;
import nl.krocket.ocr.web.backing.ActiveUser;
import nl.krocket.ocr.web.backing.graph.PieChart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GraphServiceImpl implements GraphService {

	@Autowired
	private BaseImageRepository baseImageRepository;
	
	@Override
	public Iterable<PieChart> countByProcesStatus(ActiveUser user, String type) {
		Iterable<Object[]> result = baseImageRepository.countProcessStatusGroupByProcesStatus(user, type);
		Set<PieChart> r = new HashSet<PieChart>();
		for (Object[] object : result) {
			PieChart p = new PieChart();
			p.setValue(Integer.parseInt(object[0].toString()));
			p.setLabel(object[1].toString().toLowerCase());
			List<String> colors = getProcessColors(ProcesStatus.valueOf(object[1].toString()));
			p.setColor(colors.get(0));
			p.setHighlight(colors.get(1));
			r.add(p);
		}
		return r;
	}

	@Override
	public Integer getCount(ActiveUser user, String type) {
		return baseImageRepository.countAllByUserInfoAndType(user, type);
	}
	
	private List<String> getProcessColors(ProcesStatus process) {
		if (process.equals(ProcesStatus.ACCEPTED)) {
			return Arrays.asList("#0A1036","#1B2451");
		}
		else if (process.equals(ProcesStatus.DENIED)) {
			return Arrays.asList("#464E7D","#6B729A");
		}
		else if (process.equals(ProcesStatus.PROCESSING)) {
			return Arrays.asList("#1C0835","#311850");
		}
		else if (process.equals(ProcesStatus.UNKNOWN)) {
			return Arrays.asList("#5C427C","#7D6798");
		}
		else if (process.equals(ProcesStatus.UPLOADED)) {
			return Arrays.asList("#3E0526","#5E1741");
		}
		else if (process.equals(ProcesStatus.WAITING_FOR_APPROVAL)) {
			return Arrays.asList("#914873","#B37499");
		}
		else {
			return Arrays.asList("#4E1606","#76301C");
		}
	}

}
