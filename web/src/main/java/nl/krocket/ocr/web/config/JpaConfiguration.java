package nl.krocket.ocr.web.config;

import java.util.Properties;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.service.other.PropertyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"nl.krocket.ocr.core.repositories"} )
public class JpaConfiguration {

	@Autowired
	PropertyService propertyService;

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        final LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        bean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        bean.setDataSource(dataSource());
        bean.setPackagesToScan("nl.krocket.ocr.core.model");
        final Properties props = new Properties();
        props.setProperty("hibernate.dialect", propertyService.getPropertyValue(PropertyEnum.HIBERNATE_DIALECT));
        props.setProperty("hibernate.hbm2ddl.auto", propertyService.getPropertyValue(PropertyEnum.HIBERNATE_SESSION));
        props.setProperty("hibernate.show_sql", propertyService.getPropertyValue(PropertyEnum.HIBERNATE_SHOW_SQL));
        props.setProperty("hibernate.format_sql", propertyService.getPropertyValue(PropertyEnum.HIBERNATE_FORMAT_SQL));

        bean.setJpaProperties(props);
        bean.afterPropertiesSet();
        return bean.getObject();
    }

    @Bean(destroyMethod = "") //destroyMethod to "" otherwise jdni-datasource has to be restarted after each re-deploy
    @Resource(name="jdbc/ocrDS")
    public DataSource dataSource() {
        final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
        dsLookup.setResourceRef(true);
        DataSource dataSource = dsLookup.getDataSource("jdbc/ocrDS");
        return dataSource;
    }
	
	@Bean
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
}
