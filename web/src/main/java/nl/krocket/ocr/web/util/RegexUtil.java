package nl.krocket.ocr.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.SupportedCurrency;
import nl.krocket.ocr.core.model.Vat;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.core.model.builders.PaymentRowBuilder;
	

public class RegexUtil {
	
	private static final Logger LOGGER = Logger.getLogger(RegexUtil.class.getCanonicalName());
	
	public static List<Company> getCompanies(String text) throws Exception {
		List<Company> companies=new ArrayList<Company>();
		Map<String, Address> addresses = getAddress(text);
		for (Map.Entry<String,Address> entry : addresses.entrySet()) {
			String addressString = entry.getKey();
			String companyName = getCompanyName(text, addressString);
			Company c = new CompanyBuilder()
				.withAccepted(false)
				.withAddedAddressSetElement(entry.getValue())
				.withCompanyName(companyName)
				.build();
			companies.add(c);
		}
		return companies;
	}
	
	public static String getCompanyName(String text, String addressString) {
		int indexOfAddress = text.indexOf(addressString);
		int indexOfCompanyName = text.lastIndexOf(System.lineSeparator(), indexOfAddress - (System.lineSeparator().length()+1)) + (System.lineSeparator().length());
		return text.substring(indexOfCompanyName, indexOfAddress-(System.lineSeparator().length()));
	}
	
	public static Map<String, Address> getAddress(String text) throws Exception {
		String houseNrPattern = "[1-9]{1}\\d{0,5}(—|-| )?[0-9a-zA-Z]?";
		String streetPattern = "[A-Z]{1}[a-z]{1,50}";
		String postCodePattern = "[1-9]{1}\\d{3} ?[a-zA-Z]{2}";
		String cityPattern = "('s )?[A-Z]{1}[a-z]{1,50} ?[A-Z]?[a-z]{1,50}";
		String combined = "(" + streetPattern + " " + houseNrPattern + System.lineSeparator() + postCodePattern + ",? " + cityPattern + ")";
		Pattern r = Pattern.compile(combined);
		Matcher m = r.matcher(text);
		Map<String, Address> result = new HashMap<String, Address>();
		while (m.find()) {
			String addressString = m.group();
			Address address = new AddressBuilder()
					.withAccepted(false)
					.withCountryCode("NL")
					.withStreetName(getMatch(streetPattern, getMatch(streetPattern + " " + houseNrPattern, addressString)))
					.withHouseNr(getMatch(houseNrPattern, addressString))
					.withPostcode(getMatch(postCodePattern, addressString))
					.withCity(getMatch(cityPattern, getMatch(postCodePattern + ",? " + cityPattern, addressString)))
					.build();
			
			result.put(addressString, address);
		}
		return result;
	}
	
	public static List<String> getPossibleCompanyNames(String text) throws Exception {
		String companyNamePattern = "[a-zA-Z]{2,50} ?(([a-zA-Z]\\W){0,2})?";
		Pattern r = Pattern.compile(companyNamePattern);
		Matcher m = r.matcher(text);
		List<String> result = new ArrayList<String>();
		while (m.find()) {
			String possibleCompanyName = m.group();
			result.add(possibleCompanyName);
		}
		return result;
	}
	
	private static String getMatch(String pattern, String text) {
		Pattern r = Pattern.compile("(" + pattern + ")");
		Matcher m = r.matcher(text);
		String result = "";
		if (m.find()) {
			result = m.group();
		}
		return result;
	}
	
	public static Set<PaymentRow> findTotalPaymentRow(String text) {
		String totalPart = "(Totaal|totaal|TOTAAL|TOTAL|Total)";
		String currencyPart = "(Euro|euro|EUR|eur|[€eE$]|)";
		String firstNumber = "[1-9]{1}";
		String numberPart = "[.,]{0,1}\\d{0,3}";
		String decimalPart = "([.,]{1,1})(\\d{1,2}|\\-)";
		String pattern1 = currencyPart + " *(" + firstNumber + numberPart + numberPart + decimalPart + ")";
		String pattern2 =  "(" + firstNumber + numberPart + numberPart + decimalPart + ")" + " *" + currencyPart;
		String pattern = "(" + totalPart + " *)(" + pattern1 + "|" + pattern2 + ")";
		Set<PaymentRow> result = findPaymentRows(text, pattern);
		result.forEach(RegexUtil::doSetTotal);
		return result;
	}
	
	private static void doSetTotal(PaymentRow row) {
		row.setName("Totaal");
	}
	
	public static Set<PaymentRow> findNormalPaymentRow(String text) {
		String currencyPart = "([€eE$])";
		String firstNumber = "[1-9]{1}";
		String numberPart = "[.,]{0,1}\\d{0,3}";
		String decimalPart = "([.,]{1,1})(\\d{1,2}|\\-)";
		String pattern = currencyPart + " *(" + firstNumber + numberPart + numberPart + decimalPart + ")";
		String negativeLookBehind = "(?!.*" + pattern + ")"; //only allow last price in line
		return findPaymentRows(text, pattern + negativeLookBehind);
	}
	
	public static Set<PaymentRow> findPaymentRows(String text, String pattern) {
		Pattern r = Pattern.compile(pattern);
		text = text.trim();
		Matcher m = r.matcher(text);
		Set<PaymentRow> rows = new HashSet<>();
		while (m.find()) {
			String group = m.group();
			SupportedCurrency currency = parseCurrency(group);
			Double value = new Double(0);
			try {
				value = parseDecimalValue(group);
			} catch (RegexException e) {
				LOGGER.warn("Could not parse decimal value from: " + group);
			}
			logDebugMessage("group: " + group + " currency: " + currency + " value: " + value);
			int groupIndex = text.indexOf(group);
			int lineIndex = text.lastIndexOf(System.lineSeparator(), groupIndex);
			
			if (lineIndex == -1) {
				lineIndex = 0;
			}
			String description = text.substring(lineIndex, text.indexOf(group)).trim();
			PaymentRow row = new PaymentRowBuilder()
					.withValue(value)
					.withCurrency(currency)
					.withName(description).build();
				rows.add(row);
		}
		return rows;
	}
	
	private static SupportedCurrency parseCurrency(String group) {
		String currencyChar = group.substring(0, 1);
		for (SupportedCurrency sc : EnumSet.allOf(SupportedCurrency.class)) {
			if (Arrays.asList(sc.getCurrencyChars()).contains(currencyChar)) {
				return sc;
			}
		}
		return SupportedCurrency.getDefault();
	}

	private static void logDebugMessage(String message) {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(message);
		}
	}

	public static Double parseDecimalValue(String group) throws RegexException{
		logDebugMessage("group: " + group);
		List<String> decimalSeperators = Arrays.asList(".",",");
		group = group.toLowerCase();
		group = group.replace("totaal", "").replace("total", "");
 		group = group.replace("euro", "").replace("eur", "").replace("e", "").replace("€", "").replace("$", "");
		group = group.trim();
		group = group.replace(",-", ".00"); //replace ',-' to prevent negatives;
		group = group.replace(".-", ".00"); //replace '.-' to prevent negatives;
		
		//get decimal seperator if there is one
		String decimalSeperator = group.substring(group.length() - 2, group.length() - 1); //if we have one decimal
		logDebugMessage("decimalSeperator: " + decimalSeperator);
		if (decimalSeperator.isEmpty() || !decimalSeperators.contains(decimalSeperator)) {
			decimalSeperator = group.substring(group.length() - 3, group.length() -2); //if we have two decimals
			logDebugMessage("decimalSeperator: " + decimalSeperator);
			if (decimalSeperator.isEmpty() || !decimalSeperators.contains(decimalSeperator)) {
				//no decimals, no decimal seperator
				decimalSeperator = "";
			}
		}
		String decimals = "";
		if (!decimalSeperator.isEmpty()) {
			decimals = group.substring(group.lastIndexOf(decimalSeperator)+1, group.length());
			logDebugMessage("decimals: " + decimals);
			group = group.substring(0, group.lastIndexOf(decimalSeperator)).trim();
			logDebugMessage("group: " + group);
			group = group.replaceAll("\\.", ""); //replace all grouping seperators '.'
			group = group.replaceAll(",", ""); //replace all grouping seperators ','
			group = group + "." + decimals;
			logDebugMessage("group: " + group);
		}
		else {
			group = group.replaceAll("\\.", ""); //replace all grouping seperators '.'
			group = group.replaceAll(",", ""); //replace all grouping seperators ','
		}
		
		logDebugMessage("group: " + group);
		try {
			Double result = Double.parseDouble(group);
			logDebugMessage("result: " + result);
			return result;
		}
		catch (NumberFormatException nf) {
			throw new RegexException("Unable to parse '" + group + "'. Error message: " + nf.getMessage());
		}
	}


	public static Vat getReceiptVat(String text) throws Exception{
		int ind = text.indexOf("%");
		if (ind>0) {
			String vat = text.substring(ind-3, ind);
			return Vat.enumOf(Integer.parseInt(vat.trim()));
		}
		else {
			throw new Exception("No parseable vat in text");
		}
	}
	
	public static Date getDateFromText(String text) throws Exception {
		String patrn = "([0-3]?[0-9].[0-3]?[0-9].(?:[0-9]{2})?[0-9]{2})";
		Pattern r = Pattern.compile(patrn);
		Matcher m = r.matcher(text);
		String date="";
		String error="";
		while (m.find()) {
			date = m.group();
			String[] patterns = {"dd-MM-yyyy", "dd/MM/yyyy", "dd.MM.yyyy"};
			for (String pattern : patterns) {
				SimpleDateFormat format = new SimpleDateFormat(pattern);
				try {
					return format.parse(date.trim());
	
				} catch (ParseException e) {
					error = e.getMessage();
				}
			}
		}
		throw new Exception("No parseable date in text. " + error);
	}

}
