package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.repositories.UserInfoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl implements UserInfoService {

	@Autowired
	UserInfoRepository userInfoRepository;
	
	@Override
	public Iterable<UserInfo> findAll() {
		return userInfoRepository.findAll();
	}

	@Override
	public UserInfo get(Long userId) {
		return userInfoRepository.findOne(userId);
	}

	@Override
	public UserInfo save(UserInfo userInfo) {
		return userInfoRepository.save(userInfo);
	}

	@Override
	public void delete(UserInfo userInfo) {
		userInfoRepository.delete(userInfo);
	}

	@Override
	public UserInfo get(String userName) {
		return userInfoRepository.findByUserName(userName);
	}

}
