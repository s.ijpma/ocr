package nl.krocket.ocr.web.backing;

import org.springframework.http.MediaType;

public enum UploadTypeEnum {
	
	RECEIPT("receipts", 3145728, MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE),
	INVOICE("invoices", 3145728, "application/pdf");
	
	private String name;
	private String[] supportedMediaTypes;
	private int maxUploadLimit;
	
	private UploadTypeEnum(String name, int maxUploadLimit, String... supportedMediaTypes) {
		this.name=name;
		this.supportedMediaTypes=supportedMediaTypes;
		this.maxUploadLimit=maxUploadLimit;
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String[] getSupportedMediaTypes() {
		return supportedMediaTypes;
	}


	public int getMaxUploadLimit() {
		return maxUploadLimit;
	}

}
