package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.web.backing.ActiveUser;
import nl.krocket.ocr.web.backing.graph.PieChart;

public interface GraphService {

	Iterable<PieChart> countByProcesStatus(ActiveUser user, String type);
	
	Integer getCount(ActiveUser user, String type);
}
