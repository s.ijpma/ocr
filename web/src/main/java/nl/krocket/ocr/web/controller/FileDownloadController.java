package nl.krocket.ocr.web.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import nl.krocket.ocr.web.backing.ActiveUser;
import nl.krocket.ocr.web.backing.CurrentUser;
import nl.krocket.ocr.web.backing.ImageNotFoundException;
import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.service.other.PropertyService;

@Controller
public class FileDownloadController {
	
	@Autowired
	PropertyService propertyService;

	@Autowired
	HttpServletResponse response;
	
	private static final Logger LOGGER = Logger.getLogger(FileDownloadController.class.getCanonicalName());

	@RequestMapping(value = "/get/image/{id}/{type}/{fileName}", method=RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public @ResponseBody byte[] showImageOnId(@CurrentUser ActiveUser user, @PathVariable("id") String id, @PathVariable("type") String type, @PathVariable("fileName") String fileName) {
		String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_UPLOAD_PATH);
		Path p = Paths.get(path, user.getUserId().toString(), type, id, fileName);
		return readBytesFromPath(p);
	}
	
	private byte[] readBytesFromPath(Path p) {
		try {
			return Files.readAllBytes(p);
		} catch (IOException e) {
			LOGGER.error("Error reading image:" + p.getFileName(), e);
			throw new ImageNotFoundException("Resource not found or broken: " + p.getFileName());
		} catch (Exception e) {
			LOGGER.error("Error reading image:" + p.getFileName(), e);
			throw new ImageNotFoundException("Resource not found or broken: " + p.getFileName());
		}
	}

	
}
