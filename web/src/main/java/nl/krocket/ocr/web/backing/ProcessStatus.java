package nl.krocket.ocr.web.backing;

import java.io.Serializable;

public class ProcessStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private int precentComplete;
	private String message;
	private String label;

	public int getPrecentComplete() {
		return precentComplete;
	}
	public void setPrecentComplete(int precentComplete) {
		this.precentComplete = precentComplete;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
}
