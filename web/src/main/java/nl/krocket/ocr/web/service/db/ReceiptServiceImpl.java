package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.repositories.ReceiptRepository;
import nl.krocket.ocr.web.backing.ActiveUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReceiptServiceImpl implements ReceiptService {
	
	@Autowired 
	ReceiptRepository repository;

	@Override
	public Receipt save(Receipt receipt) {
		return repository.save(receipt);
	}

	@Override
	public void delete(Receipt receipt) {
		repository.delete(receipt);
	}

	@Override
	public Iterable<Receipt> findAll() {
		return repository.findAll();
	}

	@Override
	public Receipt get(Long receiptId) {
		return repository.findOne(receiptId);
	}

	@Override
	public Iterable<Receipt> findAllByUserInfo(ActiveUser user) {
		return repository.findAllByUserInfo(user);
	}

	@Override
	public Long getCount() {
		return repository.count();
	}

	@Override
	public void delete(Long baseImageId) {
		repository.delete(baseImageId);
	}

}
