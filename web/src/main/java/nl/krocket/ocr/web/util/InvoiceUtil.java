package nl.krocket.ocr.web.util;

import java.util.HashSet;
import java.util.Set;

import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.model.OcrWord;
import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.web.service.db.AddressService;
import nl.krocket.ocr.web.service.db.CompanyService;
import nl.krocket.ocr.web.service.db.InvoiceService;
import nl.krocket.ocr.web.service.db.OcrWordService;
import nl.krocket.ocr.web.service.db.PaymentRowService;

import org.apache.log4j.Logger;

public class InvoiceUtil {
	
private static final Logger LOGGER = Logger.getLogger(InvoiceUtil.class.getCanonicalName());
	
	/**
	 * Only static, no instantiation
	 */
	private InvoiceUtil() {
	}
	
	public static Invoice save(Invoice invoice, InvoiceService invoiceService, CompanyService companyService, AddressService addressService, PaymentRowService receiptRowService, OcrWordService ocrWordService) {
		LOGGER.info("Saving invoice: " + invoice.getFileDirectory());
		
		Set<PaymentRow> paymentRows = new HashSet<PaymentRow>(invoice.getPaymentRows());
		Set<OcrWord> wordRows = new HashSet<OcrWord>(invoice.getOcrWords());
		
		invoice.setPaymentRows(null);
		invoice.setOcrWords(null);
		
		//save company and address if not yet saved before
		Company fromCompany = invoice.getFromCompany();
		if (null!=fromCompany) {
			if (null== fromCompany.getCompanyId() || 0L==fromCompany.getCompanyId()) {
				Set<Address> addresses = fromCompany.getAddressSet();
				fromCompany.setAddressSet(null);
				fromCompany = companyService.save(fromCompany);
				for (Address address : addresses) {
					address.setCompany(fromCompany);
					address = addressService.save(address);
					fromCompany.getAddressSet().add(address);
				}
				fromCompany = companyService.save(fromCompany);
				invoice.setFromCompany(fromCompany);
			}
		}
		//then save invoice
		invoice = invoiceService.save(invoice);

		//third save paymentrows with invoice constraint
		for (PaymentRow paymentRow : paymentRows) {
			//set constraint to row
			paymentRow.setBaseImage(invoice);
			//save row
			paymentRow = receiptRowService.save(paymentRow);
			//add saved row to invoice
			invoice.getPaymentRows().add(paymentRow);
		}
		//save invoice
		invoice = invoiceService.save(invoice);
		
		for (OcrWord ocrWord : wordRows) {
			ocrWord.setOcrImage(invoice);
			ocrWord = ocrWordService.save(ocrWord);
			invoice.getOcrWords().add(ocrWord);
			
		}
		invoice = invoiceService.save(invoice);
		return invoice;
	}
	
	public static String delete(String id, InvoiceService invoiceService) {
		long baseImageId = Long.parseLong(id);
		String fileDir = invoiceService.get(baseImageId).getFileDirectory();
		invoiceService.delete(baseImageId);
		return fileDir;
	}

}
