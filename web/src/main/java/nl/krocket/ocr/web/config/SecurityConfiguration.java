package nl.krocket.ocr.web.config;

import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.service.db.AuthenticationServiceImpl;
import nl.krocket.ocr.web.service.other.PropertyService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	private static final Logger LOGGER = Logger.getLogger(SecurityConfiguration.class.getCanonicalName());

	@Autowired
	PropertyService propertyService;
	
	@Autowired
	AuthenticationServiceImpl authenticationService;
	
	@Override
    public void configure(WebSecurity web) throws Exception {
		web
			.ignoring()
				.antMatchers("/images/**", "/js/**", "/css/**", "/fonts/**");
    }

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf()
				// ignore our stomp endpoints since they are protected using Stomp headers
				.ignoringAntMatchers("/progress/**")
				.and()
			.headers()
                // allow same origin to frame our site to support iframe SockJS
                .frameOptions().sameOrigin()
                .and()
			.authorizeRequests()
		        .anyRequest().authenticated()
		        .and()
			.formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/index")
                .failureUrl("/login?error=1")
                .permitAll()
                .and()
            .httpBasic()
            
            	.and()
            .logout()
				.logoutUrl("/logout")
				.logoutSuccessUrl("/login?logout=1")
				.permitAll();
	}

	@Autowired
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		String authenticationType = propertyService.getPropertyValue(PropertyEnum.APPLICATION_AUTHENTICATION_TYPE);
		if ("service".equalsIgnoreCase(authenticationType)) {
			String passwordEncoding = propertyService.getPropertyValue(PropertyEnum.APPLICATION_AUTHENTICATION_SERVICE.toString() + ".encoding");
			MessageDigestPasswordEncoder encoder;
			if (passwordEncoding.equalsIgnoreCase("MD5")) {
				encoder = new Md5PasswordEncoder();
			}
			else {
				encoder = new ShaPasswordEncoder();
			}
	        auth.userDetailsService(authenticationService).passwordEncoder(encoder);
		}
		else if ("ldap-test".equalsIgnoreCase(authenticationType)) {
			auth
			.ldapAuthentication()
				.userDnPatterns("uid={0},ou=people")
				.groupSearchBase("ou=groups")
				.contextSource().ldif("classpath:test-server.ldif");
		}
		else if ("ldap".equalsIgnoreCase(authenticationType)) {
			auth
			.ldapAuthentication()
				.userDnPatterns("uid={0},ou=people")
				.groupSearchBase("ou=groups")
				.contextSource().url("");
		}
		else {
			configureInMemUsersFromPropFile(auth);
		}
	}
	
	protected void configureInMemUsersFromPropFile(AuthenticationManagerBuilder auth) throws Exception {
		String users = propertyService.getPropertyValue(PropertyEnum.APPLICATION_AUTHENTICATION_IN_MEM.toString() + ".users");
		String[] userList = users.split(",");
		for (String user : userList) {
			String uPassword = propertyService.getPropertyValue(PropertyEnum.APPLICATION_AUTHENTICATION_IN_MEM.toString() + "." + user + ".password");
			String roles = propertyService.getPropertyValue(PropertyEnum.APPLICATION_AUTHENTICATION_IN_MEM.toString() + "." + user + ".roles");
			String[] roleList = roles.split(",");
			String passWordEncoding="{}";
			String password = uPassword;
			if (uPassword.indexOf("{") == 0 && uPassword.indexOf("}") == 4) {
				passWordEncoding = uPassword.substring(0, 5);
				password = uPassword.substring(5, uPassword.length());
			}
			LOGGER.info("Found inmem user: " + user + " with passwordEncoding: " + passWordEncoding + " Roles: " + roles);
			if ("{MD5}".equalsIgnoreCase(passWordEncoding)) {
				auth.inMemoryAuthentication().passwordEncoder(new Md5PasswordEncoder())
				.withUser(user)
				.password(password)
				.roles(roleList);
			}
			else if ("{SHA}".equalsIgnoreCase(passWordEncoding)) {
				auth.inMemoryAuthentication().passwordEncoder(new ShaPasswordEncoder())
				.withUser(user)
				.password(password)
				.roles(roleList);
			}
			else {
				auth.inMemoryAuthentication()
				.withUser(user)
				.password(password)
				.roles(roleList);
			}
		}
	}
	
}