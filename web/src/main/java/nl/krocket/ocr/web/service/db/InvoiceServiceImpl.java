package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.repositories.InvoiceRepository;
import nl.krocket.ocr.web.backing.ActiveUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired 
	InvoiceRepository repository;

	@Override
	public Iterable<Invoice> findAll() {
		return repository.findAll();
	}

	@Override
	public Invoice get(Long invoiceId) {
		return repository.findOne(invoiceId);
	}

	@Override
	public Invoice save(Invoice invoice) {
		return repository.save(invoice);
	}

	@Override
	public void delete(Invoice invoice) {
		repository.delete(invoice);
	}

	@Override
	public Iterable<Invoice> findAllByUserInfo(ActiveUser user) {
		return repository.findAllByUserInfo(user);
	}

	@Override
	public Long getCount() {
		return repository.count();
	}
	
	@Override
	public void delete(Long baseImageId) {
		repository.delete(baseImageId);
	}

}
