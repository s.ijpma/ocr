package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.web.backing.ActiveUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements UserDetailsService {

	@Autowired
	private UserInfoService userInfoService;
	
	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		UserInfo userInfo = userInfoService.get(userName);
		if (userInfo!=null && userInfo.getUserId()>0) {
			ActiveUser userDetails = new ActiveUser();
			userDetails.setRole(userInfo.getRole());
			userDetails.setPassword(userInfo.getPassword());
			userDetails.setUserName(userInfo.getUserName());
			userDetails.setUserId(userInfo.getUserId());
			return userDetails;
		}
		else {
			throw new UsernameNotFoundException("Username " + userName + " not found.");
		}
	}
	

}
