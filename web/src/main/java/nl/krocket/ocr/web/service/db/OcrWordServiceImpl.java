package nl.krocket.ocr.web.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.krocket.ocr.core.model.OcrWord;
import nl.krocket.ocr.core.repositories.OcrWordRepository;

@Service
public class OcrWordServiceImpl implements OcrWordService {
	
	@Autowired
	OcrWordRepository repository;

	@Override
	public OcrWord get(Long ocrWordId) {
		return repository.findOne(ocrWordId);
	}

	@Override
	public OcrWord save(OcrWord ocrWord) {
		return repository.save(ocrWord);
	}

	@Override
	public void delete(OcrWord ocrWord) {
		repository.delete(ocrWord);
		
	}

	@Override
	public Long getCount() {
		return repository.count();
	}

}
