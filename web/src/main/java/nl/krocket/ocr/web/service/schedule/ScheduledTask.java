package nl.krocket.ocr.web.service.schedule;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.util.CollectionsUtil;
import nl.krocket.ocr.web.service.db.AddressService;
import nl.krocket.ocr.web.service.db.BaseImageService;
import nl.krocket.ocr.web.service.db.CompanyService;
import nl.krocket.ocr.web.service.db.InvoiceService;
import nl.krocket.ocr.web.service.db.OcrWordService;
import nl.krocket.ocr.web.service.db.PaymentRowService;
import nl.krocket.ocr.web.service.db.ReceiptService;
import nl.krocket.ocr.web.service.other.PropertyService;
import nl.krocket.ocr.web.service.parse.InvoiceParserService;
import nl.krocket.ocr.web.service.parse.ReceiptParserService;
import nl.krocket.ocr.web.service.script.ScriptService;

@Component
public class ScheduledTask {

	private static final Logger LOGGER = Logger.getLogger(ScheduledTask.class.getCanonicalName());
	
	@Autowired
	@Qualifier(value="taskExecutor")
	private ThreadPoolTaskExecutor executor;
	
	@Autowired
	private SimpMessagingTemplate template;
	
	@Autowired
	private PropertyService propertyService;
	@Autowired
	private ScriptService scriptService;
	
	@Autowired
	private ReceiptParserService receiptParserService;
	@Autowired
	private InvoiceParserService invoiceParserService;
	
	@Autowired
    private BaseImageService baseImageService;
	
	@Autowired
    private ReceiptService receiptService;

	@Autowired
    private PaymentRowService paymentRowService;
	
	@Autowired
	private OcrWordService ocrWordService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Scheduled(fixedRate = 5000)
    public void run() {
		Iterable<BaseImage> list = baseImageService.findAllByProcesStatus(ProcesStatus.UPLOADED);
		BaseImage nextImage;
		if (CollectionsUtil.getSize(list)>0) {
			nextImage = list.iterator().next();
			LOGGER.info("Processing next available image: " + nextImage.getClass().getCanonicalName());
			if (nextImage.getClass().getCanonicalName().equalsIgnoreCase(Receipt.class.getCanonicalName())) {
				ReceiptRunnable receiptRunnable = new ReceiptRunnable();
				//set template
				receiptRunnable.setTemplate(template);
				//set services
				receiptRunnable.setAddressService(addressService);
				receiptRunnable.setBaseImageService(baseImageService);
				receiptRunnable.setCompanyService(companyService);
				receiptRunnable.setOcrWordService(ocrWordService);
				receiptRunnable.setPaymentRowService(paymentRowService);
				receiptRunnable.setPropertyService(propertyService);
				receiptRunnable.setScriptService(scriptService);
				//set receipt services
				receiptRunnable.setReceiptParserService(receiptParserService);
				receiptRunnable.setReceiptService(receiptService);
				//set image
				receiptRunnable.setNextImage(nextImage);
				executor.execute(receiptRunnable);
			}
			else if (nextImage.getClass().getCanonicalName().equalsIgnoreCase(Invoice.class.getCanonicalName())) {
				InvoiceRunnable invoiceRunnable = new InvoiceRunnable();
				//set template
				invoiceRunnable.setTemplate(template);
				//set services
				invoiceRunnable.setAddressService(addressService);
				invoiceRunnable.setBaseImageService(baseImageService);
				invoiceRunnable.setCompanyService(companyService);
				invoiceRunnable.setOcrWordService(ocrWordService);
				invoiceRunnable.setPaymentRowService(paymentRowService);
				invoiceRunnable.setPropertyService(propertyService);
				invoiceRunnable.setScriptService(scriptService);
				//set invoice services
				invoiceRunnable.setInvoiceParserService(invoiceParserService);
				invoiceRunnable.setInvoiceService(invoiceService);
				//set image
				invoiceRunnable.setNextImage(nextImage);
				executor.execute(invoiceRunnable);
			}
			else {
				LOGGER.warn("Unable to process image " + nextImage.getFileDirectory() + " of type: " + nextImage.getClass().getCanonicalName()); 
			}
		}
	}

}
