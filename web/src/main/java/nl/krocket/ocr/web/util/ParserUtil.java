package nl.krocket.ocr.web.util;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import nl.krocket.ocr.backing.ScannedWord;
import nl.krocket.ocr.core.model.OcrWord;
import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.Vat;
import nl.krocket.ocr.core.model.builders.OcrWordBuilder;
import nl.krocket.ocr.web.service.parse.ReceiptParserService;

public class ParserUtil {
	
	private static final Logger LOGGER = Logger.getLogger(ReceiptParserService.class.getCanonicalName());
	
	/**
	 * Only static, no instantiation
	 */
	private ParserUtil() {
		
	}
	
	public static Set<OcrWord> getOcrWords(List<ScannedWord> words) {
		Set<OcrWord> result = new HashSet<OcrWord>();
		for (ScannedWord scannedWord : words) {
			result.add(OcrWordBuilder.ocrWord()
					.withConfidence(scannedWord.getConfidence())
					.withWord(scannedWord.getWord())
					.build());
		}
		return result;
	}
	
	public static Set<PaymentRow> findPaymentRows(String scannedText) {
		Set<PaymentRow> rows = new HashSet<PaymentRow>();
		Set<PaymentRow> total = RegexUtil.findTotalPaymentRow(scannedText);
		
		rows.addAll(RegexUtil.findNormalPaymentRow(scannedText));
		return rows;
	}

	public static Vat getVat(String scannedText) {
		try {
			return RegexUtil.getReceiptVat(scannedText);
		} catch (Exception e) {
			LOGGER.warn(e.getMessage());
			return null;
		}
	}

	public static Date getDate(String scannedText) {
		try {
			return RegexUtil.getDateFromText(scannedText);
		} catch (Exception e) {
			LOGGER.warn(e.getMessage());
			return null;
		}
	}

}
