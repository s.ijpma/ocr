package nl.krocket.ocr.web.service.parse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.service.db.AddressService;
import nl.krocket.ocr.web.service.db.CompanyService;
import nl.krocket.ocr.web.service.other.PropertyService;
import nl.krocket.ocr.web.util.RegexUtil;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyParserService {
	
	private static final Logger LOGGER = Logger.getLogger(CompanyParserService.class.getCanonicalName());
	
	@Autowired
	PropertyService propertyService;
	
	@Autowired
	private CompanyService companyService;
	@Autowired
	private AddressService addressService;
	
	public Map<String, Company> parseForInvoice(String scannedText) throws Exception {
		List<Company> companies = RegexUtil.getCompanies(scannedText);
		LOGGER.info("companies from scannedText: " + companies.size());
		String ownCompanyName = propertyService.getPropertyValue(PropertyEnum.CUSTOMER_COMPANY_NAME);
		LOGGER.info("Own company: " + ownCompanyName);
		Company toCompany=null;
		Company fromCompany=null;
		for (Company possibleCompany : companies) {
			if (ownCompanyName.equalsIgnoreCase(possibleCompany.getCompanyName())) {
				toCompany = getOwnCompany(); //get from db!
				LOGGER.info("To company: " + toCompany.getCompanyName());
			}
			else {
				fromCompany = companyService.findDistinctByCompanyNameIgnoreCase(possibleCompany.getCompanyName()); //find company if in db
				if (null==fromCompany) {
					fromCompany = possibleCompany;
				}
				LOGGER.info("From company: " + fromCompany.getCompanyName());
			}
		}
		Map<String, Company> result = new HashMap<String, Company>();
		result.put("toCompany", toCompany);
		result.put("fromCompany", fromCompany);
		return result;
	}
	
	public Company parseForReceipt(String scannedText) throws Exception {
		Company company=null;
		List<String> possibleCompanies = RegexUtil.getPossibleCompanyNames(scannedText);
		for (String possibleCompany : possibleCompanies) {
			//get company from db
			company = getCompanyByName(possibleCompany.trim());
			if (null!=company) {
					return company;
			}
		}
		return company;
	}
	
	public Company getOwnCompany() {
		String ownCompanyName = propertyService.getPropertyValue(PropertyEnum.CUSTOMER_COMPANY_NAME);
		Company company = companyService.findDistinctByCompanyNameIgnoreCase(ownCompanyName);
		if (null==company) {
			String[] ownCompanyAddress = propertyService.getPropertyValue(PropertyEnum.CUSTOMER_COMPANY_ADDRESS).split(",");
			company = new CompanyBuilder()
						.withAccepted(true)
						.withCompanyName(ownCompanyName)
						.build();
			company = companyService.save(company);
			Address address = new AddressBuilder()
						.withAccepted(true)
						.withStreetName(ownCompanyAddress[0])
						.withHouseNr(ownCompanyAddress[1])
						.withPostcode(ownCompanyAddress[2])
						.withCity(ownCompanyAddress[3])
						.withCountryCode(ownCompanyAddress[4])
						.withCompany(company)
						.build();
			address = addressService.save(address);
			company.getAddressSet().add(address);
			company = companyService.save(company);
		}
		LOGGER.info("result from getting own company: " + company.getCompanyId());
		return company;
	}

	private Company getCompanyByName(String textLine) {
		if (!textLine.isEmpty()) {
			//try to find company name in database
			return companyService.findDistinctByCompanyNameIgnoreCase(textLine);
		}
		else {
			return null;
		}
	}

}
