package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.repositories.CompanyRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyServiceImpl implements CompanyService {
	
	@Autowired
	private CompanyRepository repository;

	@Override
	public Iterable<Company> findAll() {
		return repository.findAll();
	}

	@Override
	public Company get(Long companyId) {
		return repository.findOne(companyId);
	}

	@Override
	public Company save(Company company) {
		return repository.save(company);
	}

	@Override
	public void delete(Company company) {
		repository.delete(company);
	}

	@Override
	public Long getCount() {
		return repository.count();
	}

	@Override
	public Company findDistinctByCompanyNameIgnoreCase(String companyName) {
		return repository.findDistinctByCompanyNameIgnoreCase(companyName);
	}

	@Override
	public Iterable<Company> findByCompanyNameStartsWithIgnoreCase(
			String companyName) {
		return repository.findByCompanyNameStartsWithIgnoreCase(companyName);
	}

}
