package nl.krocket.ocr.web.controller;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nl.krocket.ocr.web.backing.ImageNotFoundException;
import nl.krocket.ocr.web.backing.Result;
import nl.krocket.ocr.web.backing.UploadException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class ExceptionController {
	
	private static final Logger LOGGER = Logger.getLogger(ExceptionController.class.getCanonicalName());
	private static final String DEFAULT_ERROR_VIEW = "/oops";
	
	@Autowired
	HttpServletRequest request;
	
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(value=HttpStatus.NOT_FOUND)
    public ModelAndView handleNoHandlerFoundException(NoHandlerFoundException ex) {
		LOGGER.error("------------> handleNoHandlerFoundException" + ex.getLocalizedMessage());
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("exception", ex);
		m.put("errorcode", "404");
		m.put("url", request.getRequestURL().toString());
		ModelAndView mav = new ModelAndView(DEFAULT_ERROR_VIEW, "model", m);
        return mav;
    }
	
	@ExceptionHandler(ImageNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public @ResponseBody byte[] handleImageNotFoundException(ImageNotFoundException e) {
		LOGGER.info("handle image not found: " + e.getMessage());
		String fileName = request.getSession().getServletContext().getRealPath("/images/no-image-found.jpg");
		Path p = Paths.get(fileName);
		return readBytesFromPath(p);
	}

	@ExceptionHandler(UploadException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    public  @ResponseBody Result handleSizeLimitExceededException(UploadException e) {
		LOGGER.info("handle UploadException : " + e.getMessage());
		Result r = new Result();
    	r.setError(e.getMessage());
        return r;
    }
	
	@ExceptionHandler(org.springframework.web.multipart.MaxUploadSizeExceededException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	 public  @ResponseBody Result handleMaxUploadSizeExceededException(org.springframework.web.multipart.MaxUploadSizeExceededException e) {
		LOGGER.info("handle MaxUploadSizeExceededException : " + e.getMessage());
		Result r = new Result();
    	r.setError("Filesize exceeds max.");
        return r;
    }
	
	private byte[] readBytesFromPath(Path p) {
		try {
			return Files.readAllBytes(p);
		} catch (IOException e) {
			LOGGER.error("Error reading image:" + p.getFileName(), e);
			throw new ImageNotFoundException("Resource not found or broken: " + p.getFileName());
		} catch (Exception e) {
			LOGGER.error("Error reading image:" + p.getFileName(), e);
			throw new ImageNotFoundException("Resource not found or broken: " + p.getFileName());
		}
	}

}
