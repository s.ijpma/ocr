package nl.krocket.ocr.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("nl.krocket.ocr.core.repositories")
@Import({JpaConfiguration.class})
public class DataAccessConfiguration {
	
}
