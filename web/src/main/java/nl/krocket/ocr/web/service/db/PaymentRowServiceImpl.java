package nl.krocket.ocr.web.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.repositories.PaymentRowRepository;

@Service
public class PaymentRowServiceImpl implements PaymentRowService {
	
	@Autowired
	PaymentRowRepository repository;

	@Override
	public Iterable<PaymentRow> findAll() {
		return repository.findAll();
	}

	@Override
	public PaymentRow get(Long receiptRowId) {
		return repository.findOne(receiptRowId);
	}

	@Override
	public PaymentRow save(PaymentRow receiptRow) {
		return repository.save(receiptRow);
	}

	@Override
	public void delete(PaymentRow receiptRow) {
		repository.delete(receiptRow);
	}

	@Override
	public Long getCount() {
		return repository.count();
	}

}
