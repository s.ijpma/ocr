package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.OcrWord;

public interface OcrWordService {
	
	/**
	 * Gets ocrWord object based on given ocrWordId
	 * @param ocrWordId
	 * @return
	 */
	public OcrWord get(Long ocrWordId);
	
	/**
	 * Saves given ocrWord object
	 * @param ocrImage
	 * @return
	 */
	public OcrWord save(OcrWord ocrWord);
	
	/**
	 * Deletes given ocrWord object
	 * @param ocrWord
	 */
	public void delete(OcrWord ocrWord);
	
	/**
	 * Gets count of rows
	 * @return
	 */
	public Long getCount();
}
