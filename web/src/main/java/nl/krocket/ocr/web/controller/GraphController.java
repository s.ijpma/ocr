package nl.krocket.ocr.web.controller;

import nl.krocket.ocr.web.backing.ActiveUser;
import nl.krocket.ocr.web.backing.CurrentUser;
import nl.krocket.ocr.web.backing.graph.PieChart;
import nl.krocket.ocr.web.service.db.GraphService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GraphController {

	@Autowired
	GraphService graphService;
	
	@RequestMapping(value="/get/graph/processtatus/{type}", method=RequestMethod.GET)
    public @ResponseBody Iterable<PieChart> getCountByProcesStatus(@CurrentUser ActiveUser user, @PathVariable("type") String type) {
		Iterable<PieChart> result = graphService.countByProcesStatus(user, type);
		return result;
    }
	
	@RequestMapping(value="/get/graph/count/{type}", method=RequestMethod.GET)
    public @ResponseBody Integer getCount(@CurrentUser ActiveUser user, @PathVariable("type") String type) {
		return graphService.getCount(user, type);
    }
}
