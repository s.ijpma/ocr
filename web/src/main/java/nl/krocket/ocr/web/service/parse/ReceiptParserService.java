package nl.krocket.ocr.web.service.parse;

import java.util.Date;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.model.builders.ReceiptBuilder;
import nl.krocket.ocr.web.backing.UploadTypeEnum;
import nl.krocket.ocr.web.util.ParserUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReceiptParserService {
	
	@Autowired
	private CompanyParserService companyParserService;

	public Receipt getReceiptParsed(ScannedImage image) throws Exception {
		String scannedText = image.getScannedText();
		Receipt receipt = new ReceiptBuilder()
				.withCreationDate(new Date())
				.withFileDirectory(image.getDir())
				.withFileName(image.getFileName())
				.withType(UploadTypeEnum.RECEIPT.toString())
				.withHOcr(image.gethOCRText())
				.withOcrThreshold(image.getThresh())
				.withOcrText(scannedText)
				.withOcrWords(ParserUtil.getOcrWords(image.getWords()))
				.withReceiptCompany(companyParserService.parseForReceipt(scannedText))
				.withReceiptDate(ParserUtil.getDate(scannedText))
				.withVat(ParserUtil.getVat(scannedText))
				.withPaymentRows(ParserUtil.findPaymentRows(scannedText))
				.build();
		return receipt;
	}

}
