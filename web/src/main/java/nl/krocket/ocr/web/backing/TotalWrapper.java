package nl.krocket.ocr.web.backing;

import java.io.Serializable;

import nl.krocket.ocr.core.util.CollectionsUtil;

public class TotalWrapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer total;
	
	private Iterable<?> rows;
	
	public TotalWrapper(Iterable<?> iterable) {
		this.rows = iterable;
		this.total = CollectionsUtil.getSize(this.rows);
	}
	
	public TotalWrapper(Iterable<?> iterable, Integer total) {
		this.rows = iterable;
		this.total = total;
	}

	public Integer getTotal() {
		return this.total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Iterable<?> getRows() {
		return rows;
	}

	
}
