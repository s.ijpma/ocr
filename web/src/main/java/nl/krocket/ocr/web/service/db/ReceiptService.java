package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.web.backing.ActiveUser;

public interface ReceiptService {

	/**
	 * Finds all parsed receipts
	 * @param user 
	 * @return
	 */
	public Iterable<Receipt> findAll();
	
	/**
	 * Gets parsed receipt based on given receiptId
	 * @param user 
	 * @param receiptId
	 * @return
	 */
	public Receipt get(Long receiptId);
	
	/**
	 * Saves given parsed receipt object
	 * @param receiptParsed
	 * @return
	 */
	public Receipt save(Receipt receiptParsed);
	
	/**
	 * Deletes given parsed receipt object
	 * @param receiptParsed
	 */
	public void delete(Receipt receiptParsed);
	
	/**
	 * Deletes receipt object based on given baseImageId
	 * @param baseImageId
	 */
	public void delete(Long baseImageId);

	/**
	 * Finds all parsed receipts for given user
	 * @param user
	 * @return
	 */
	public Iterable<Receipt> findAllByUserInfo(ActiveUser user);

	/**
	 * Gets count of rows
	 * @return
	 */
	public Long getCount();
	
}
