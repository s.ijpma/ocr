package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.OcrImage;
import nl.krocket.ocr.core.model.UserInfo;

public interface OcrImageService {
	
	/**
	 * Finds all ocrImage row objects
	 * @return
	 */
	public Iterable<OcrImage> findAll();
	
	/**
	 * Finds all ocrImage row objects for given user
	 * @param userInfo
	 * @return
	 */
	public Iterable<OcrImage> findAllByUserInfo(UserInfo userInfo);
	
	/**
	 * Gets ocrImage object based on given baseImageId
	 * @param baseImageId
	 * @return
	 */
	public OcrImage get(Long baseImageId);
	
	/**
	 * Saves given ocrImage object
	 * @param ocrImage
	 * @return
	 */
	public OcrImage save(OcrImage ocrImage);
	
	/**
	 * Deletes given ocrImage object
	 * @param ocrImage
	 */
	public void delete(OcrImage ocrImage);

	/**
	 * Gets count of rows
	 * @return
	 */
	public Long getCount();
	
}
