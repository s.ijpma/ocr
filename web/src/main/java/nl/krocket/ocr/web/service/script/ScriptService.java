package nl.krocket.ocr.web.service.script;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import nl.krocket.ocr.service.OSUtil;
import nl.krocket.ocr.web.backing.Result;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ScriptService {
	
	private static final Logger LOGGER = Logger.getLogger(ScriptService.class.getCanonicalName());

	public Result runScript(String arg) {
		if (OSUtil.isWindows()) {
			String[] arr = arg.split(" ");
			if (arr.length==3) {
				arg = arr[0] + " " + arr[1].replaceAll("/", "\\\\") + " " + arr[2].replaceAll("/", "\\\\");
			}
			else {
				arg = arr[0] + " " + arr[1].replaceAll("/", "\\\\");
			}
		}
		LOGGER.info("Running script: " + arg);
		
		String message = "";
		String error = "";
		try {
            Process proc = Runtime.getRuntime().exec(arg);
            BufferedReader read = new BufferedReader(new InputStreamReader(
                    proc.getInputStream()));
            try {
                proc.waitFor();
            } catch (InterruptedException e) {
                error += e.getMessage();
            }
            
            while (read.ready()) {
                message += read.readLine();
            }
            
            BufferedReader errorRead = new BufferedReader(new InputStreamReader(
                    proc.getErrorStream()));
            try {
                proc.waitFor();
            } catch (InterruptedException e) {
                error += e.getMessage();
            }
            
            while (errorRead.ready()) {
                error += errorRead.readLine();
            }
            read.close();
            errorRead.close();
            
        } catch (IOException e) {
            error += e.getMessage();
        }
		Result r = new Result();
		r.setError(error);
        r.setMessage(message);
		return r;
	}
	
}
