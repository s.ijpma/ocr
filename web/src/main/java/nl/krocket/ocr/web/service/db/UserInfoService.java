package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.UserInfo;

public interface UserInfoService {

	/**
	 * Finds all userInfo
	 * @return
	 */
	public Iterable<UserInfo> findAll();
	
	/**
	 * Gets receipt based on given userId
	 * @param receiptId
	 * @return
	 */
	public UserInfo get(Long userId);
	
	/**
	 * Gets receipt based on given userName
	 * @param receiptId
	 * @return
	 */
	public UserInfo get(String userName);
	
	/**
	 * Saves given userInfo object
	 * @param receipt
	 * @return
	 */
	public UserInfo save(UserInfo userInfo);
	
	/**
	 * Deletes given userInfo object
	 * @param receipt
	 */
	public void delete(UserInfo userInfo);
}
