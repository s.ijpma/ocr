package nl.krocket.ocr.web.service.other;

import nl.krocket.ocr.web.backing.PropertyEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
/**
 * Property service for loading properties from adapter.properties
 * @author Sander IJpma
 *
 */
public class PropertyService {
	
	@Autowired
    private Environment env;
	
	/**
	 * Returns the property value associated with the given property enum. 
	 * Throws IllegalStateException if property enum not found in properties file
	 * @param p
	 * @return
	 */
	public String getPropertyValue(PropertyEnum p) {
		return env.getRequiredProperty(p.toString());
	}
	
	/**
	 * Returns the property value associated with the given property key. 
	 * Throws IllegalStateException if property key not found in properties file
	 * @param propKey
	 * @return
	 */
	public String getPropertyValue(String propKey) {
		return env.getRequiredProperty(propKey);
	}

}
