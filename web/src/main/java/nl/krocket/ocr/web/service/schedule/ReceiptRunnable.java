package nl.krocket.ocr.web.service.schedule;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

import nl.krocket.img.PreProcessor;
import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.scan.ReceiptScanner;
import nl.krocket.ocr.service.FileUtil;
import nl.krocket.ocr.web.backing.ProcessState;
import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.backing.Result;
import nl.krocket.ocr.web.backing.UploadTypeEnum;
import nl.krocket.ocr.web.service.db.ReceiptService;
import nl.krocket.ocr.web.service.parse.ReceiptParserService;
import nl.krocket.ocr.web.util.ReceiptUtil;

public class ReceiptRunnable extends BaseImageRunnable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(ReceiptRunnable.class.getCanonicalName());
	
	private ReceiptParserService receiptParserService;
	private ReceiptService receiptService;

	@Override
	public void run(BaseImage nextImage) {
		state = ProcessState.STATE_BUSY; //set to busy instantly
		updateStatus(5, "Done", "Initilizing...");
		String scriptPath = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_PATH);
		String resize = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_RESIZE);
		String textclean = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_TEXTCLEAN);
		String orientationScript = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_ORIENTATION);
		String rotationScript = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_ROTATION);
    	String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_UPLOAD_PATH);
		LOGGER.info("Processing receipt: " + nextImage.getFileDirectory());
		//update ProcesStatus
		nextImage = updateProcesStatus(nextImage, ProcesStatus.PROCESSING);
		try {
			Path sourcePath = Paths.get(path, nextImage.getUserInfo().getUserId().toString(), UploadTypeEnum.RECEIPT.toString(), nextImage.getFileDirectory());
			Path sourceFile = Paths.get(sourcePath + File.separator + nextImage.getFileName());
			//do orientation
			updateStatus(5, "Done", "Checking orientation...");
			LOGGER.info("Getting orientation on: " + sourceFile.toString());
			Result orientationResult = scriptService.runScript(scriptPath + orientationScript + " " + sourceFile.toString());
			String orientation = orientationResult.getMessage();
			LOGGER.info("Orientation " + orientation + ", errors?: " + orientationResult.getError());
			updateStatus(5, "Done", "Doing rotation...");
			if (!orientation.isEmpty() || !orientation.equalsIgnoreCase("1")) {
				if (orientation.equalsIgnoreCase("6")) {
					Result rotationResult = scriptService.runScript(scriptPath + rotationScript + " " + sourceFile.toString());
					LOGGER.info("Info from scriptservice: " + rotationResult.getMessage() + ", errors?:" + rotationResult.getError());
				}
			}
			//do preprocessing
			String preProcessedFileName = FileUtil.renameFile(nextImage.getFileName(), "", "_cropped");
			Path preProcessedFilePath = Paths.get(sourcePath + File.separator + preProcessedFileName);
			LOGGER.info("Do resize on: " + preProcessedFileName);
			PreProcessor.preProcess(sourceFile.toString(), preProcessedFilePath.toString());
			
			//do resize
			updateStatus(5, "Done", "Resizing image...");
			LOGGER.info("Do resize on: " + nextImage.getFileName());
			String resizedFileName = FileUtil.renameFile(preProcessedFileName, "", "_resized");
			Path resizedFilePath = Paths.get(sourcePath + File.separator + resizedFileName);

			Result r2 = scriptService.runScript(scriptPath + resize + " " + preProcessedFilePath.toString() + " " + resizedFilePath.toString());
			LOGGER.info("Info from scriptservice: " + r2.getMessage() + ", errors?:" + r2.getError()); 
			//do imagemagic
			updateStatus(10, "Done", "Image processing...");
			LOGGER.info("Do imagemagic stuff");
			String magicFileName = FileUtil.renameFile(resizedFileName, "", "_magic");
		
			Path magicFilePath = Paths.get(sourcePath + File.separator + magicFileName);
			
			Result r = scriptService.runScript(scriptPath + textclean + " " + resizedFilePath.toString() + " " + magicFilePath.toString());
			//update fileName
			
			nextImage = updateFileName(nextImage, magicFileName);
			LOGGER.info("Info from scriptservice: " + r.getMessage() + ", errors?:" + r.getError()); 
			LOGGER.info("New fileName: " + magicFileName);
			updateStatus(20, "Done", "Image ocr-ing...");
			//do ocr scan
			ScannedImage scannedImage = scan(nextImage, UploadTypeEnum.RECEIPT);
			LOGGER.info("Ocred receipt " + scannedImage.getScannedText());
			//set dir to original dir!!
			scannedImage.setDir(nextImage.getFileDirectory());
			updateStatus(20, "Done", "Parsing...");
			//do parsing
			Receipt receipt = receiptParserService.getReceiptParsed(scannedImage);
			
			LOGGER.info("Receipt" + receipt.toString());
			//if is receipt
			if (receipt.isReceipt()) {
				if (null == receipt.getReceiptCompany()) {
					receipt.setProcesStatus(ProcesStatus.COMPANY_UNKNOWN);
				}
				else {
					receipt.setProcesStatus(ProcesStatus.WAITING_FOR_APPROVAL);
				}
			}
			else {
				receipt.setProcesStatus(ProcesStatus.DENIED);
			}
			//set id & userinfo before saving
			receipt.setBaseImageId(nextImage.getBaseImageId());
			receipt.setUserInfo(nextImage.getUserInfo());
			//do save
			updateStatus(20, "Done", "Save to database...");
			ReceiptUtil.save(receipt, receiptService, paymentRowService, ocrWordService);
			this.state=ProcessState.STATE_FINISHED;
			updateStatus(10, "Done", "Finished");
		}
		catch (Exception e) {
			LOGGER.error("Something went wrong scanning receipt " + nextImage.getFileDirectory(), e);
			updateProcesStatus(nextImage, ProcesStatus.UNKNOWN);
			this.state=ProcessState.STATE_FINISHED;
			updateStatus(0, "Done", "Something went wrong...");
		}
	}

	/**
	 * @return the receiptParserService
	 */
	public ReceiptParserService getReceiptParserService() {
		return receiptParserService;
	}

	/**
	 * @param receiptParserService the receiptParserService to set
	 */
	public void setReceiptParserService(ReceiptParserService receiptParserService) {
		this.receiptParserService = receiptParserService;
	}

	/**
	 * @return the receiptService
	 */
	public ReceiptService getReceiptService() {
		return receiptService;
	}

	/**
	 * @param receiptService the receiptService to set
	 */
	public void setReceiptService(ReceiptService receiptService) {
		this.receiptService = receiptService;
	}
	
	public ScannedImage scan(BaseImage baseImage, UploadTypeEnum type) throws Exception {
		String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_UPLOAD_PATH);
   		return ReceiptScanner.scanReceipt(path, baseImage.getUserInfo().getUserId() + File.separator + type.toString() + File.separator + baseImage.getFileDirectory(), baseImage.getFileName());
    }

}
