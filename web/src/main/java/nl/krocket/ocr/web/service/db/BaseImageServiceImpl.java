package nl.krocket.ocr.web.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.repositories.BaseImageRepository;

@Service
public class BaseImageServiceImpl implements BaseImageService {
	
	@Autowired 
	BaseImageRepository repository;

	@Override
	public Iterable<BaseImage> findAllByUserInfo(UserInfo userInfo) {
		return repository.findAllByUserInfo(userInfo);
	}
	
	@Override
	public Iterable<BaseImage> findAll() {
		return repository.findAll();
	}

	@Override
	public BaseImage get(Long baseImageId) {
		return repository.findOne(baseImageId);
	}

	@Override
	public BaseImage save(BaseImage baseImage) {
		return repository.save(baseImage);
	}

	@Override
	public void delete(BaseImage baseImage) {
		repository.delete(baseImage);
	}

	@Override
	public Iterable<BaseImage> findAllByProcesStatus(ProcesStatus status) {
		return repository.findAllByProcesStatus(status);
	}

	@Override
	public Long getCount() {
		return repository.count();
	}

	@Override
	public Page<BaseImage> findAllByUserInfo(UserInfo user, String sort,
			String order, int limit, int offset) {
		Page<BaseImage> p = repository.findAllByUserInfo(user, new PageRequest(offset, limit, new Sort(Direction.fromString(order), sort)));
		return p;
	}

	

}
