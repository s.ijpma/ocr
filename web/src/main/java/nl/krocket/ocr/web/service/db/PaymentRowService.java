package nl.krocket.ocr.web.service.db;

import nl.krocket.ocr.core.model.PaymentRow;

public interface PaymentRowService {

	/**
	 * Finds all receiptRows
	 * @return
	 */
	public Iterable<PaymentRow> findAll();
	
	/**
	 * Gets ReceiptRow based on given receiptRowId
	 * @param rReceiptRowId
	 * @return
	 */
	public PaymentRow get(Long receiptRowId);
	
	/**
	 * Saves given receiptRow object
	 * @param receiptRow
	 * @return
	 */
	public PaymentRow save(PaymentRow receiptRow);
	
	/**
	 * Deletes given receiptRow object
	 * @param receiptRow
	 */
	public void delete(PaymentRow receiptRow);
	
	/**
	 * Gets count of rows
	 * @return
	 */
	public Long getCount();
}
