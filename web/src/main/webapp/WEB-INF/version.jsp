<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Over - versie</title>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap table -->
<link href="./css/bootstrap-table.css" rel="stylesheet" >

<!-- Custom styles for this template -->
<link href="./css/navbar.css" rel="stylesheet">
<style>

</style>
</head>
<body>
<div class="container">

<jsp:include page="./navbar.jsp" />

<h1>Versie informatie</h1>
<div id="toolbar">

</div>
 <table id="table"
 		   data-toggle="table"
           data-toolbar="#toolbar"
           data-show-toggle="true"
           data-show-export="true"
           data-id-field="receiptId"
           data-url="./get/version"
           data-height="299"
           data-mobile-responsive="true"
           data-cache="true"
           class="table table-striped">
    <thead>
        <tr>
            <th data-field="name" data-sortable="true">Atribuut</th>
            <th data-field="value" data-sortable="true">Waarde</th>
        </tr>
    </thead>
    
</table>

</div>

<jsp:include page="./footer.jsp" />

<!-- Bootstrap core JavaScript
   ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/umd/tooltip.js"></script>
<script type="text/javascript" src="./js/tableExport.js"></script>
<script type="text/javascript" src="./js/bootstrap-table.js"></script>
<script type="text/javascript" src="./js/extensions/mobile/bootstrap-table-mobile.js"></script>
<script type="text/javascript" src="./js/tableExport.js"></script>
<script type="text/javascript" src="./js/extensions/export/bootstrap-table-export.js"></script>

<script>
	
</script>
</body>
</html>