<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>Oops</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!-- Bootstrap core CSS -->
<link href="${contextPath}/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap table -->
<link href="${contextPath}/css/bootstrap-table.css" rel="stylesheet" >

<!-- Custom styles for this template -->
<link href="${contextPath}/css/navbar.css" rel="stylesheet">
<style>

</style>
</head>
<body>
<div class="container">

<jsp:include page="./navbar.jsp" />

<h1>Oops...</h1>

Er is iets fout gegaan: ${model.errorcode}

<!--
    Failed URL: ${model.url}
    Exception:  ${model.exception.message}
    <c:forEach items="${model.exception.stackTrace}" var="ste">    ${ste} 
    </c:forEach>
-->

</div>

<jsp:include page="./footer.jsp" />

<script type="text/javascript" src="${contextPath}/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${contextPath}/js/hocr.js"></script>
</body>
</html>