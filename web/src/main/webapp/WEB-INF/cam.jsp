<!DOCTYPE html>
<html>
<head>
	<title>OCR - cam</title>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/style.css">
	
	<!--[if lt IE 9]>
	     <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
<script type="text/javascript" src="./js/webcam.js"></script>

    <div id="my_camera" style="width:240px; height:480px;"></div>
    <div id="my_result"></div>
    
    <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
    
    <script>
        Webcam.attach( '#my_camera' );

        function take_snapshot() {
            Webcam.snap( function(data_uri) {
                document.getElementById('my_result').innerHTML = '<img src="'+data_uri+'"/>';
                Webcam.upload( data_uri, './snapupload', function(code, text) {
                    // Upload complete!
                    // 'code' will be the HTTP response code from the server, e.g. 200
                    // 'text' will be the raw response content
                    if (code===200) {
                    	Webcam.off();
                    }
                    else {
                    	alert('Foutcode: ' + code + ': ' + text);
                    }
                    alert(text);
                } );
            } );
        }
    </script>

    <a href="javascript:void(take_snapshot())">Take Snapshot</a>

</body>
</html>