<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title>Bon debug</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!-- Bootstrap core CSS -->
<link href="${contextPath}/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap table -->
<link href="${contextPath}/css/bootstrap-table.css" rel="stylesheet" >

<!-- Custom styles for this template -->
<link href="${contextPath}/css/navbar.css" rel="stylesheet">
<style>
 .source_image {
      position:absolute;
      border: 1px solid #ddd;
    }
#image_container {
	position: relative;
}
body {
      position:absolute;
      top: 0px;
    }

.ocrx_word {
      display:none;
}
    
.child {
	position: absolute;
}
</style>
</head>
<body>
<div class="container">

<h1>Bon debug</h1>

<div class="input-group">
<div class="checkbox">
    <label>
      <input type="checkbox" id="showRects" aria-label="Show ocr-bounded-rectangles">Show rectangles
    </label>
  </div>

<div class="checkbox">
    <label>
      <input type="checkbox" id="toggleThresh" aria-label="Show thresh image">Toggle image
    </label>
  </div>

</div>
	
<div id="image_container">
    <img id="thresh_image" src="${contextPath}/get/image/${model.fileDirectory}/${model.type}/${model.threshFileName}"/>
</div>


<div id="hocr">${model.hOcr}</div>
</div>

<!-- Bootstrap core JavaScript
   ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="${contextPath}/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${contextPath}/js/hocr.js"></script>
<script>
$(document).ready(function() {
	
	$('#showRects').on('click',function(){ 
		var $inputRects = $( this );
		isChecked = $inputRects.prop("checked");
		if(isChecked){
			var resize = 100;
		    $(".ocrx_word").attr('style', function() {
		 		Hocr.bboxToRect(this.title,  this.textContent, $("#image_container"), 'blue', 'red', resize);
		 	});
		}
		else {
			Hocr.removeRect($("#image_container"));
		}
	});
	
	$('#toggleThresh').on('click',function(){
		var $inputText = $( this );
		isChecked = $inputText.prop("checked");
		if(isChecked){
			$('#thresh_image').hide();
		}
		else {
			$('#thresh_image').show();
		}
	});

	
});
 
</script>
</body>
</html>