<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Welkom</title>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap table -->
<link href="./css/bootstrap-table.css" rel="stylesheet" >

<!-- Custom styles for this template -->
<link href="./css/navbar.css" rel="stylesheet">
<style>

</style>
</head>
<body>
<div class="container">

<jsp:include page="./navbar.jsp" />

<h1>Welkom</h1>

<div id="no_invoices" class="jumbotron" style="display: none;">
<h1>Upload facturen</h1>
<p>Je hebt nog geen facturen geupload. Je kunt dat doen door op de knop te klikken.</p>
<p><a class="btn btn-primary btn-lg" href="./upload_invoice" role="button">Learn more</a></p>
</div>

<div id="no_receipts" class="jumbotron" style="display: none;">
<h1>Upload bonnen</h1>
<p>Je hebt nog geen bonnen geupload. Je kunt dat doen door op de knop te klikken.</p>
<p><a class="btn btn-primary btn-lg" href="./upload" role="button">Learn more</a></p>
</div>

<div id="graphs_invoices" class="panel panel-default" style="display: none;">
  <div class="panel-heading">
    <h3 class="panel-title">Facturen</h3>
  </div>
  <div class="panel-body">
    <canvas id="procesStatus_invoices" width="208" height="208"></canvas>
  </div>
</div>

<div id="graphs_receipts" class="panel panel-default" style="display: none;">
  <div class="panel-heading">
    <h3 class="panel-title">Bonnen</h3>
  </div>
  <div class="panel-body">
    <canvas id="procesStatus_receipts" width="208" height="208"></canvas>
  </div>
</div>


</div>

<jsp:include page="./footer.jsp" />

<!-- Bootstrap core JavaScript
   ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/Chart.js"></script>
<script>

function showJumboOrGraphs(type) {
	$.getJSON( "./get/graph/count/" + type, null )
		.done(function( json ) {
			console.log( "JSON Data: " + json );
			if (json===0) {
				$('#no_' + type).show();
			}
			else {
				showGraphs(type);
			}
		})
		.fail(function( jqxhr, textStatus, error ) {
			var err = textStatus + ", " + error;
			console.log( "Request Failed: " + err );
	});
}

function showGraphs(type) {
	var ctx = document.getElementById("procesStatus_" + type).getContext("2d");
	loadGraph(ctx, type);
}

function loadGraph(canvas, type) {
	$('#graphs_' + type).show();
	$.getJSON( "./get/graph/processtatus/" + type, null )
	.done(function( json ) {
		var options = 
		{
		    //Boolean - Whether we should show a stroke on each segment
		    segmentShowStroke : true,
		    //String - The colour of each segment stroke
		    segmentStrokeColor : "#313A75",
		    //Number - The width of each segment stroke
		    segmentStrokeWidth : 2,
		    //Number - The percentage of the chart that we cut out of the middle
		    percentageInnerCutout : 50, // This is 0 for Pie charts
		    //Number - Amount of animation steps
		    animationSteps : 100,
		    //String - Animation easing effect
		    animationEasing : "easeOutBounce",
		    //Boolean - Whether we animate the rotation of the Doughnut
		    animateRotate : true,
		    //Boolean - Whether we animate scaling the Doughnut from the centre
		    animateScale : false,
		    
		};
			var myDoughnutChart = new Chart(canvas).Doughnut(json,options);
			
		})
		.fail(function( jqxhr, textStatus, error ) {
			var err = textStatus + ", " + error;
			console.log( "Request Failed: " + err );
	});
	 
	 
}
	
	 $(document).ready(function() {
		 showJumboOrGraphs('invoices');
		 showJumboOrGraphs('receipts');
	});
	
</script>
</body>
</html>
