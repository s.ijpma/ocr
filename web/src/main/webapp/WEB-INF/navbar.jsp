<!-- Static navbar -->
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><img alt="Brand" src="./images/k-rocket_1.png"></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="./">Home</a></li>
				<li><a href="./upload">Scan bon</a></li>
				<li><a href="./upload_invoice">Scan factuur</a></li>
				<li><a href="./receipts">Bonnen-facturen</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Over <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="./version">Versie</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Contact</li>
						<li><a href="./contact">Contact</a></li>
						<li><a href="http://krocket.nl/" target="_blank">krocket</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">

				<li><form class="navbar-form navbar-right" action="./logout"
						method="POST">
						<button type="submit" class="btn btn-default navbar-btn">
							<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Afmelden
						</button>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</form></li>

			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
	<!--/.container-fluid -->
</nav>