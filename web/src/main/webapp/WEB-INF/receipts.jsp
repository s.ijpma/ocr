<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Ingediende facturen-bonnen</title>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap table -->
<link href="./css/bootstrap-table.css" rel="stylesheet" >

<!-- Custom styles for this template -->
<link href="./css/navbar.css" rel="stylesheet">
<style>

</style>
</head>
<body>
<div class="container">

<jsp:include page="./navbar.jsp" />

<h1>Ingediende facturen en bonnen</h1>
<div id="toolbar">

</div>
 <table id="table"
 		   data-toggle="table"
           data-toolbar="#toolbar"
           data-search="true"
           data-show-refresh="true"
           data-show-toggle="true"
           data-show-columns="true"
           data-show-export="true"
           data-minimum-count-columns="2"
           data-sort-order="desc"
           data-pagination="true"
           data-page-list="[10, 25, 50, 100, ALL]"
           data-side-pagination="server"
           data-id-field="baseImageId"
           data-url="./get/baseimages/total"
           data-height="299"
           data-mobile-responsive="true"
           data-cache="false"
           data-response-handler="responseHandler"
           class="table table-striped">
    <thead>
        <tr>
            <th data-field="baseImageId" data-sortable="true">Bon id</th>
            <th data-field="dateTime" data-sortable="true">Creatie datum</th>
            <th data-field="fileName" data-sortable="true">Bestandsnaam</th>
            <th data-field="total" data-sortable="false">Totaal</th>
            <th data-field="procesStatus" data-formatter="statusFormatter" data-sortable="true">Status</th>
            <th data-field="operate" data-formatter="operateFormatter" data-events="operateEvents" data-sortable="false">Operatie</th>
        </tr>
    </thead>
    
</table>

<div class="modal fade" id="showReceipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="modalContenct">
                    	<div class="thumbnail">
                    		<img id="source_image" src="" class="img-responsive"/>
                   		</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="./footer.jsp" />

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<sec:authentication property="principal.userId" var="userId" /> 

<!-- Bootstrap core JavaScript
   ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/umd/tooltip.js"></script>
<script type="text/javascript" src="./js/tableExport.js"></script>
<script type="text/javascript" src="./js/bootstrap-table.js"></script>
<script type="text/javascript" src="./js/extensions/mobile/bootstrap-table-mobile.js"></script>
<script type="text/javascript" src="./js/tableExport.js"></script>
<script type="text/javascript" src="./js/extensions/export/bootstrap-table-export.js"></script>
<script type="text/javascript" src="./js/sockjs.min.js"></script>
<script type="text/javascript" src="./js/stomp.min.js"></script>
<script>

//Open the web socket connection.
var sock = new SockJS('${contextPath}/progress');
var stompClient = Stomp.over(sock);
//Callback function to be called when stomp client is connected to server
var connectCallback = function() {
  stompClient.subscribe('/topic/progress/${userId}', renderProgress);
}; 
 
// Callback function to be called when stomp client could not connect to server
var errorCallback = function(error) {
  alert(error.headers.message);
};

var headerName = "${_csrf.headerName}";
var token = "${_csrf.token}";

var headers = {};
headers[headerName] = token;

// Connect to server via websocket
stompClient.connect(headers, connectCallback, errorCallback);

var $table = $('#table');

//Render data from server into HTML, registered as callback
//when subscribing to topic
function renderProgress(frame) {
	var data = JSON.parse(frame.body);
	var percent = data.precentComplete;
	var message = data.message;
	var label = data.label;
	var id = data.id;
	if (!$('#operation' + id).length){ //check if exist
		$table.bootstrapTable('refresh'); //refresh table to load new record
    }
	$('#operation' + id).hide();
	$('#progress' + id).show();
	$('#progress-bar' + id).css('width', percent+'%').attr('aria-valuenow', percent);
	$('#progress-bar' + id).text(percent+'%' + ' ' + message);
}
	
	function responseHandler(res) {
		var flatArray = [];
		var item = {}
		item ["total"] = res.total;
		item ["rows"] = flatArray;
			$.each(res.rows.content, function(i, element) {
			flatArray.push(element);
		});
		return item;
	}
	
	function operateFormatter(value, row, index) {
		if (row.procesStatus.toUpperCase() !== 'accepted'.toUpperCase()) {
		    return [
		        '<div id="operation' +row.baseImageId+ '">',
				'<a class="eye" href="javascript:void(0)" title="Open">',
				'<i class="glyphicon glyphicon-eye-open"></i>',
				'</a>',
		        '<a class="remove" href="javascript:void(0)" title="Remove">',
		        '<i class="glyphicon glyphicon-remove"></i>',
		        '</a>',
				'<a class="debug" href="./get/baseimage/' +row.baseImageId+ '"  target="_blank" title="Debug">',
				'<i class="glyphicon glyphicon glyphicon-wrench"></i>',
				'</a>',
				'</div>',
				'<div id="progress' +row.baseImageId+ '" class="progress" style="display: none;">',
				'<div class="progress-bar" id="progress-bar' +row.baseImageId+ '" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%; min-width: 2em;" >0%',
				'</div></div>'
		    ].join('');
		}
		else {
			return [
					'<div id="operation' +row.baseImageId+ '">',
					'<a class="eye" href="javascript:void(0)" title="Open">',
					'<i class="glyphicon glyphicon-eye-open"></i>',
					'</a>',
					'<a class="debug" href="./get/baseimage/' +row.baseImageId+ '"  target="_blank" title="Debug">',
					'<i class="glyphicon glyphicon glyphicon-wrench"></i>',
					'</a>',
					'</div>',
					'<div id="progress' +row.baseImageId+ '" class="progress" style="display: none;">',
					'<div class="progress-bar" id="progress-bar' +row.baseImageId+ '" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%; min-width: 2em;" >0%',
					'</div></div>'
			    ].join('');
		}
	}
	
	window.operateEvents = {
	    'click .remove': function (e, value, row, index) {
	        $table.bootstrapTable('remove', {
	            field: 'baseImageId',
	            values: [row.baseImageId]
	        });
	        delReceipt([row.baseImageId]);
	    },
	    'click .eye': function (e, value, row, index) {
	    	openModel(row);
	    },
	};
	
	function delReceipt(id) {
		$.ajax({
            type:"POST",
            beforeSend: function (request)
            {
                request.setRequestHeader('X-CSRF-Token', document.getElementsByName("_csrf")[0].value);
            },
            url: "./delete/receipt/" + id,
            success: function(msg) {
            	alert("The result =" + msg.message);
            }
    	});
	}
	
	function statusFormatter(data) {
		var firstLetter = data.substring(0, 1);
		var rest = data.substring(1, data.length);
		rest = rest.toLowerCase();
	    return firstLetter.toUpperCase() + rest.toLowerCase();
	}
	function totalNameFormatter(data) {
	    return data.length;
	}
	function totalPriceFormatter(data) {
	    var total = 0;
	    $.each(data, function (i, row) {
	        total += +(row.price.substring(1));
	    });
	    return '$' + total;
	}
	
	function openModel(row) {
		var modal = $('#showReceipt');
		var modalBody = $('#showReceipt .modal-body');
		
        modal.on('show.bs.modal', function () {
        	var src = "./get/image/"+row.fileDirectory+"/" + row.type+"/" + row.fileName;
        	
        	$('#source_image').attr("src", src);
        	
        }).modal();
    
        $(this).css('display', 'block');
        var $dialog = $(this).find(".modal-dialog");
        var offset = ($(window).height() - $dialog.height()) / 2;
        // Center modal vertically in window
        $dialog.css("margin-top", offset);
        
        modal.show();
	}
	
	 $(document).ready(function() {
		 $table.on('dbl-click-row.bs.table', function(e, row, $element) {
			openModel(row);	 
		 });
	});
	
</script>
</body>
</html>
