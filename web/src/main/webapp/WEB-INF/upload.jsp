<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Scan bon</title>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./css/navbar.css" rel="stylesheet">
<style>
#progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
#bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
#percent { position:absolute; display:inline-block; top:3px; left:48%; }
.source_image {
	position: absolute;
	border: 1px solid #ddd;
}

.child {
	position: absolute;
	}
#hocr {
	display: none;
}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>
</head>
<body>
	<div class="container">
	
	<jsp:include page="./navbar.jsp" />
      
		<h1>Scan bon</h1>
			
			<div id="message" role="alert"></div>

			<form id="uploadForm"
				action="./snapupload?${_csrf.parameterName}=${_csrf.token}"
				method="post" enctype="multipart/form-data">
				<div class="form-group">
					<p class="help-block">Scan je bon door het maken van een foto met je mobiele telefoon.</p>
					<label for="inputFile">Bon scannen</label>
					<span class="btn btn-default btn-file">
						Scan... <input id="inputFile" type="file" capture="camera" accept="image/*" size="60" name="cam">
					</span>
					
				</div>
				<div class="progress">
					<div class="progress-bar" id="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
					</div>
				</div>
			</form>
		</div>

	<!-- /container -->

	<jsp:include page="./footer.jsp" />
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="./js/jquery.form.js"></script>
	<script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<script>
$(document).ready(function() {
 
	var response;
    var options = { 
    beforeSend: function() 
    {
    	//clear everything
        $("#message").html("");
        $("#message").hide();
        $('#progress-bar').css('width', '0%').attr('aria-valuenow', 0);

    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
    	$('#progress-bar').css('width', percentComplete+'%').attr('aria-valuenow', percentComplete);
    },
    success: function() 
    {
    	$('#progress-bar').css('width', '100%').attr('aria-valuenow', 100);
 
    },
    complete: function(response) 
    {
		response = response.responseJSON;
    	
    	if(response.error) {
    		$("#message").html("<font color='red'> ERROR: " + response.error +"</font>").attr("class", "alert alert-danger");
    		$("#message").show();
    	}
    	else {
    		$("#message").html("<font color='black'>" + response.message +
    				" Ga <a href='./receipts'>hier</a> naar de bonnenlijst, of scan een nieuwe bon!" +
    				"</font>").attr("class", "alert alert-info");
    		$("#message").show();
    	}
    	
    },
    error: function(response)
    {
        $("#message").html("<font color='red'> ERROR: " + response.error +"</font>").attr("class", "alert alert-danger");
        $("#message").show();
 
    }
 
}; 
 
     $("#uploadForm").ajaxForm(options);
     
     $('input[type=file]').change(function() {
         $("#uploadForm").submit();
      });

    
});
 
</script>
</body>
</body>
</html>