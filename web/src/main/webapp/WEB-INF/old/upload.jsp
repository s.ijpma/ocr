<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<title>OCR- Upload file</title>
<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/jquery.form.js"></script>
<script type="text/javascript" src="./js/hocr.js"></script>
<style>
form { display: block; margin: 20px auto; background: #eee; border-radius: 10px; padding: 15px }
#progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
#bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px; }
#percent { position:absolute; display:inline-block; top:3px; left:48%; }
.source_image {
	position: absolute;
	border: 1px solid #ddd;
}

#container {
	position: relative;
}

.child {
	position: absolute;
	}
#hocr {
	display: none;
}
</style>
</head>
<body>
 
<form id="myForm" action="./snapupload?${_csrf.parameterName}=${_csrf.token}" method="post" enctype="multipart/form-data">
     <input type="file" size="60" name="cam">
     <input type="submit" value="Ajax File Upload">
 </form>
 
 <div id="progress">
        <div id="bar"></div>
        <div id="percent">0%</div >
</div>


<br/>
 <div id="container">
		<img class="source_image" id = "source_image" src="" />
	</div>
	
	<div id="hocr">
	</div>

<div id="message" class="alert alert-danger" role="alert"></div>
 
<script>
$(document).ready(function() {
 
	var response;
    var options = { 
    beforeSend: function() 
    {
        
        //clear everything
        $("#progress").show();
        $("#bar").width('0%');
        $("#percent").html("0%");

        $("#message").html("");
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        $("#bar").width(percentComplete+'%');
        $("#percent").html(percentComplete+'%');
 
    },
    success: function() 
    {
        $("#bar").width('100%');
        $("#percent").html('100%');
 
    },
    complete: function(response) 
    {
		response = response.responseJSON;

    	imageurl = './get/image/'+response.dir + '/' + response.fileName;
    	$("#source_image").attr("src", imageurl);
    	
    	if(response.error) {
    		$("#message").html("<font color='red'> ERROR: " + response.error +"</font>");
    	}
    	
    	$("#hocr").html(response.hOCRText);
    	
    	$(".ocrx_word").attr('style', function() {
			//return Hocr.bboxToStyle(this.title);
			return Hocr.bboxToRect(this.title, $("#container"), 'red');
		});
    	
    },
    error: function(response)
    {
        $("#message").html("<font color='red'> ERROR: unable to upload files</font>");
 
    }
 
}; 
 
     $("#myForm").ajaxForm(options);
    
});
 
</script>
</body>
 
</html>