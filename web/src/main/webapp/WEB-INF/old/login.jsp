<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<title>Login test ocr</title>
	<link rel="stylesheet" href="./css/normalize.css">
	<link rel="stylesheet" href="./css/style.css">
	
	<!--[if lt IE 9]>
	     <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<body>

	<div class="wrapperHeader">
		<div class="header">
			<img src="./images/logo_krocket.png" alt="logo" />
		</div>
	</div>

	<div class="container">
	
		<c:url value="/login" var="loginUrl"/>
		<form action="${loginUrl}" method="POST">
			<c:if test="${param.error != null}">
				<p>
					Invalid username and password.
				</p>
			</c:if>
			<c:if test="${param.logout != null}">
				<p>
					You have been logged out.
				</p>
			</c:if>

			<fieldset>
				<legend>Login</legend>
				<label>Username:</label>
					<input type="text" name="username" autofocus />
				<label>Password:</label>
					<input type="password" name="password" autocomplete="off" /> 
				<label></label>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<input type="submit" name="submit" value="Submit" /> 
			</fieldset>
		</form>
	</div>


</body>
</html>