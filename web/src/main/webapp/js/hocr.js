function Hocr() {
}

// "bbox 197 272 249 281"
// hOCR format: https://docs.google.com/document/d/1QQnIQtvdAC_8n92-LhwPcjtAUFwBlzE8EWnKAxlgVf0/preview
// bbox x0 y0 x1 y1

Hocr.bboxToRect = function(bbox_str, text, container, textcolor, linecolor, resize) {
	arrbbox = bbox_str.split(";");
    arr = arrbbox[0].split(" ");
    var d = resize/100;

	div = '<div class="child" style="color:' + textcolor + '; display: inline-block; left: '+Math.ceil(arr[1]*d);
    div = div + 'px; top: '+Math.ceil(arr[2]*d);
    div = div + 'px; width: ' + Math.ceil((arr[3]-arr[1])*d);
    div = div + 'px; height: ' + Math.ceil((arr[4]-arr[2])*d);
    div = div + 'px; border: 1px solid ' + linecolor + ';">' + text + '</div>';  

    $(div).appendTo(container);
	return null;
};

Hocr.removeRect = function(container) {
	$( ".child" ).remove();
	return null;
};