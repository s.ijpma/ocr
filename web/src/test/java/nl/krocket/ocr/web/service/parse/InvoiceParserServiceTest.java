package nl.krocket.ocr.web.service.parse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.util.TextHelper;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class} )
@RunWith(SpringJUnit4ClassRunner.class)
public class InvoiceParserServiceTest {

	@Autowired
	private InvoiceParserService invoiceParseService;
	
	@Test
	public void test() throws Exception {
		ScannedImage scannedImage = new ScannedImage();
		scannedImage.setDir("12345");
		scannedImage.setError("");
		scannedImage.setFileName("");
		scannedImage.sethOCRText("");
		scannedImage.setScannedText(TextHelper.INVOICE_1);
		Invoice invoice = invoiceParseService.parse(scannedImage);
		assertTrue(invoice.isInvoice());
		assertEquals("Brainconsuham", invoice.getFromCompany().getCompanyName());
		assertEquals("pinniq", invoice.getToCompany().getCompanyName());
	}
	
	@Test
	public void tes2t() throws Exception {
		ScannedImage scannedImage = new ScannedImage();
		scannedImage.setDir("12345");
		scannedImage.setError("");
		scannedImage.setFileName("");
		scannedImage.sethOCRText("");
		scannedImage.setScannedText(TextHelper.INVOICE_2);
		Invoice invoice = invoiceParseService.parse(scannedImage);
		assertTrue(invoice.isInvoice());
		assertEquals("Brainconsultant", invoice.getFromCompany().getCompanyName());
		assertEquals("pinniq", invoice.getToCompany().getCompanyName());
	}
}
