package nl.krocket.ocr.web.service.schedule;

import static org.junit.Assert.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileInputStream;
import java.util.Date;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import nl.krocket.ocr.core.model.BaseImage;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.core.util.CollectionsUtil;
import nl.krocket.ocr.web.backing.UploadTypeEnum;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.service.db.BaseImageService;
import nl.krocket.ocr.web.service.db.UserInfoService;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ScheduledTaskTest {
	
	@Autowired
	private ScheduledTask scheduledTask;
	
	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	private UserInfo userInfo;
	@Autowired
    private UserInfoService userInfoService;
	@Autowired
	private BaseImageService baseImageService;
	
	@Before
	public void setup() {
		userInfo = new UserInfoBuilder()
				.withCreationDate(new Date())
				.withPassword("098f6bcd4621d373cade4e832627b4f6")
				.withRole("role")
				.withUserName("test")
				.build();
		
		userInfo = userInfoService.save(userInfo);
		
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity()) 
				.build();
	}

	@Test
	public void testRunReceiptTask() throws Exception {
		
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/totaal_bon.png").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "totaal_bon.png", UploadTypeEnum.RECEIPT.getSupportedMediaTypes()[1], fis);
       
        mvc.perform(MockMvcRequestBuilders.fileUpload("/snapupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(jsonPath("$message").value(Matchers.startsWith("Upload success!!")))
                    .andExpect(status().is(200));
        
		scheduledTask.run();
		
		Iterable<BaseImage> result = baseImageService.findAll();
		
		assertEquals(1, CollectionsUtil.getSize(result));
		
	}

	@Test
	public void testRunInvoiceTask() throws Exception {
		
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/test.pdf").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "test.pdf", UploadTypeEnum.INVOICE.getSupportedMediaTypes()[0], fis);
       
        mvc.perform(MockMvcRequestBuilders.fileUpload("/invoiceupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(jsonPath("$message").value(Matchers.startsWith("Upload success!!")))
                    .andExpect(status().is(200));
        
		scheduledTask.run();
		
		Iterable<BaseImage> result = baseImageService.findAll();
		
		assertEquals(1, CollectionsUtil.getSize(result));
		
	}

}
