package nl.krocket.ocr.web.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Manifest;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class VersionControllerTest {
    private static final Logger LOGGER = Logger.getLogger(VersionControllerTest.class);

    @Autowired
    private WebApplicationContext context;
    private MockMvc mvc;
    @Autowired VersionController versionController;

    @Before
    public void setup() throws Exception {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    public void testVersion() throws Exception {
        try(InputStream is = getClass().getClassLoader().getResourceAsStream("MANIFEST.MF")){
            versionController.setManifest(new Manifest(is));
            new Manifest(is);
        } catch (IOException e) {
            LOGGER.error(e);
        }

        mvc.perform(MockMvcRequestBuilders.get("/get/version"))
                //.andExpect(status().is(200))
                .andDo(print());
    }
}
