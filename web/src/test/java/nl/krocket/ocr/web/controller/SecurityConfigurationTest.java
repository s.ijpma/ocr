package nl.krocket.ocr.web.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.service.db.UserInfoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@WebAppConfiguration
public class SecurityConfigurationTest {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;

	UserInfo userInfo;
	@Autowired
    private UserInfoService userInfoService;
	
	@Before
	public void setup() {
		userInfo = new UserInfoBuilder()
				.withCreationDate(new Date())
				.withPassword("098f6bcd4621d373cade4e832627b4f6")
				.withRole("role")
				.withUserName("test")
				.build();
		
		userInfo = userInfoService.save(userInfo);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity()) 
				.build();
	}
	
	@After
	public void tearDown() {
		userInfoService.delete(userInfo);
	}

	@Test
	public void loginSuccessTest() throws Exception {
		mvc.perform(formLogin("/login")
				.user("test")
				.password("test"))
			.andDo(print())	
			.andExpect(authenticated())
			.andExpect(redirectedUrl("/index"));
	}
	@Test
	public void loginFailure() throws Exception {
		mvc.perform(formLogin("/login")
				.user("test")
				.password("other"))
			.andDo(print())	
			.andExpect(unauthenticated())
			.andExpect(redirectedUrl("/login?error=1"));
	}
	@Test
	public void logoutTest() throws Exception {
		mvc.perform(logout("/logout"))
			.andDo(print())	
			.andExpect(unauthenticated())
			.andExpect(redirectedUrl("/login?logout=1"));
	}
	
	@Test
	public void loginBasicSuccessTest() throws Exception {
		mvc.perform(get("/index")
				.with(httpBasic("test","test")))
				.andDo(print())
				.andExpect(authenticated())
				.andExpect(forwardedUrl("/WEB-INF/index.jsp"));
	}
	
	@Test
	public void loginBasicFailureTest() throws Exception {
		mvc.perform(get("/index")
				.with(httpBasic("test","other")))
				.andDo(print())
				.andExpect(unauthenticated())
				.andExpect(status().is(401));
	}
	

}