package nl.krocket.ocr.web.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileInputStream;
import java.util.Date;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.service.db.UserInfoService;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class FileUploadControllerTest {
	
	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	UserInfo userInfo;
	@Autowired
    private UserInfoService userInfoService;
	
	@Before
	public void setup() {
		userInfo = new UserInfoBuilder()
				.withCreationDate(new Date())
				.withPassword("098f6bcd4621d373cade4e832627b4f6")
				.withRole("role")
				.withUserName("test")
				.build();
		
		userInfo = userInfoService.save(userInfo);
		
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity()) 
				.build();
	}
	
	@After
	public void tearDown() {
		userInfoService.delete(userInfo);
	}
	
	@Test
    public void testSuccessPng() throws Exception {
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/totaal_bon.png").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "totaal_bon.png", MediaType.IMAGE_PNG_VALUE, fis);
       
        mvc.perform(MockMvcRequestBuilders.fileUpload("/snapupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(jsonPath("$message").value(Matchers.startsWith("Upload success!!")))
                    .andExpect(status().is(200));
    }
	
	@Test
    public void testSuccessPdf() throws Exception {
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/test.pdf").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "test.pdf", "application/pdf", fis);
       
        mvc.perform(MockMvcRequestBuilders.fileUpload("/invoiceupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(jsonPath("$message").value(Matchers.startsWith("Upload success!!")))
                    .andExpect(status().is(200));
    }
	
	@Test
    public void testFail1() throws Exception {
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/totaal_geen_bon.png").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "totaal_geen_bon.png", MediaType.IMAGE_PNG_VALUE, fis);
       
        mvc.perform(MockMvcRequestBuilders.fileUpload("/snapupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(jsonPath("$message").value(Matchers.startsWith("Upload success!!")))
                    .andExpect(status().is(200));
    }
	
	@Test
    public void testFailWrongContentType() throws Exception {
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/totaal_geen_bon.png").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "totaal_geen_bon.png", MediaType.MULTIPART_FORM_DATA_VALUE, fis);
       
        mvc.perform(MockMvcRequestBuilders.fileUpload("/snapupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(jsonPath("$error").value(Matchers.startsWith("You failed to upload")))
                    .andExpect(status().is(400));
    }
	
	@Test
    public void testFailWrongSize() throws Exception {
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/totaal_bon_too_large.jpg").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "totaal_bon_too_large.jpg", MediaType.IMAGE_JPEG_VALUE, fis);
        mvc.perform(MockMvcRequestBuilders.fileUpload("/snapupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(jsonPath("$error").value(Matchers.startsWith("You failed to upload")))
                    .andExpect(status().is(400));
    }
	
	
	@Test
    public void testFailFakeJpg() throws Exception {
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/fakejpg.jpg").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "fakejpg.jpg", MediaType.IMAGE_JPEG_VALUE, fis);
        mvc.perform(MockMvcRequestBuilders.fileUpload("/snapupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print());
                        //.andExpect(jsonPath("$error").value(Matchers.startsWith("You failed to upload")))
                    //.andExpect(status().is(400));
                        //TODO: make this work!
    }
	
	
	@Test
    public void testFailEmptyJpg() throws Exception {
		FileInputStream fis=new FileInputStream(this.getClass().getResource("/empty.jpg").getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", "empty.jpg", MediaType.IMAGE_JPEG_VALUE, fis);
        mvc.perform(MockMvcRequestBuilders.fileUpload("/snapupload")
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(jsonPath("$error").value(Matchers.startsWith("You failed to upload")))
                    .andExpect(status().is(400));
    }
}
