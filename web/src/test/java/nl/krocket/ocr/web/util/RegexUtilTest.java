package nl.krocket.ocr.web.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.Vat;

public class RegexUtilTest {
	
	@Before
	public void init() {
		
	}
	
	@Test
	public void testGetCompanyName() throws Exception {
		List<String> r = RegexUtil.getPossibleCompanyNames("Pinniq");
		assertEquals("Pinniq", r.get(0));
	}
	
	@Test
	public void testGetCompanyName2() throws Exception {
		List<String> r = RegexUtil.getPossibleCompanyNames("Werk B.V.");
		assertEquals("Werk B.V.", r.get(0));
	}
	
	@Test
	public void testGetCompanyName3() throws Exception {
		List<String> r = RegexUtil.getPossibleCompanyNames("Werk etc");
		assertEquals("Werk ", r.get(0));
	}
	
	@Test
	public void testAddress1() throws Exception {
		String text = "Dorpsstraat 57" + System.lineSeparator() + "9332PC, Den Horn";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Dorpsstraat", address.getStreetName());
		assertEquals("57", address.getHouseNr());
		assertEquals("9332PC", address.getPostcode());
		assertEquals("Den Horn", address.getCity());
	}
	
	@Test
	public void testAddress2() throws Exception {
		String text = "Dorpsstraat 1" + System.lineSeparator() + "9332PC, Groningen";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Dorpsstraat", address.getStreetName());
		assertEquals("1", address.getHouseNr());
		assertEquals("9332PC", address.getPostcode());
		assertEquals("Groningen", address.getCity());
	}
	
	@Test
	public void testAddress3() throws Exception {
		String text = "Dorpsstraat 1" + System.lineSeparator() + "9332PC Groningen";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Dorpsstraat", address.getStreetName());
		assertEquals("1", address.getHouseNr());
		assertEquals("9332PC", address.getPostcode());
		assertEquals("Groningen", address.getCity());
	}
	
	@Test
	public void testAddress4() throws Exception {
		String text = "Dorpsstraat 1" + System.lineSeparator() + "9332 PC Groningen";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Dorpsstraat", address.getStreetName());
		assertEquals("1", address.getHouseNr());
		assertEquals("9332 PC", address.getPostcode());
		assertEquals("Groningen", address.getCity());
	}
	
	@Test
	public void testAddress5() throws Exception {
		String text = "Dorpsstraat 1" + System.lineSeparator() + "9332 PC, Groningen";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Dorpsstraat", address.getStreetName());
		assertEquals("1", address.getHouseNr());
		assertEquals("9332 PC", address.getPostcode());
		assertEquals("Groningen", address.getCity());
	}
	
	@Test
	public void testAddress6() throws Exception {
		String text = "Dorpsstraat 1-a" + System.lineSeparator() + "9332 PC, Groningen";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Dorpsstraat", address.getStreetName());
		assertEquals("1-a", address.getHouseNr());
		assertEquals("9332 PC", address.getPostcode());
		assertEquals("Groningen", address.getCity());
	}
	
	@Test
	public void testAddress7() throws Exception {
		String text = "Dorpsstraat 12" + System.lineSeparator() + "9332 PC, 's Gravenzande";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Dorpsstraat", address.getStreetName());
		assertEquals("12", address.getHouseNr());
		assertEquals("9332 PC", address.getPostcode());
		assertEquals("'s Gravenzande", address.getCity());
	}
	
	@Test
	public void testAddress8() throws Exception {
		String text = "Helperpnrk 292-0" + System.lineSeparator() + "9332PC, Groningen";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Helperpnrk", address.getStreetName());
		assertEquals("292-0", address.getHouseNr());
		assertEquals("9332PC", address.getPostcode());
		assertEquals("Groningen", address.getCity());
	}
	
	@Test
	public void testAddress9() throws Exception {
		String text = "Helperpnrk 292—0" + System.lineSeparator() + "9332PC, Groningen";
		Map<String, Address> addresses = RegexUtil.getAddress(text);
		Address address = addresses.get(text);
		assertEquals("Helperpnrk", address.getStreetName());
		assertEquals("292—0", address.getHouseNr());
		assertEquals("9332PC", address.getPostcode());
		assertEquals("Groningen", address.getCity());
	}
	
	@Test
	public void testGetCompaniesOne() throws Exception {
		String text = System.lineSeparator() + "PinniQ" + System.lineSeparator()
				+ "Dorpsstraat 57" + System.lineSeparator()
				+ "9332PC, Den Horn" + System.lineSeparator();
		List<Company> companies = RegexUtil.getCompanies(text);
		assertTrue(companies.size()==1);
		Company company = companies.iterator().next();
		assertEquals("PinniQ", company.getCompanyName());
		assertEquals("Dorpsstraat", company.getAddressSet().iterator().next().getStreetName());
	}
	
	@Test
	public void testGetCompaniesTwo() throws Exception {
		String text = System.lineSeparator() + "Krocket" + System.lineSeparator()
				+ "Ergens 12" + System.lineSeparator()
				+ "9800AA, Groningen "
				+ System.lineSeparator() + "PinniQ" + System.lineSeparator()
				+ "Dorpsstraat 57" + System.lineSeparator()
				+ "9332PC, Den Horn" + System.lineSeparator();
		List<Company> companies = RegexUtil.getCompanies(text);
		assertTrue(companies.size()==2);
	}

	@Test
	public void testGetPaymentRowDoubleLine() throws Exception {
		String text = "  dasads  E 121,12 " + System.lineSeparator()
				+ "   vdfsfds E 12,20 ";
		Set<PaymentRow> result = RegexUtil.findNormalPaymentRow(text);
		Iterator<PaymentRow> it = result.iterator();
		PaymentRow result1 = it.next();
		
		assertEquals(new Double("121.12"), result1.getValue());
		assertEquals("dasads", result1.getName());
		
		PaymentRow result2 = it.next();
		
		assertEquals(new Double("12.20"), result2.getValue());
		assertEquals("vdfsfds", result2.getName());
	}
	
	@Test
	public void testGetPaymentRowInvoice() throws Exception {
		String text = "162 Facturabele uren van de maand september € 65,- € 10.530,- " + System.lineSeparator()+ "blaat";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		assertEquals(new Double("10530"), result.getValue());
		assertEquals("162 Facturabele uren van de maand september € 65,-", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetPaymentRowEuroBehind() throws Exception {
		String text = "Totaal 101,92 EUR";
		PaymentRow result = RegexUtil.findTotalPaymentRow(text).iterator().next();
		assertEquals(new Double("101.92"), result.getValue());
	}
	
	@Test
	public void testGetPaymentRowNoCurrency() throws Exception {
		String text = "Totaal 101,92";
		PaymentRow result = RegexUtil.findTotalPaymentRow(text).iterator().next();
		assertEquals(new Double("101.92"), result.getValue());
	}
	
	@Test
	public void testGetPaymentRowCurrency() throws Exception {
		String text = "Totaal   Euro 82,30";
		PaymentRow result = RegexUtil.findTotalPaymentRow(text).iterator().next();
		assertEquals(new Double("82.30"), result.getValue());
	}
	

	@Test
	public void testGetPaymentRowEndingDot() throws Exception {
		String text = ".e 7.46.";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		assertEquals(new Double("7.46"), result.getValue());
	}
	
	@Test
	public void testGetPaymentRowCode() throws Exception {
		String text = "Qutor. code 989586";
		Set<PaymentRow> rows = RegexUtil.findNormalPaymentRow(text);
		assertTrue(rows.isEmpty());
	}
	
	@Test
	public void testGetPaymentRowCod2e() throws Exception {
		String text = "7 21.00 e 35.54 .e 7.34";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		assertEquals(new Double("7.34"), result.getValue());
	}
	
	@Test
	public void testGetPaymentRow2() throws Exception {
		String text = "162 Facturabele uren van de maand september $ 65,- $ 10.530,-";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		assertEquals(new Double("10530"), result.getValue());
		assertEquals("162 Facturabele uren van de maand september $ 65,-", result.getName());
		assertEquals("USD", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetPaymentRow4() throws Exception {
		String text = "Totaal Euro 82,30";
		PaymentRow result = RegexUtil.findTotalPaymentRow(text).iterator().next();
		assertEquals(new Double("82.30"), result.getValue());
		assertEquals("Totaal", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetPaymentRow5() throws Exception {
		String text = "Totaal EUR 82,30";
		PaymentRow result = RegexUtil.findTotalPaymentRow(text).iterator().next();
		assertEquals(new Double("82.30"), result.getValue());
		assertEquals("Totaal", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetPaymentRow6() throws Exception {
		String text = "Totaal euro 82,30";
		PaymentRow result = RegexUtil.findTotalPaymentRow(text).iterator().next();
		assertEquals(new Double("82.30"), result.getValue());
		assertEquals("Totaal", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetPaymentRow7() throws Exception {
		String text = "Totaal eur 82,30";
		PaymentRow result = RegexUtil.findTotalPaymentRow(text).iterator().next();
		assertEquals(new Double("82.30"), result.getValue());
		assertEquals("Totaal", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetPaymentRow8() throws Exception {
		String text = "' TOTAAL € 51.01";
		PaymentRow result = RegexUtil.findTotalPaymentRow(text).iterator().next();
		assertEquals(new Double("51.01"), result.getValue());
		assertEquals("Totaal", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetPaymentRow3() throws Exception {
		String text = "PUMP 3; 38.38 I! * €1.329/l";
		PaymentRow result= RegexUtil.findNormalPaymentRow(text).iterator().next();
		assertEquals(new Double("1.32"), result.getValue());
	}
	
	@Test
	public void testDateAsPaymentRow() {
		String text = "10-10-2015";
		Set<PaymentRow> rows = RegexUtil.findNormalPaymentRow(text);
		assertTrue(rows.isEmpty());
	}
	
	@Test
	public void testGetReceiptRow2() throws Exception {
		String text = "	 MESTRO € 51,01";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		
		assertEquals(new Double("51.01"), result.getValue());
		assertEquals("MESTRO", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetReceiptRow3() throws Exception {
		String text = "	 MESTRO e 50,01";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		
		assertEquals(new Double("50.01"), result.getValue());
		assertEquals("MESTRO", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetReceiptRow4() throws Exception {
		String text = "	 MESTRO E 50,01";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		
		assertEquals(new Double("50.01"), result.getValue());
		assertEquals("MESTRO", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetReceiptRow5() throws Exception {
		String text = "	 e4.15";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		assertEquals(new Double("4.15"), result.getValue());
		assertEquals("", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetReceiptRow10() throws Exception {
		String text = "POTGROND/TURF €4.99";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		assertEquals(new Double("4.99"), result.getValue());
		assertEquals("POTGROND/TURF", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetReceiptRow11() throws Exception {
		String text = " bla € 10.056,-";
		PaymentRow result = RegexUtil.findNormalPaymentRow(text).iterator().next();
		assertEquals(new Double("10056"), result.getValue());
		assertEquals("bla", result.getName());
		assertEquals("EUR", result.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetReceiptRow1() throws Exception {
		String text = "  Diesel  E 50,01 " + System.lineSeparator() + " Totaal  E 52,01 ";
		Iterator<PaymentRow> it = RegexUtil.findNormalPaymentRow(text).iterator(); 
		PaymentRow result1 = it.next();
		PaymentRow result2 = it.next();
		assertEquals(new Double("50.01"), result1.getValue());
		assertEquals("Diesel", result1.getName());
		assertEquals("EUR", result1.getCurrency().getCurrencyCode());
		assertEquals(new Double("52.01"), result2.getValue());
		assertEquals("Totaal", result2.getName());
		assertEquals("EUR", result2.getCurrency().getCurrencyCode());
	}
	
	@Test
	public void testGetReceiptRow6() throws Exception {
		String text = "	 142424  "; //prevent normal numbers
		Set<PaymentRow> rows = RegexUtil.findNormalPaymentRow(text);
		assertTrue(rows.isEmpty());
	}
	
	@Test 
	public void testGetReceiptRow7() throws Exception {
		String text = "1"; //prevent single numbers
		Set<PaymentRow> rows = RegexUtil.findNormalPaymentRow(text);
		assertTrue(rows.isEmpty());
	}
	
	@Test
	public void testGetReceiptRow8() throws Exception {
		String text = "   d32443212321  "; //prevent numbers with char
		Set<PaymentRow> rows = RegexUtil.findNormalPaymentRow(text);
		assertTrue(rows.isEmpty());
	}
		
	@Test
	public void testGetDate() throws Exception {
		String text = "   dsda 10-10-2014   ";
		Date d = RegexUtil.getDateFromText(text);
		assertNotNull(d);
	}
	
	@Test
	public void testGetVat() throws Exception {
		String text = "   6 %   ";
		Vat vat = RegexUtil.getReceiptVat(text);
		assertEquals(Vat.SIX, vat);
	}
	
	@Test
	public void testGetVat21() throws Exception {
		String text = " dsa  21%   ds ";
		Vat vat = RegexUtil.getReceiptVat(text);
		assertEquals(Vat.TWENTYONE, vat);
	}
	
	@Test
	public void testParseDecimalValue1() throws Exception {
		String text = "E 100.00";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(100.00), value);
	}
	@Test
	public void testParseDecimalValue2() throws Exception {
		String text = "E 100.0";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(100.00), value);
	}
	@Test
	public void testParseDecimalValue3() throws Exception {
		String text = "E 100,12";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(100.12), value);
	}
	@Test
	public void testParseDecimalValue4() throws Exception {
		String text = "E 100,0";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(100.00), value);
	}
	
	@Test
	public void testParseDecimalValue5() throws Exception {
		String text = "E 100,-";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(100.00), value);
	}
	
	@Test
	public void testParseDecimalValue6() throws Exception {
		String text = "E 100.-";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(100.00), value);
	}
	
	@Test
	public void testParseDecimalValue7() throws Exception {
		String text = "E 100";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(100.00), value);
	}
	
	@Test
	public void testParseDecimalValue8() throws Exception {
		String text = "E100";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(100.00), value);
	}
	
	@Test
	public void testParseDecimalValue9() throws Exception {
		String text = "€ 10.056,-";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(10056), value);
	}
	
	@Test
	public void testParseDecimalValue10() throws Exception {
		String text = "€ 10.056";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(10056), value);
	}
	
	@Test
	public void testParseDecimalValue11() throws Exception {
		String text = "€ 10,056";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(10056), value);
	}
	
	@Test
	public void testParseDecimalValue12() throws Exception {
		String text = "€ 10,056.45";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(10056.45), value);
	}
	
	@Test
	public void testParseDecimalValue13() throws Exception {
		String text = "€51.01";
		Double value = RegexUtil.parseDecimalValue(text);
		assertEquals(new Double(51.01), value);
	}

	
}
