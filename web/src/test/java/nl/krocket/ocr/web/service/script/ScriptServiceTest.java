package nl.krocket.ocr.web.service.script;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import nl.krocket.ocr.service.OSUtil;
import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.backing.Result;
import nl.krocket.ocr.web.config.SimpleWebMvcTestConfiguration;
import nl.krocket.ocr.web.service.other.PropertyService;

@WebAppConfiguration
@ContextConfiguration(classes = { SimpleWebMvcTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ScriptServiceTest {
	
	@Autowired
	PropertyService propertyService;
	
	@Test
	public void testRunScript() {
		String scriptFileName="runscripttest.bat";
		if (OSUtil.isLinux()) {
			scriptFileName = "runscripttest.sh";
		}
		
		
		String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_PATH);

		String script = path + scriptFileName;
		
		String message = "HelloWorld!";
		ScriptService scriptRunner = new ScriptService();
		Result r = scriptRunner.runScript(script + " " + message + " " + message);
		assertEquals("", r.getError());
		assertTrue(r.getMessage().contains(message));
	}
	
	@Test
	public void testRunScriptWithSlash() {
		String scriptFileName="runscripttest.bat";
		if (OSUtil.isLinux()) {
			scriptFileName = "runscripttest.sh";
		}
		String path = propertyService.getPropertyValue(PropertyEnum.APPLICATION_SCRIPT_PATH);

		String script = path + scriptFileName;
		String message = "Hello//World//";
		ScriptService scriptRunner = new ScriptService();
		Result r = scriptRunner.runScript(script + " " + message + " " + message);
		assertEquals("", r.getError());
		assertTrue(r.getMessage().contains("Hello"));
	}

	

}
