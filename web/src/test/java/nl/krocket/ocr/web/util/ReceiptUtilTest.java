package nl.krocket.ocr.web.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.backing.ScannedWord;
import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.service.db.AddressService;
import nl.krocket.ocr.web.service.db.BaseImageService;
import nl.krocket.ocr.web.service.db.CompanyService;
import nl.krocket.ocr.web.service.db.OcrImageService;
import nl.krocket.ocr.web.service.db.OcrWordService;
import nl.krocket.ocr.web.service.db.PaymentRowService;
import nl.krocket.ocr.web.service.db.ReceiptService;
import nl.krocket.ocr.web.service.db.UserInfoService;
import nl.krocket.ocr.web.service.parse.ReceiptParserService;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ReceiptUtilTest {
	
	@Autowired
    private ReceiptService receiptService;

	@Autowired
    private PaymentRowService paymentRowService;
	
	@Autowired
	private OcrImageService ocrImageService;
	
	@Autowired
	private BaseImageService baseImageService;
	
	@Autowired
	private OcrWordService ocrWordService;
	
	@Autowired
	private ReceiptParserService receiptParserService;

	@Autowired
	private CompanyService companyService;
	@Autowired
	private AddressService addressService;
	
	private UserInfo userInfo;
	private Receipt receipt;
	private Company company;
	private Address address;
	
	@Autowired
    private UserInfoService userInfoService;
	
	@Before
	public void setup() throws Exception {
		
		company = new CompanyBuilder()
			.withAccepted(false)
			.withCompanyName("TOTAL")
			.withKvkNumber("kvk")
			.withUrl("url")
			.withVatNumber("vat")
			.build();

		company = companyService.save(company);
		
		address = new AddressBuilder()
				.withAccepted(false)
				.withCompany(company)
				.withCity("city")
				.withCountryCode("nl")
				.withHouseNr("1")
				.withPostcode("postcode")
				.withStreetName("street")
				.build();
		
		address = addressService.save(address);
		
		company.getAddressSet().add(address);
		
		company = companyService.save(company);

		userInfo = new UserInfoBuilder()
		.withCreationDate(new Date())
		.withPassword("pass")
		.withRole("role")
		.withUserName("username")
		.build();

		userInfo = userInfoService.save(userInfo);
		ScannedImage image = new ScannedImage();
		String id = "12345";
		image.setDir(id);
		image.setError("");
		image.setFileName("");
		image.sethOCRText("");
		String text = "  TOTAL  " + System.lineSeparator() + "  Diesel  E 50,01 " + System.lineSeparator() + " Totaal  E 50,01 " + System.lineSeparator() + " BTW   6% " + System.lineSeparator() + " Datum: 10-10-2015 " + System.lineSeparator() + "  ";
		image.setScannedText(text);
		List<ScannedWord> words = new ArrayList<ScannedWord>();
		words.add(new ScannedWord("Diesel", 90));
		words.add(new ScannedWord("Totaal", 90));
		image.setWords(words);
		image.setThresh(0);
		receipt = receiptParserService.getReceiptParsed(image);
		receipt.setProcesStatus(ProcesStatus.ACCEPTED);
		receipt.setUserInfo(userInfo);
		receipt = ReceiptUtil.save(receipt, receiptService, paymentRowService, ocrWordService);
		
	}
	
	@Test
	public void testSave() throws Exception {
		Long expected = 1L;
		PaymentRow r = receipt.getPaymentRows().iterator().next();
		PaymentRow row = paymentRowService.get(r.getPaymentRowId());
		System.out.println(receipt.getOcrText());
		assertEquals(row.getValue(), new Double("50.01"));
		assertEquals(expected, paymentRowService.getCount());
	}
	
	@Test
	public void testDelete() {
		Long expected = 0L;
		ReceiptUtil.delete(receipt.getBaseImageId().toString(), receiptService);
		assertEquals(expected, receiptService.getCount());
		assertEquals(expected, paymentRowService.getCount());
		assertEquals(expected, ocrWordService.getCount());
		assertEquals(expected, ocrImageService.getCount());
		assertEquals(expected, baseImageService.getCount());
		assertTrue(companyService.getCount()>0L);
	}
}
