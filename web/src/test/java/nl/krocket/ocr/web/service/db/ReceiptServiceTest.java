package nl.krocket.ocr.web.service.db;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.core.model.builders.ReceiptBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ReceiptServiceTest {
	
	
	@Autowired
    private UserInfoService userInfoService;
	@Autowired
	private ReceiptService receiptService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private AddressService addressService;
	
	private Receipt receipt;
	
	private UserInfo userInfo;
	
	@Before
	public void setup() throws Exception {
		userInfo = new UserInfoBuilder()
		.withCreationDate(new Date())
		.withPassword("098f6bcd4621d373cade4e832627b4f6")
		.withRole("role")
		.withUserName("test")
		.build();

		userInfo = userInfoService.save(userInfo);
		
		//uploaded
		receipt = new ReceiptBuilder()
		.withCreationDate(new Date())
		.withFileDirectory("test")
		.withFileName("")
		.withProcesStatus(ProcesStatus.UPLOADED)
		.withUserInfo(userInfo).build();

		receipt = receiptService.save(receipt);
		
		//scanned and parsed
		receipt = new ReceiptBuilder()
		.withCreationDate(new Date())
		.withBaseImageId(receipt.getBaseImageId())
		.withFileDirectory("test")
		.withFileName("")
		.withHOcr("")
		.withOcrText("")
		.withOcrThreshold(1)
		.withProcesStatus(ProcesStatus.ACCEPTED)
		.withReceiptCompany(getCompany())
		.withReceiptDate(new Date())
		.withUserInfo(userInfo).build();

		receipt = receiptService.save(receipt);
	}
	
	private Company getCompany() {
		Company company = new CompanyBuilder()
		.withAccepted(false)
		.withCompanyName("companyName")
		.withKvkNumber("kvk")
		.withUrl("url")
		.withVatNumber("vat")
		.build();

		company = companyService.save(company);
		
		Address address = new AddressBuilder()
				.withAccepted(false)
				.withCompany(company)
				.withCity("city")
				.withCountryCode("nl")
				.withHouseNr("1")
				.withPostcode("postcode")
				.withStreetName("street")
				.build();
		
		address = addressService.save(address);
		
		company.getAddressSet().add(address);
		
		company = companyService.save(company);
		return company;
	}
	
	@Test
	public void testBaseImage() {
		Long expected = 1L;
		assertEquals(expected, receiptService.getCount());
		assertEquals(ProcesStatus.ACCEPTED, receipt.getProcesStatus());
	}

}
