package nl.krocket.ocr.web.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.core.model.builders.ReceiptBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.service.db.AddressService;
import nl.krocket.ocr.web.service.db.CompanyService;
import nl.krocket.ocr.web.service.db.ReceiptService;
import nl.krocket.ocr.web.service.db.UserInfoService;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ReceiptDeleteControllerTest {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	
	@Autowired
    private UserInfoService userInfoService;
	@Autowired
	private ReceiptService receiptService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private AddressService addressService;
	
	private Receipt receipt;
	private UserInfo userInfo;
	
	@Before
	public void setup() throws Exception {
		
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity()) 
				.build();
		
		userInfo = new UserInfoBuilder()
				.withCreationDate(new Date())
				.withPassword("098f6bcd4621d373cade4e832627b4f6")
				.withRole("role")
				.withUserName("test")
				.build();
		
		userInfo = userInfoService.save(userInfo);

		receipt = new ReceiptBuilder()
					.withCreationDate(new Date())
					.withFileDirectory("test")
					.withFileName("")
					.withHOcr("")
					.withType("receipts")
					.withOcrText("")
					.withOcrThreshold(1)
					.withProcesStatus(ProcesStatus.ACCEPTED)
					.withReceiptCompany(getCompany())
					.withReceiptDate(new Date())
					.withUserInfo(userInfo).build();
		receipt = receiptService.save(receipt);
	}
	
	private Company getCompany() {
		Company company = new CompanyBuilder()
		.withAccepted(false)
		.withCompanyName("companyName")
		.withKvkNumber("kvk")
		.withUrl("url")
		.withVatNumber("vat")
		.build();

		company = companyService.save(company);
		
		Address address = new AddressBuilder()
				.withAccepted(false)
				.withCompany(company)
				.withCity("city")
				.withCountryCode("nl")
				.withHouseNr("1")
				.withPostcode("postcode")
				.withStreetName("street")
				.build();
		
		address = addressService.save(address);
		
		company.getAddressSet().add(address);
		
		company = companyService.save(company);
		return company;
	}
	
	@After
	public void tearDown() {
		userInfoService.delete(userInfo);
	}
	
	@Test
    public void testPostDelete() throws Exception {
		System.out.println("------------->" + receipt.getBaseImageId());
       mvc.perform(post("/delete/receipt/" + receipt.getBaseImageId())
    		    .with(httpBasic("test","test")).with(csrf()))
				.andDo(print())
				.andExpect(authenticated())
				.andExpect(status().is(200));
    }
	
}
