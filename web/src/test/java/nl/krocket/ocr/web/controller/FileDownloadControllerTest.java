package nl.krocket.ocr.web.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileInputStream;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.web.backing.Result;
import nl.krocket.ocr.web.backing.UploadTypeEnum;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.service.db.UserInfoService;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class FileDownloadControllerTest {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	UserInfo userInfo;
	@Autowired
    private UserInfoService userInfoService;
	
	@Before
	public void setup() {
		userInfo = new UserInfoBuilder()
				.withCreationDate(new Date())
				.withPassword("098f6bcd4621d373cade4e832627b4f6")
				.withRole("role")
				.withUserName("test")
				.build();
		
		userInfo = userInfoService.save(userInfo);
		
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity()) 
				.build();
	}
	
	@After
	public void tearDown() {
		userInfoService.delete(userInfo);
	}
	
	@Test
    public void testPng() throws Exception {
		doDownload("/totaal_bon.png", UploadTypeEnum.RECEIPT);
    }
	
	@Test
    public void testJpg() throws Exception {
		doDownload("/totaal_bon.jpg",  UploadTypeEnum.RECEIPT);
    }
	
	@Test
    public void testPdf() throws Exception {
		doDownload("/test.pdf",  UploadTypeEnum.INVOICE);
    }
	
	@Test
    public void testResourceNotFound() throws Exception {
		String fileName = "bla.jpg";
	      mvc.perform(MockMvcRequestBuilders.get("/get/image/bla/receipts/" + fileName)
        		.with(httpBasic("test", "test")))
        		.andDo(print())
        		.andExpect(status().is(404));
    }
	
	
	
	private void doDownload(String fileName, UploadTypeEnum uploadType) throws Exception {
		FileInputStream fis=new FileInputStream(this.getClass().getResource(fileName).getPath());
		
        MockMultipartFile firstFile = new MockMultipartFile("cam", fileName, uploadType.getSupportedMediaTypes()[0], fis);
       
        String url = "";
        if (uploadType.equals(UploadTypeEnum.RECEIPT)) {
        	url = "/snapupload";
        }
        else {
        	url = "/invoiceupload";
        }
        MvcResult result = mvc.perform(MockMvcRequestBuilders.fileUpload(url)
                        .file(firstFile).with(csrf()).with(httpBasic("test","test")))
                        .andDo(print())
                        .andExpect(status().is(200)).andReturn();
        
        String json = result.getResponse().getContentAsString();
        
        ObjectMapper mapper = new ObjectMapper();
        Result r = mapper.readValue(json, Result.class);
        
        mvc.perform(MockMvcRequestBuilders.get("/get/image/"+r.getDirId()+"/" + uploadType.toString() + "/" + fileName  )
        		.with(httpBasic("test", "test")))
        		.andDo(print())
        		.andExpect(content().contentType(MediaType.IMAGE_JPEG))
        		.andExpect(status().is(200));
	}

}
