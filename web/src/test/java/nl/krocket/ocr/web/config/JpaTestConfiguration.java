package nl.krocket.ocr.web.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.service.other.PropertyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"nl.krocket.ocr.core.repositories"})
public class JpaTestConfiguration {
	
	@Autowired
    private PropertyService propertyService;
	
	@Bean(destroyMethod = "close")
    DataSource dataSource() {
        HikariConfig dataSourceConfig = new HikariConfig();
        dataSourceConfig.setDriverClassName(propertyService.getPropertyValue(PropertyEnum.DATABASE_DRIVER));
        dataSourceConfig.setJdbcUrl(propertyService.getPropertyValue(PropertyEnum.DATABASE_URL));
        dataSourceConfig.setUsername(propertyService.getPropertyValue(PropertyEnum.DATABASE_USERNAME));
        dataSourceConfig.setPassword(propertyService.getPropertyValue(PropertyEnum.DATABASE_PASSWORD));
         return new HikariDataSource(dataSourceConfig);
    }
	
	@Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                                Environment env) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan("nl.krocket.ocr.core.model");
 
        Properties jpaProperties = new Properties();
     
        //Configures the used database dialect. This allows Hibernate to create SQL
        //that is optimized for the used database.
        jpaProperties.put("hibernate.dialect", propertyService.getPropertyValue(PropertyEnum.HIBERNATE_DIALECT));
 
        //Specifies the action that is invoked to the database when the Hibernate
        //SessionFactory is created or closed.
        jpaProperties.put("hibernate.hbm2ddl.auto",
        		propertyService.getPropertyValue(PropertyEnum.HIBERNATE_SESSION)
        );
 
        //Configures the naming strategy that is used when Hibernate creates
        //new database objects and schema elements
        jpaProperties.put("hibernate.ejb.naming_strategy",
        		propertyService.getPropertyValue(PropertyEnum.HIBERNATE_NAMING_STRATEGY)
        );
 
        //If the value of this property is true, Hibernate writes all SQL
        //statements to the console.
        jpaProperties.put("hibernate.show_sql",
        		propertyService.getPropertyValue(PropertyEnum.HIBERNATE_SHOW_SQL)
        );
 
        //If the value of this property is true, Hibernate will format the SQL
        //that is written to the console.
        jpaProperties.put("hibernate.format_sql",
        		propertyService.getPropertyValue(PropertyEnum.HIBERNATE_FORMAT_SQL)
        );
 
        entityManagerFactoryBean.setJpaProperties(jpaProperties);
 
        return entityManagerFactoryBean;
    }
	
	@Bean
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
	
	

}
