package nl.krocket.ocr.web.service.parse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import nl.krocket.ocr.core.model.Address;
import nl.krocket.ocr.core.model.Company;
import nl.krocket.ocr.core.model.builders.AddressBuilder;
import nl.krocket.ocr.core.model.builders.CompanyBuilder;
import nl.krocket.ocr.core.repositories.AddressRepository;
import nl.krocket.ocr.core.repositories.CompanyRepository;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class} )
@RunWith(SpringJUnit4ClassRunner.class)
public class CompanyParserServiceTest {
	
	@Autowired
	private CompanyParserService service;
	
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired 
	private AddressRepository addressRepository;
	
	private Company company;
	private Address address;
	
	@Before
	public void setUp() throws Exception {
		company = new CompanyBuilder()
				.withAccepted(false)
				.withCompanyName("companyName")
				.withKvkNumber("kvk")
				.withUrl("url")
				.withVatNumber("vat")
				.build();

		company = companyRepository.save(company);
		
		address = new AddressBuilder()
				.withAccepted(false)
				.withCompany(company)
				.withCity("city")
				.withCountryCode("nl")
				.withHouseNr("1")
				.withPostcode("postcode")
				.withStreetName("street")
				.build();
		
		address = addressRepository.save(address);
		
		company.getAddressSet().add(address);
		
		company = companyRepository.save(company);

	}
	
	@After
	public void tearDown() {
		companyRepository.delete(company);
	}
	
	@Test
	public void testParserIsNull() throws Exception {
		String scannedText = "";
		Company actual = service.parseForReceipt(scannedText);
		assertNull(actual);
	}
	
	@Test
	public void testParserIsNullDoesNotExist() throws Exception {
		String scannedText = "blaat";
		Company actual = service.parseForReceipt(scannedText);
		assertNull(actual);
	}
	
	@Test
	public void testParser() throws Exception  {
		String scannedText = "companyname";
		Company actual = service.parseForReceipt(scannedText);
		assertEquals(company, actual);
	}
	
	@Test
	public void testParserIgnoreCase() throws Exception {
		String scannedText = "ComPanyName";
		Company actual = service.parseForReceipt(scannedText);
		assertEquals(company, actual);
	}
	
	@Test
	public void testParserFullName() throws Exception  {
		String scannedText = "companyname";
		Company actual = service.parseForReceipt(scannedText);
		assertEquals(company, actual);
	}
	
	@Test
	public void testParserFullNameIgnoreCase() throws Exception {
		String scannedText = "ComPanYnaMe";
		Company actual = service.parseForReceipt(scannedText);
		assertEquals(company, actual);
	}
	
	@Test
	public void testOwnCompany() {
		Company c = service.getOwnCompany();
		assertEquals(c.getCompanyName(), "pinniq");
		assertTrue(c.getCompanyId() > 0);
	}

}
