package nl.krocket.ocr.web.util;

import java.util.Set;

import org.junit.Test;
import static org.junit.Assert.*;

import nl.krocket.ocr.core.model.PaymentRow;

public class ParserUtilTest {
	
	@Test
	public void testPaymentRowsReceipt1() {
		String scannedText = TextHelper.RECEIPT_1;
		Set<PaymentRow> rows = ParserUtil.findPaymentRows(scannedText);
		for (PaymentRow paymentRow : rows) {
			assertEquals(paymentRow.getValue(), new Double(50.01));
		}
	}
	
	@Test
	public void testPaymentRowsReceipt2() {
		String scannedText = TextHelper.RECEIPT_2;
		Set<PaymentRow> rows = ParserUtil.findPaymentRows(scannedText);
		for (PaymentRow paymentRow : rows) {
			assertEquals(paymentRow.getValue(), new Double(51.01));
		}
	}
	
	@Test
	public void testPaymentRowsInvoice1() {
		String scannedText = TextHelper.INVOICE_1;
		Set<PaymentRow> rows = ParserUtil.findPaymentRows(scannedText);
		assertEquals(rows.size(), 1);
	}
	
	@Test
	public void testPaymentRowsInvoice2() {
		String scannedText = TextHelper.INVOICE_2;
		Set<PaymentRow> rows = ParserUtil.findPaymentRows(scannedText);
		assertEquals(rows.size(), 2);
	}

}
