package nl.krocket.ocr.web.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.Invoice;
import nl.krocket.ocr.core.model.ProcesStatus;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.core.util.CollectionsUtil;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.service.db.AddressService;
import nl.krocket.ocr.web.service.db.CompanyService;
import nl.krocket.ocr.web.service.db.InvoiceService;
import nl.krocket.ocr.web.service.db.OcrWordService;
import nl.krocket.ocr.web.service.db.PaymentRowService;
import nl.krocket.ocr.web.service.db.UserInfoService;
import nl.krocket.ocr.web.service.parse.InvoiceParserService;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class InvoiceUtilTest {
	
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private PaymentRowService paymentRowService;
	@Autowired
	private OcrWordService ocrWordService;
	@Autowired
    private UserInfoService userInfoService;
	
	@Autowired
	private InvoiceParserService invoiceParserService;

	private UserInfo userInfo;
	private Invoice invoice;
	
	@Before
	public void setup() throws Exception {
		userInfo = new UserInfoBuilder()
			.withCreationDate(new Date())
			.withPassword("pass")
			.withRole("role")
			.withUserName("username")
			.build();

		userInfo = userInfoService.save(userInfo);
		
		ScannedImage scannedImage = new ScannedImage();
		scannedImage.setDir("12345");
		scannedImage.setError("");
		scannedImage.setFileName("");
		scannedImage.sethOCRText("");
		scannedImage.setScannedText(TextHelper.INVOICE_1);

		invoice = invoiceParserService.parse(scannedImage);
		invoice.setProcesStatus(ProcesStatus.WAITING_FOR_APPROVAL);
		invoice.setUserInfo(userInfo);
		invoice = InvoiceUtil.save(invoice, invoiceService, companyService, addressService, paymentRowService, ocrWordService);
	}
	
	@Test
	public void testSave() {
		assertTrue(invoice.getBaseImageId()>0);
	}
	
	@Test
	public void testSaveAnotherOne() throws Exception {
		ScannedImage scannedImage = new ScannedImage();
		scannedImage.setDir("12345");
		scannedImage.setError("");
		scannedImage.setFileName("");
		scannedImage.sethOCRText("");
		scannedImage.setScannedText(TextHelper.INVOICE_1);

		Invoice invoice = invoiceParserService.parse(scannedImage);
		invoice.setProcesStatus(ProcesStatus.WAITING_FOR_APPROVAL);
		invoice.setUserInfo(userInfo);
		invoice = InvoiceUtil.save(invoice, invoiceService, companyService, addressService, paymentRowService, ocrWordService);
		Long expected = 2L;
		assertEquals(expected, invoiceService.getCount());
		Long expectedCompany = 2L;
		assertEquals(expectedCompany, companyService.getCount());
	}
	
	@Test
	public void testToCompany() {
		assertTrue(invoice.getToCompany().getCompanyId()>0);
	}
	
	@Test
	public void testFromCompany() {
		assertTrue(invoice.getFromCompany().getCompanyId()>0);
	}
	
	@Test
	public void testPaymentRows() {
		assertEquals(1, CollectionsUtil.getSize(invoice.getPaymentRows()));
	}
	
	@Test
	public void testDelete() {
		Long expected = 0L;
		InvoiceUtil.delete(invoice.getBaseImageId().toString(), invoiceService);
		assertEquals(expected, invoiceService.getCount());
		assertEquals(expected, paymentRowService.getCount());
		assertEquals(expected, ocrWordService.getCount());
		Long expectedCompany = 2L;
		assertEquals(expectedCompany, companyService.getCount());
	}
}
