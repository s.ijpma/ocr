package nl.krocket.ocr.web.service.parse;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.PaymentRow;
import nl.krocket.ocr.core.model.Receipt;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.PaymentRowBuilder;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.util.TextHelper;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class} )
@RunWith(SpringJUnit4ClassRunner.class)
public class ReceiptParserServiceTest {
	
	private UserInfo userInfo;
	@Autowired
	private ReceiptParserService receiptParserService;
	
	@Before
	public void setup() {
		userInfo = new UserInfoBuilder()
				.withCreationDate(new Date())
				.withPassword("pass")
				.withRole("role")
				.withUserName("username")
				.build();
	}

	@Test
	public void testPaymentRowSetMultipleItems() {
		Set<PaymentRow> receiptRows = new HashSet<PaymentRow>();
		PaymentRow row1 = PaymentRowBuilder.paymentRow().withName("b").build();
		PaymentRow row2 = PaymentRowBuilder.paymentRow().withName("a").build();
		assertFalse(row1.equals(row2));
		receiptRows.add(row1);
		receiptRows.add(row2);
		assertTrue(receiptRows.size()==2);
	}
	
	@Test
	public void testParseSimpleLine() throws Exception {
		ScannedImage image = new ScannedImage();
		image.setDir("");
		image.setFileName("");
		image.setScannedText(TextHelper.RECEIPT_1);
		
		Receipt parsed = receiptParserService.getReceiptParsed(image);
		
		assertTrue(parsed.isReceipt());
	}
	
	@Test
	public void testParseReceipt() throws Exception {
		ScannedImage image = new ScannedImage();
		image.setDir("");
		image.setFileName("");
		image.setScannedText(TextHelper.RECEIPT_2);
		Receipt parsed = receiptParserService.getReceiptParsed(image);
		for (PaymentRow row : parsed.getPaymentRows()) {
			System.out.println(row.toString());
		}
		assertTrue(parsed.isReceipt());
	}
}
