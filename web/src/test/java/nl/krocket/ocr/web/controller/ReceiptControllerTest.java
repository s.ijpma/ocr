package nl.krocket.ocr.web.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.core.model.UserInfo;
import nl.krocket.ocr.core.model.builders.UserInfoBuilder;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;
import nl.krocket.ocr.web.service.db.UserInfoService;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ReceiptControllerTest {

	@Autowired
	private WebApplicationContext context;
	private MockMvc mvc;
	
	UserInfo userInfo;
	@Autowired
    private UserInfoService userInfoService;
	
	@Before
	public void setup() {
		userInfo = new UserInfoBuilder()
				.withCreationDate(new Date())
				.withPassword("098f6bcd4621d373cade4e832627b4f6")
				.withRole("role")
				.withUserName("test")
				.build();
		
		userInfo = userInfoService.save(userInfo);

		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity()) 
				.build();
		
	}
	
	@After
	public void tearDown() {
		userInfoService.delete(userInfo);
	}
	
	@Test
    public void testPostReceipt() throws Exception {
		ObjectMapper om = new ObjectMapper();
		ScannedImage im = new ScannedImage();
		im.setDir("dir");
		im.setFileName("fileName");
		im.sethOCRText("hoxr");
		im.setScannedText("  Total  \n Datum: 18-8-2016 \n  Diesel 10,00 \n BTW   6%  0,60 \n Totaal   10,60 ");
       mvc.perform(post("/save/baseimage/").content(om.writeValueAsString(im)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.with(httpBasic("test","test")).with(csrf()))
				.andDo(print())
				.andExpect(authenticated())
				.andExpect(status().is(200));
    }
	
	@Test
    public void testGetReceiptModel() throws Exception {
       mvc.perform(get("/get/receipt_model/0")
				.with(httpBasic("test","test")))
				.andDo(print())
				.andExpect(authenticated())
				.andExpect(status().is(200));
    }
}
