package nl.krocket.ocr.web.service.other;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import nl.krocket.ocr.web.backing.PropertyEnum;
import nl.krocket.ocr.web.config.DataAccessTestConfiguration;
import nl.krocket.ocr.web.config.JpaTestConfiguration;
import nl.krocket.ocr.web.config.SecurityConfiguration;
import nl.krocket.ocr.web.config.WebMvcTestConfiguration;
import nl.krocket.ocr.web.config.WebSocketTestConfiguration;

@WebAppConfiguration
@ContextConfiguration(classes = { WebSocketTestConfiguration.class, WebMvcTestConfiguration.class, SecurityConfiguration.class, JpaTestConfiguration.class, DataAccessTestConfiguration.class} )
@RunWith(SpringJUnit4ClassRunner.class)
public class PropertyServiceTest {
	
	@Autowired
	PropertyService propertyService;

	@Before
	public void setup() throws Exception {
		
	}
	
	@Test
	public void testPropertyServiceProp() {
		String expected = "service";
		String actual = propertyService.getPropertyValue(PropertyEnum.APPLICATION_AUTHENTICATION_TYPE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testPropertyServicePropString() {
		String expected = "MD5";
		String actual = propertyService.getPropertyValue(PropertyEnum.APPLICATION_AUTHENTICATION_SERVICE + ".encoding") ;
		assertEquals(expected, actual);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testAppConfigException() {
		String expected = "testurl";
		String actual = propertyService.getPropertyValue("does.not.exist") ;
		assertEquals(expected, actual);
	}

}
