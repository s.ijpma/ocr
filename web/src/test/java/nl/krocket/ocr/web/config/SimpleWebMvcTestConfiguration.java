package nl.krocket.ocr.web.config;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import nl.krocket.ocr.web.backing.PropertyEnum;

@Configuration
@EnableWebMvc
@EnableAsync
@EnableScheduling
@ComponentScan(basePackages = {"nl.krocket.ocr.web.service.other" })
public class SimpleWebMvcTestConfiguration extends WebMvcConfigurerAdapter {
	
	static {
		System.setProperty(PropertyEnum.PROPERTY_FILE_LOCATION.toString(), SimpleWebMvcTestConfiguration.class.getResource("/").getPath());
	}
	
	@PostConstruct
	public void init() {
		
	}
	
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/css/**").addResourceLocations("/css/");
		registry.addResourceHandler("/js/**").addResourceLocations("/js/");
		registry.addResourceHandler("/fonts/**").addResourceLocations("/fonts/");
		registry.addResourceHandler("/images/**").addResourceLocations("/images/");
	}

	@Override
	public void configurePathMatch(PathMatchConfigurer matcher) {
		// allowing dots in @PathVariables
		matcher.setUseSuffixPatternMatch(false);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
		registry.addViewController("/index").setViewName("index");
		registry.addViewController("/login");
		registry.addViewController("/logout");
		registry.addViewController("/upload");
		registry.addViewController("/cam");
		registry.addViewController("/oops");
		registry.addViewController("/navbar");
	}

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setOrder(0);
		return viewResolver;
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setDefaultEncoding("UTF-8");
		multipartResolver.setMaxUploadSize(-1);
		return multipartResolver;
	}

	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
            //configurer.enable();
    }

	@Override
	public void configureMessageConverters(
			List<HttpMessageConverter<?>> converters) {
		converters.add(byteArrayHttpMessageConverter());
		converters.add(mappingJackson2HttpMessageConverter());
	}

	@Override
	public void configureContentNegotiation(
			ContentNegotiationConfigurer configurer) {
		configurer.favorPathExtension(false).favorParameter(false)
				.ignoreAcceptHeader(false).useJaf(false)
				.mediaType("jpg", MediaType.IMAGE_JPEG)
				.mediaType("png", MediaType.IMAGE_PNG)
				.mediaType("tif", MediaType.APPLICATION_OCTET_STREAM)
				.mediaType("json", MediaType.APPLICATION_JSON);
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		// objectMapper.registerModule(new JSR310Module());
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setObjectMapper(objectMapper);
		return converter;
	}

	@Bean
	public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
		ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
		return arrayHttpMessageConverter;
	}
	
	/**
     * Sets property source is per defined by system property
     * -Dkrocket.propertyfile.location=/where/is/your/propertyfile/
     * 
     * @return
     * @throws BeanCreationException
     */
    @Bean
    public static Environment environment() {
    	String propFileLocation = System.getProperty(PropertyEnum.PROPERTY_FILE_LOCATION.toString());
        if (null == propFileLocation || propFileLocation.isEmpty()) {
        	throw new BeanCreationException("System property " + PropertyEnum.PROPERTY_FILE_LOCATION.toString() + " not set!");
        }
        ConfigurableEnvironment environment = new StandardEnvironment();
        environment.getPropertySources().addFirst(getResourcePropertySource(propFileLocation, PropertyEnum.PROPERTY_FILE_NAME.toString()));
        environment.getPropertySources().addFirst(getResourcePropertySource(propFileLocation, PropertyEnum.PROPERTY_DB_FILE_NAME.toString()));
    	return environment; 
    }
    
    public static ResourcePropertySource getResourcePropertySource(String propFileLocation, String propertyFileName) {
    	File f = new File(propFileLocation + propertyFileName);
    	if (f.exists() && f.canRead()) {
            try {
				return new ResourcePropertySource(new FileSystemResource(f));
			} catch (IOException e) {
				throw new BeanCreationException("IO Exception trying to set property source: " + propFileLocation + propertyFileName, e);
			}
       	 	
        }
        else {
        	throw new BeanCreationException("Property file " + propFileLocation + propertyFileName + " not found or no read rights!");
        }
    }

}
