package nl.krocket.ocr.hocr;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.jhocr.attribute.BBox;
import com.googlecode.jhocr.attribute.ParagraphDirection;
import com.googlecode.jhocr.element.HocrCarea;
import com.googlecode.jhocr.element.HocrDocument;
import com.googlecode.jhocr.element.HocrLine;
import com.googlecode.jhocr.element.HocrPage;
import com.googlecode.jhocr.element.HocrParagraph;
import com.googlecode.jhocr.element.HocrWord;
import com.googlecode.jhocr.util.LoggUtilException;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.Source;
import net.htmlparser.jericho.StartTag;

public class HocrScanner {
	private static final Logger logger = LoggerFactory
			.getLogger(new LoggUtilException().toString());

	private static String ATTRIBUTE_ID = "id";
	private static String ATTRIBUTE_CLASS = "class";
	private static String ATTRIBUTE_TITLE = "title";
	private static String ATTRIBUTE_DIR = "dir";
	private static String TAG_STRONG = "strong";

	private final Pattern PATTERN_IMAGE = Pattern.compile("image\\s+([^;]+)");
	private final Pattern PATTERN_BBOX = Pattern.compile("bbox(\\s+-?\\d+){4}");
	private final Pattern PATTERN_BBOX_COORDINATE = Pattern
			.compile("(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)\\s+(-?\\d+)");

	private HocrDocument document;

	public HocrScanner() {
		
	}

	public HocrDocument parse(String hocr) {
			Source source = new Source(hocr);

			List<Element> allElements = source.getAllElements("meta");

			this.document = new HocrDocument();

			for (Element elem : allElements) {
				String attrHttp = elem.getAttributeValue("http-equiv");

				if ((attrHttp != null) && (attrHttp.equals("Content-Type"))) {
					this.document.setContentType(elem
							.getAttributeValue("content"));
				}

				String attrName = elem.getAttributeValue("name");

				if ((attrName != null) && (attrName.equals("ocr-system"))) {
					this.document.setOcrSystem(elem
							.getAttributeValue("content"));
				}
			}

			StartTag pageTag = source.getNextStartTag(0, ATTRIBUTE_CLASS,
					"ocr_page", false);

			while (pageTag != null) {
				this.document.addPage(parsePageTag(pageTag));
				pageTag = source.getNextStartTag(pageTag.getEnd(),
						ATTRIBUTE_CLASS, "ocr_page", false);
			}

			return this.document;
	}

	private HocrPage parsePageTag(StartTag pageTag) {
		Element element = pageTag.getElement();

		Matcher imageMatcher = this.PATTERN_IMAGE.matcher(element
				.getAttributeValue(ATTRIBUTE_TITLE));

		if (!(imageMatcher.find()))
			;
		String id = element.getAttributeValue(ATTRIBUTE_ID);
		String image = imageMatcher.group(1);
		BBox bbox = parseAttributeBBox(element);

		HocrPage page = new HocrPage(id, bbox, image);

		List<StartTag> careaTags = element.getAllStartTagsByClass("ocr_carea");

		for (StartTag careaTag : careaTags) {
			page.addCarea(parseCareaTag(careaTag));
		}

		return page;
	}

	private HocrCarea parseCareaTag(StartTag careaTag) {
		Element element = careaTag.getElement();

		String id = element.getAttributeValue(ATTRIBUTE_ID);
		BBox bbox = parseAttributeBBox(element);

		HocrCarea carea = new HocrCarea(id, bbox);

		List<StartTag> paragraphTags = element.getAllStartTagsByClass("ocr_par");

		for (StartTag paragraphTag : paragraphTags) {
			carea.addParagraph(parseParagraphTag(paragraphTag));
		}

		return carea;
	}

	private HocrParagraph parseParagraphTag(StartTag paragraphTag) {
		Element element = paragraphTag.getElement();

		String id = element.getAttributeValue(ATTRIBUTE_ID);
		String dir = element.getAttributeValue(ATTRIBUTE_DIR);
		BBox bbox = parseAttributeBBox(element);

		HocrParagraph paragraph = new HocrParagraph(id, bbox,
				(dir != null) ? ParagraphDirection.valueOf(dir.toUpperCase())
						: ParagraphDirection.LTR);

		List<StartTag> lineTags = element.getAllStartTagsByClass("ocr_line");

		for (StartTag lineTag : lineTags) {
			paragraph.addLine(parseLineTag(lineTag));
		}

		return paragraph;
	}

	private HocrLine parseLineTag(StartTag lineTag) {
		Element element = lineTag.getElement();

		String id = element.getAttributeValue(ATTRIBUTE_ID);
		BBox bbox = parseAttributeBBox(element);

		HocrLine line = new HocrLine(id, bbox);

		List<StartTag> wordTags = element.getAllStartTagsByClass("ocrx_word");

		if ((wordTags == null) || (wordTags.isEmpty())) {
			wordTags = element.getAllStartTagsByClass("ocr_word");
		}

		for (StartTag wordTag : wordTags) {
			line.addWord(parseWordTag(wordTag));
		}

		return line;
	}

	private HocrWord parseWordTag(StartTag wordTag) {
		Element element = wordTag.getElement();

		String id = element.getAttributeValue(ATTRIBUTE_ID);
		BBox bbox = parseAttributeBBox(element);

		List<StartTag> strongTags = element.getAllStartTags(TAG_STRONG);

		boolean strong = strongTags.size() > 0;

		String text = element.getContent().getTextExtractor().toString();

		return new HocrWord(id, bbox, text, strong);
	}

	private BBox parseAttributeBBox(Element element) {
		try {
			String attributeTitleValue = element.getAttributeValue("title");

			if (attributeTitleValue == null) {
				return null;
			}

			Matcher bboxMatcher = this.PATTERN_BBOX
					.matcher(attributeTitleValue);

			if (!(bboxMatcher.find())) {
				throw new Exception();
			}

			Matcher bboxCoordinateMatcher = this.PATTERN_BBOX_COORDINATE
					.matcher(bboxMatcher.group());

			if (!(bboxCoordinateMatcher.find())) {
				throw new Exception();
			}

			return new BBox(Integer.valueOf(Integer
					.parseInt(bboxCoordinateMatcher.group(1))),
					Integer.valueOf(Integer.parseInt(bboxCoordinateMatcher
							.group(2))), Integer.valueOf(Integer
							.parseInt(bboxCoordinateMatcher.group(3))),
					Integer.valueOf(Integer.parseInt(bboxCoordinateMatcher
							.group(4))));
		} catch (Exception e) {
			logger.error("Error when performing file parser hOCR, It was not possible to parse the bbox attribute");
		}
		return null;
	}
}
