package nl.krocket.ocr.scan;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import com.drew.imaging.ImageProcessingException;

import nl.krocket.ocr.service.OSUtil;

public class ExifTest {
	
	@Test
	public void testExif() throws ImageProcessingException, IOException {
		Exif f = new Exif();
		String path = this.getClass().getResource("/bonnen/").getPath();
		if (OSUtil.isWindows()) {
			path = path.substring(1, path.length());
		}
		Path p = Paths.get(path, "bon1.jpg");
		File imageFile = p.toFile();
		f.test(imageFile);
	}
	

}
