package nl.krocket.ocr.scan;

import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import nl.krocket.img.PreProcessor;
import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.service.OSUtil;

public class ReceiptScannerTest {
	
	@Before
	public void init() {
		
	}
	
	@Test
	public void testBonTotal() throws Exception {
		String fileName = "total_real_960_1280.jpg";
		
		ScannedImage image = scan(fileName);
		System.out.println(image.getScannedText());
		assertTrue(image.getScannedText().contains("51.01"));
	}

	@Test
	public void testBonSimplePng() throws Exception {
		String fileName = "bon_simple_png.png";
		
		ScannedImage image = scan(fileName);
		System.out.println(image.getScannedText());
		assertTrue(image.getScannedText().contains("Diesel"));
	}
	
	@Test
	public void testBonSimpleJpg() throws Exception {
		String fileName = "bon_simple_jpg.jpg";
		
		ScannedImage image = scan(fileName);
		System.out.println(image.getScannedText());
		assertTrue(image.getScannedText().contains("Diesel"));
	}
	
	@Test
	public void testTotalRealCropped() throws Exception {
		String fileName = "total_cropped.png";
		
		ScannedImage image = scan(fileName);
		System.out.println(image.getScannedText());
		assertTrue(image.getScannedText().contains("51.01"));
	}


	private ScannedImage scan(String fileName) throws Exception {
		String path = this.getClass().getResource("/").getPath() + "bonnen/";
		if (OSUtil.isWindows()) {
			path = path.substring(1, path.length());
		}
		PreProcessor.preProcess(Paths.get(path, fileName).toFile());
		
		ScannedImage image = ReceiptScanner.scanReceipt(path, "", fileName +  "_cropped.jpg");
		return image;
	}
	
	

}
