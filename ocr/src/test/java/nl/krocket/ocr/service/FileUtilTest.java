package nl.krocket.ocr.service;



import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FileUtilTest {
	
	@Test
	public void testRenameFile() {
		String fileName = "test.jpg";
		String newExt = "";
		String infix = "_bla";
		String actual = FileUtil.renameFile(fileName, newExt, infix);
		assertEquals("test_bla.jpg", actual);
	}
	
	@Test
	public void testRenameFileNewExt() {
		String fileName = "test.jpg";
		String newExt = "png";
		String infix = "_bla";
		String actual = FileUtil.renameFile(fileName, newExt, infix);
		assertEquals("test_bla.png", actual);
	}
	
	@Test
	public void testRenameFileWitdDots() {
		String fileName = "invoice-CXX73-Krocket_v.o.f-2015-12-02.pdf";
		String newExt = "";
		String infix = "_bla";
		String actual = FileUtil.renameFile(fileName, newExt, infix);
		assertEquals("invoice-CXX73-Krocket_v.o.f-2015-12-02_bla.pdf", actual);
	}
	
	@Test
	public void testRenameFileWitdDotsNewExt() {
		String fileName = "invoice-CXX73-Krocket_v.o.f-2015-12-02.pdf";
		String newExt = "jpg";
		String infix = "_bla";
		String actual = FileUtil.renameFile(fileName, newExt, infix);
		assertEquals("invoice-CXX73-Krocket_v.o.f-2015-12-02_bla.jpg", actual);
	}

}
