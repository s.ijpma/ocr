package nl.krocket.img;

import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.junit.Test;

import nl.krocket.ocr.service.OSUtil;

public class PreProcessorTest {
	
	@Test
	public void testBlaat() throws Exception {
		String path = this.getClass().getResource("/bonnen").getPath();
		if (OSUtil.isWindows()) {
			path = path.substring(1, path.length());
		}
		path="/bonnen/test/";
		Path p = Paths.get(path, "ma85.jpg");
		Binarizer.process((p.toFile()));
	}
	
	@Test
	public void testOne() throws Exception {
		String path = this.getClass().getResource("/bonnen").getPath();
		if (OSUtil.isWindows()) {
			path = path.substring(1, path.length());
		}
		Path p = Paths.get(path, "bon3.jpg");
		PreProcessor.preProccess2(p.toFile());
	}
	
	@Test
	public void testAll() throws Exception{ 
		String path = this.getClass().getResource("/bonnen").getPath();
		if (OSUtil.isWindows()) {
			path = path.substring(1, path.length());
		}
		DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(path));
		Consumer<Path> action = PreProcessorTest::doPreprocess2;
		directoryStream.forEach(action);
	}
	
	public static void doPreprocess(Path dir) {
		PreProcessor.preProcess(dir.toFile());
	}
	
	public static void doPreprocess2(Path dir) {
		try {
			PreProcessor.preProccess2(dir.toFile());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
