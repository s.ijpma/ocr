package nl.krocket.img;

import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.opencv_core.*;

import java.io.File;

import org.bytedeco.javacpp.opencv_core.*;

public class PreProcessor {
	
	public static void preProcess(String input, String output) {
		File f = new File(input);
		Mat image = getImageFromFile(f);
		Mat cropSource = image.clone();
		float d = getDim(image);
		Mat morph = doMorph(image);
		Rect rect = findOuterMostRectangle(image, morph, d);
		Mat cropped = crop(cropSource, rect);
		
		imwrite(output, cropped);
	}
	
	public static void preProcess(File file) {
		Mat image = getImageFromFile(file);
		Mat cropSource = image.clone();
		float d = getDim(image);
		Mat morph = doMorph(image);
		imwrite(file.getParent() + "/" + file.getName() + "_morph.jpg", morph);
		Rect rect = findOuterMostRectangle(image, morph, d);
		Mat rectangle = image;
		imwrite(file.getParent()  + "/" + file.getName() +  "_rectangle.jpg", rectangle);
		Mat cropped = crop(cropSource, rect);
		imwrite(file.getParent()  + "/" + file.getName() +  "_cropped.jpg", cropped);
	}
	
	public static void preProccess2(File file) throws Exception {
		Mat image = getImageFromFile(file);
		Mat morph = doMorph(image.clone());
		imwrite(file.getParent() + "/" + file.getName() + "_morph.jpg", morph);
		Mat texterized = getImageWithOnlyTextContours(image, morph);
		imwrite(file.getParent()  + "/" + file.getName() +  "_texterized.jpg", texterized);
	}
	
	public static void preProccess3(File file) throws Exception {
		Mat image = getImageFromFile(file);
		Mat grey = toGreyScale(image);
		imwrite(file.getParent() + "/" + file.getName() + "_grey.jpg", grey);
		Mat texterized = thresHold(grey);
		imwrite(file.getParent()  + "/" + file.getName() +  "_texterized.jpg", texterized);
	}
	
	private static Mat crop(Mat source, Rect rect) {
		Mat cropped = new Mat(source, rect);
		return cropped;
	}
	
	public static Mat getImageFromFile(File file) {
		Mat image=null;
		if (file.exists() && file.canRead()) {
			image = imread(file.getAbsolutePath());
			if (image.empty()) { 
				System.err.println("Empty image!");
			}
		}
		else {
			System.err.println("Empty file!");
		}
		return image;
	}
	
	private static float getDim(Mat src) {
		Mat small = new Mat();
		 pyrDown(src.clone(), small);
		 return (float)src.arrayHeight() / small.arrayHeight();
	}
	
	private static float getRatioHeight(Mat original, Mat small) {
		 return (float)original.arrayHeight() / small.arrayHeight();
	}
	private static float getRatioWidth(Mat original, Mat small) {
		 return (float)original.arrayWidth() / small.arrayWidth();
	}
	
	
	public static Mat toGreyScale(Mat m) {
		Mat rgb = new Mat();
		cvtColor(m, rgb, CV_BGR2GRAY);
		return rgb;
	}
	
	public static Mat thresHold(Mat m) {
		Mat thresh = new Mat();
		//threshold(m, thresh, 160, 255, THRESH_BINARY);
		adaptiveThreshold(m, thresh, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 15, 2);
		return thresh;
	}
	
	public static Mat doMorph(Mat src) {
		Mat rgb = new Mat();
	    // downsample and use it for processing
	    pyrDown(src, rgb);

	    Mat small = new Mat();
	    cvtColor(rgb, small, CV_BGR2GRAY);
	    
	    // morphological gradient
	    Mat grad = new Mat();
	    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, new Size(3, 3));
	    morphologyEx(small, grad, MORPH_GRADIENT, morphKernel);
	    
	    // binarize
	    Mat bw = new Mat();
	    threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);
	    // connect horizontally oriented regions
	    Mat connected = new Mat();
	    morphKernel = getStructuringElement(MORPH_RECT, new Size(9, 1));
	    morphologyEx(bw, connected, MORPH_CLOSE, morphKernel);
	    return connected;
	}
	
	public static Mat doMorph2(Mat src) {

	    Mat small = new Mat();
	    cvtColor(src, small, CV_BGR2GRAY);
	    
	    // morphological gradient
	    Mat grad = new Mat();
	    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, new Size(3, 3));
	    morphologyEx(small, grad, MORPH_GRADIENT, morphKernel);
	    
	    // binarize
	    Mat bw = new Mat();
	    threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);
	    // connect horizontally oriented regions
	    Mat connected = new Mat();
	    morphKernel = getStructuringElement(MORPH_RECT, new Size(9, 1));
	    morphologyEx(bw, connected, MORPH_CLOSE, morphKernel);
	    return connected;
	}
	
		
	private static Mat getNewBlankImage(int width, int height, int type) {
		Scalar color = new Scalar(255, 255, 255, 0);
		Size size = new Size(width, height);
		Mat tempMask = new Mat(size, type, color);
		return tempMask;
	}
	
	private static Mat getImageWithOnlyTextContours(Mat original, Mat source) throws Exception {
		MatVector contours = new MatVector();

		findContours(source, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
		Mat newImage = toGreyScale(getNewBlankImage(original.arrayWidth(), original.arrayHeight(), original.type()));
		float ratioHeight = getRatioHeight(original, source);
		float ratioWidth = getRatioWidth(original, source);
		System.out.println("ratioHeight:" + ratioHeight + ", ratioWidth:" + ratioWidth);

		for (int i = 0; i < contours.size(); i++) {
			Mat contour = contours.get(i);
			Rect rect = boundingRect(contour);
			float heightPerc = (float)rect.height() / source.arrayHeight();
			float widthPerc = (float)rect.width() / source.arrayWidth();
			//TODO: get some mathematical reasoning for the percentage
			if (heightPerc>0.012 && widthPerc>0.012 && rect.height()<100) {
				int tlx = Math.round(rect.x() * ratioHeight);
				int tly = Math.round(rect.y() * ratioWidth);
				int brx = Math.round(rect.br().x() * ratioHeight);
				int bry = Math.round(rect.br().y() * ratioWidth);
				Rect originalRect = new Rect(new Point(tlx, tly), new Point(brx, bry));
				Mat m = new Mat(original, originalRect);
				imwrite("D:\\bonnen\\test\\ma" + i +".jpg", m);
				m = Binarizer.process(m); //thresHold(toGreyScale(m));
				imwrite("D:\\bonnen\\test\\mb" + i +".jpg", m);
				Range colRange = new Range(tlx, brx);
				Range rowRange = new Range(tly, bry);
				m.copyTo(newImage.rowRange(rowRange).colRange(colRange));
				m.close();
			}
		}
		return resizer(newImage, ratioWidth, ratioHeight);
	}
	
	
	private static Rect findOuterMostRectangle(Mat original, Mat source, float d) {
		MatVector contours = new MatVector();

		findContours(source, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

		Rect mostLeft = new Rect(new Point(original.arrayWidth(), 0)); //initialize @ right
		Rect mostRight = new Rect(); //initialize @ left
		Rect mostBottom = new Rect();	//initilize @ top
		Rect mostTop = new Rect(new Point(0, original.arrayHeight())); //initilize @ bottom

		for (int i = 0; i < contours.size(); i++) {
			Mat contour = contours.get(i);
			Rect rect = boundingRect(contour);
			float heightPerc = (float)rect.height() / source.arrayHeight();
			float widthPerc = (float)rect.width() / source.arrayWidth();
//			System.out.println("height: " + rect.height() + " heightPerc: " + heightPerc);
//			System.out.println("width: " + rect.width() + " widthPerc: " + widthPerc);
			if (heightPerc>0.012 && widthPerc>0.012 && rect.height()<100) {
				rectangle(original, rect.tl(), rect.br(), new Scalar(0, 0, 255, 0), 2, 8, 0);
				if (rect.tl().x() < mostLeft.tl().x()) {
					mostLeft = rect; //we have a new mostLeft
				}
				if (rect.br().x() >= mostRight.br().x()) {
					mostRight = rect; //we have a new mostRight
				}
				if (rect.br().y() > mostBottom.br().y()) {
					mostBottom = rect;
				}
				if (rect.tl().y() < mostTop.tl().y()) {
					mostTop = rect;
				}
			}
			
		}

		rectangle( original, mostLeft.tl(), mostLeft.br(), new Scalar(0, 255, 0, 0), 2, 8, 0 );
		rectangle( original, mostRight.tl(), mostRight.br(), new Scalar(0, 255, 0, 0), 2, 8, 0 );
		rectangle( original, mostBottom.tl(), mostBottom.br(), new Scalar(0, 255, 0, 0), 2, 8, 0 );
		rectangle( original, mostTop.tl(), mostTop.br(), new Scalar(0, 255, 0, 0), 2, 8, 0 );
		
		int tlx = Math.round(mostLeft.x() * d);
		int tly = Math.round(mostTop.y() * d);
		int brx = Math.round(mostRight.br().x() * d);
		int bry = Math.round(mostBottom.br().y() * d);
		Rect boss = new Rect(new Point(tlx, tly), new Point(brx, bry));
		
		rectangle( original, boss.tl(), boss.br(), new Scalar(0, 255, 0, 0), 2, 8, 0 );
		return boss;
	}

	public static Mat resizer(Mat image, float ratioWidth, float ratioHeight) {
		Mat resized = new Mat();
		Size size = new Size(Math.round(image.arrayWidth()/ratioWidth), Math.round(image.arrayHeight()/ratioHeight));
		
		resize(image, resized,  size);
		return resized;
	}
	
	
	
}
