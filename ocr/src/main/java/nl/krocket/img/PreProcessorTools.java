package nl.krocket.img;

import static org.bytedeco.javacpp.opencv_core.BORDER_CONSTANT;
import static org.bytedeco.javacpp.opencv_core.BORDER_DEFAULT;
import static org.bytedeco.javacpp.opencv_core.CV_16S;
import static org.bytedeco.javacpp.opencv_core.CV_8UC3;
import static org.bytedeco.javacpp.opencv_core.addWeighted;
import static org.bytedeco.javacpp.opencv_core.convertScaleAbs;
import static org.bytedeco.javacpp.opencv_core.copyMakeBorder;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.CHAIN_APPROX_SIMPLE;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.Canny;
import static org.bytedeco.javacpp.opencv_imgproc.GaussianBlur;
import static org.bytedeco.javacpp.opencv_imgproc.MORPH_CROSS;
import static org.bytedeco.javacpp.opencv_imgproc.RETR_EXTERNAL;
import static org.bytedeco.javacpp.opencv_imgproc.Sobel;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY_INV;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.dilate;
import static org.bytedeco.javacpp.opencv_imgproc.drawContours;
import static org.bytedeco.javacpp.opencv_imgproc.erode;
import static org.bytedeco.javacpp.opencv_imgproc.findContours;
import static org.bytedeco.javacpp.opencv_imgproc.getStructuringElement;
import static org.bytedeco.javacpp.opencv_imgproc.medianBlur;
import static org.bytedeco.javacpp.opencv_imgproc.morphologyDefaultBorderValue;
import static org.bytedeco.javacpp.opencv_imgproc.resize;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.MatVector;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.Scalar;
import org.bytedeco.javacpp.opencv_core.Size;

public class PreProcessorTools {

	
	public static Mat doDrawContours(Mat image) {
		image = resizer(image, 500); //height 500
		
		//convert the image to grayscale, blur it, and find edges
		//in the image
		Mat gray = new Mat();
		cvtColor(image, gray, COLOR_BGR2GRAY);
		Mat blur = new Mat();
		GaussianBlur(gray, blur, new Size(3,3), 0);
		
		int threshold = 12;
		Mat errode = new Mat();
		erode(blur, errode, new Mat());
		Mat edged = new Mat();
		Canny(errode, edged,  threshold, threshold * 3, 3, true);

		MatVector contours = new MatVector();
		Mat hierarchy = new Mat();
		findContours(edged, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
		//Mat black = Mat.zeros(image.rows(), image.cols(), CV_8UC3).asMat();
		
		for (int i = 0; i < contours.size(); i++) {
			drawContours( image, contours, i, new Scalar(255, 255, 255, 0), 1, 1, hierarchy, 8, new Point(-1, -1));
		}
		return image;
	}


	public static Mat doDilate(Mat image) {
		image = resizer(image, 500); //height 500
		Mat gray = new Mat();
		cvtColor(image, gray, COLOR_BGR2GRAY);
		Mat thresh = new Mat();
		threshold(gray, thresh, 150, 255, THRESH_BINARY_INV);
		Mat kernel = getStructuringElement(MORPH_CROSS, new Size(3,3));
		Mat dilate = new Mat();
		Scalar borderValue = morphologyDefaultBorderValue();
		dilate(thresh, dilate, kernel, new Point(-1, -1), 7, 2, borderValue);
		return dilate;
	}
	

	public static Mat doEdgeDetection(Mat image) {
			//load the image and compute the ratio of the old height
			//to the new height, clone it, and resize it
			//Mat ratio = image.reshape(500);
			Mat original = new Mat();
			image.copyTo(original);
			image = resizer(image, 500); //height 500
			
			//convert the image to grayscale, blur it, and find edges
			//in the image
			Mat gray = new Mat();
			cvtColor(image, gray, COLOR_BGR2GRAY);
			Mat blur = new Mat();
			GaussianBlur(gray, blur, new Size(3,3), 0);
			
			int threshold = 12;
			Mat errode = new Mat();
			erode(blur, errode, new Mat());
			Mat edged = new Mat();
			Canny(errode, edged,  threshold, threshold * 3, 3, true);
			return edged;
	}
	
	public static Mat resizer(Mat image, int height) {
		int h = image.arrayHeight();
		int w = image.arrayWidth();
		float r = (float)height / h;
		Mat resized = new Mat();
		Size size = new Size(Math.round(w*r), height);
		resize(image, resized,  size);
		return resized;
	}
	
	public static Mat doSobeling(Mat gray) {
		//edge enhancing by Sobeling
		//Gradient-X
		Mat grad_x = new Mat();
		//Gradient-Y
		Mat grad_y = new Mat();
		int ddepth = CV_16S;
		int scale = 1;
		int delta = 0;
		Sobel(gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
		
		Sobel(gray, grad_y, ddepth, 0, 1, 3, scale , delta, BORDER_DEFAULT);
		
		Mat abs_grad_x = new Mat();
		convertScaleAbs(grad_x, abs_grad_x);
		Mat abs_grad_y = new Mat();
		convertScaleAbs(grad_y, abs_grad_y);
		
		Mat sobeled = new Mat();
		addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, sobeled);
		return sobeled;
		
	}
	
	public static Mat doBinarize(Mat src) {
		Mat blur = new Mat();
		medianBlur(src, blur, 5);
		blur = doDarkerToWhite(blur);
		Mat blur2 = new Mat();
		GaussianBlur(blur, blur2, new Size(31,13), 0);
		blur2 = binarize(blur2);
		return blur2;
	}

	public static Mat sharpen(Mat src) {
		Mat dst = new Mat();
		GaussianBlur(src, dst, new Size(0, 0), 3);
		addWeighted(src, 1.5, src, -0.5, 0, dst);
		return dst;
	}
	public static Mat binarize(Mat m) {
		Scalar colorWhite = new Scalar(255, 255, 255, 0);
		Scalar colorBlack = new Scalar(0, 0, 0, 0);
		int color_offset = 230;
		int cols = m.cols();
		int rows = m.rows();
                
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Mat d = m.row(i).col(j);
				byte b = d.ptr().get();
				int bb =  b & 0xff;
				if (bb >= color_offset) {
					m.row(i).col(j).setTo(new Mat(d.size(), d.type(), colorWhite));
				}
				if (bb < color_offset) {
					m.row(i).col(j).setTo(new Mat(d.size(), d.type(), colorBlack));
				}
			}
		}
		return m;
	}
	
	public static Mat doDarkerToWhite(Mat m) {
		Scalar colorWhite = new Scalar(255, 255, 255, 0);
		int cols = m.cols();
		int rows = m.rows();
               
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Mat d = m.row(i).col(j);
				byte b = d.ptr().get();
				int bb =  b & 0xff;
				if (bb < 50) {
					m.row(i).col(j).setTo(new Mat(d.size(), d.type(), colorWhite));
				}
			}
		}
		return m;
	}

	public static Mat doBorder(Mat src) {
		int offset=30;
		Scalar color = new Scalar(255,255,255,0);
		Mat borderized = new Mat();
		copyMakeBorder(src,borderized,offset,offset,offset,offset,BORDER_CONSTANT,color);
		return borderized;
	}
}
