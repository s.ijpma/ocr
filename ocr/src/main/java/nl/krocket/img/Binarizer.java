package nl.krocket.img;

import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import java.io.File;

import org.bytedeco.javacpp.opencv_core.Mat;

/**
 * Binerizer of image containing text. Translated from:
 * https://code.google.com/p/python-tesseract/wiki/CodeSnippets
 * Works best for smaller sized image.
 * 
 * @author Sander
 *
 */
public class Binarizer {
	
	public static void process(File file) {
		Mat image = PreProcessor.getImageFromFile(file);
		Mat sharpen = PreProcessorTools.sharpen(image);
		imwrite(file.getParent() + "/" + file.getName() + "_sharpen.jpg", sharpen);
		Mat gray = PreProcessor.toGreyScale(sharpen);
		
		Mat sobeled = PreProcessorTools.doSobeling(gray);
		
		
		imwrite(file.getParent() + "/" + file.getName() + "_morph.jpg", sobeled);
		Mat binarized = PreProcessorTools.doBinarize(sobeled);
		
	
		imwrite(file.getParent() + "/" + file.getName() + "_binarized.jpg", binarized);
	}
	
	public static Mat process(Mat src) {
		Mat gray = PreProcessor.toGreyScale(src);
		Mat sobeled = PreProcessorTools.doSobeling(gray);
		Mat binarized = PreProcessorTools.doBinarize(sobeled);
		return binarized;
	}
	
	public static Mat process2(Mat src) {
		Mat sobeled = PreProcessor.doMorph2(src);
		Mat binarized = PreProcessorTools.doBinarize(sobeled);
		return binarized;
	}

}
