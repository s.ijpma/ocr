package nl.krocket.ocr.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ScannedImage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String fileName;
	private String dir;
	private String scannedText;
	private String hOCRText;
	private List<ScannedWord> words;
	private int thresh;
	
	private String error;
	
	public int getThresh() {
		return thresh;
	}
	public void setThresh(int thresh) {
		this.thresh = thresh;
	}
	public List<ScannedWord> getWords() {
		if (this.words==null) {
			this.words = new ArrayList<ScannedWord>();
		}
		return words;
	}
	public void setWords(List<ScannedWord> words) {
		this.words = words;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getScannedText() {
		return scannedText;
	}
	public void setScannedText(String scannedText) {
		this.scannedText = scannedText;
	}
	public String gethOCRText() {
		return hOCRText;
	}
	public void sethOCRText(String hOCRText) {
		this.hOCRText = hOCRText;
	}
	public String getDir() {
		return dir;
	}
	public void setDir(String dir) {
		this.dir = dir;
	}

	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	@Override
	public String toString() {
		return "ScannedImage [fileName=" + fileName + ", dir=" + dir
				+ ", scannedText=" + scannedText + ", hOCRText=" + hOCRText
				+ "]";
	}
	
	
	
}
