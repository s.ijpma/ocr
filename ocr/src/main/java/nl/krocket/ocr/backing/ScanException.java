package nl.krocket.ocr.backing;

public class ScanException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1446020057865519192L;

	public ScanException(String message) {
		super(message);
	}
}
