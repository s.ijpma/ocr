package nl.krocket.ocr.backing;

public class ScannedWord {

	private String word;
	private float confidence;
	public ScannedWord() {
		
	}
	
	public ScannedWord(String word, float f) {
		this.word = word;
		this.confidence = f;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public float getConfidence() {
		return confidence;
	}
	public void setConfidence(float confidence) {
		this.confidence = confidence;
	}
	
}
