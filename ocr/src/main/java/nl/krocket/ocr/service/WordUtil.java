package nl.krocket.ocr.service;

import java.util.List;

import nl.krocket.ocr.backing.ScannedWord;

public class WordUtil {
	
	public static float getOverallConfidence(List<ScannedWord> words) {
		int wordCount = words.size();
		float conf = 0;
		for (ScannedWord scannedWords : words) {
			conf += scannedWords.getConfidence();
		}
		return conf/wordCount;
	}


}
