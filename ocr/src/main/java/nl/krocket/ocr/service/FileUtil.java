package nl.krocket.ocr.service;


public class FileUtil {
	
	/**
	 * Preventing instantiation
	 */
	private FileUtil() {
		
	}
	
	public static String renameFile(String fileName, String newExt, String infix) {
		if (newExt.isEmpty()) {
			newExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		}
		return fileName.substring(0, fileName.lastIndexOf(".")) + infix + "." + newExt;
 	}

}
