package nl.krocket.ocr.scan;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;
import static org.bytedeco.javacpp.tesseract.RIL_WORD;

import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.lept.PIX;
import org.bytedeco.javacpp.tesseract.ResultIterator;
import org.bytedeco.javacpp.tesseract.STRING;
import org.bytedeco.javacpp.tesseract.StringGenericVector;
import org.bytedeco.javacpp.tesseract.TessBaseAPI;

import nl.krocket.ocr.backing.ScanException;
import nl.krocket.ocr.backing.ScannedWord;

public class OcrScanner {
	
	private static final String TESS_DATA = ResourceBundle.getBundle("application", Locale.getDefault()).getString("ocr.config.tessdata.path");
	
	private TessBaseAPI api;
	private BytePointer outText;
	private PIX image;
	private List<ScannedWord> words;

	/**
	 * Reads input file and ocr's. Write's threshhold file as outputfile
	 * @param inputFile
	 * @param outputFile
	 */
	public void doOcr(String inputFile, String outputFile) {
        // Open input image with leptonica library
        this.image = pixRead(inputFile);
        this.api.SetImage(image);
        
        // Get OCR result
        outText = this.api.GetUTF8Text();

        lept.PIX pix = api.GetThresholdedImage();
        
        lept.pixWrite(outputFile, pix, lept.IFF_JFIF_JPEG);
        
        this.words = new ArrayList<ScannedWord>();
        ResultIterator it = this.api.GetIterator();
        
        it.Begin();
        float f = it.Confidence(RIL_WORD);
    	String word = it.GetUTF8Text(RIL_WORD).getString();
    	words.add(new ScannedWord(word, f));
        while (it.Next(RIL_WORD)){
        	words.add(new ScannedWord(word, f));
        	f = it.Confidence(RIL_WORD);
        	word = it.GetUTF8Text(RIL_WORD).getString();
        }
        words.add(new ScannedWord(word, f));

	}
	
	public String getOcrText() {
		return this.outText.getString();
	}

	public String getHocrText() {
		return api.GetHOCRText(3).getString();
	}

	public List<ScannedWord> getWords() {
		return words;
	}

	public void init(int psm) throws ScanException {
		this.api = new TessBaseAPI();
		
		StringGenericVector pars = new StringGenericVector();
	        pars.addPut(new STRING("load_system_dawg"));
	        pars.addPut(new STRING("load_freq_dawg"));
        StringGenericVector parsValues = new StringGenericVector();
	        parsValues.addPut(new STRING("0"));
	        parsValues.addPut(new STRING("0"));
		
		if (this.api.Init(Paths.get(TESS_DATA).toString(), "eng+equ", 
                0, (ByteBuffer)null, 0, pars, parsValues, false) != 0) {
	    	throw new ScanException("Could not initialize tesseract on tessdata path: " + TESS_DATA);
	    }
		this.api.SetVariable("tessedit_write_images", "True");
		this.api.SetVariable("tessedit_char_whitelist", "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz%+,-.€*");
		this.api.SetPageSegMode(psm);
	}
	
	public void destroy() {
		// Destroy used object and release memory
	    this.api.End();
	   	this.outText.deallocate();
	    pixDestroy(this.image);
	}
	
	

}
