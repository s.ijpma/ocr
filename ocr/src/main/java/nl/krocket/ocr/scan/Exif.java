package nl.krocket.ocr.scan;

import java.io.File;
import java.io.IOException;
import java.util.StringJoiner;
import java.util.function.Consumer;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
/**
 * See https://drewnoakes.com/code/exif/
 * @author Sander
 *
 */
public class Exif {
	
	
	public void test(File imageFile) throws ImageProcessingException, IOException {
		Metadata metadata = ImageMetadataReader.readMetadata(imageFile);
		Consumer<Directory> action = Exif::doPrint;
		metadata.getDirectories().forEach(action);
	}
	
	public static void doPrint(Directory dir) {
		Consumer<Tag> action = Exif::doPrint;
		dir.getTags().forEach(action);
		
	}
	public static void doPrint(Tag t) {
		StringJoiner sj = new StringJoiner(";", "[", "]");
		sj.add("dir_name: " + t.getDirectoryName());
		sj.add("tag name: " + t.getTagName());
		sj.add("description: " + t.getDescription());
		sj.add("type: " + t.getTagType()+"");
		System.out.println(sj.toString());
	}

}
