package nl.krocket.ocr.scan;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import nl.krocket.ocr.backing.ScannedImage;
import nl.krocket.ocr.service.FileUtil;

public class ReceiptScanner {
	
	private final static int Orientation_and_script_detection_only=0;
	private final static int Automatic_page_segmentation_with_OSD=1;
	private final static int Automatic_page_segmentation_but_no_OSD_or_OCR=2;
	private final static int Fully_automatic_page_segmentation_but_no_OSD_Default=3;
	private final static int Assume_a_single_column_of_text_of_variable_sizes=4;
	private final static int Assume_a_single_uniform_block_of_vertically_aligned_text=5;
	private final static int Assume_a_single_uniform_block_of_text=6;
	private final static int Treat_the_image_as_a_single_text_line=7;
	private final static int Treat_the_image_as_a_single_word=8;
	private final static int Treat_the_image_as_a_single_word_in_a_circle=9;
	private final static int Treat_the_image_as_a_single_character=10;
	
	public static ScannedImage scanReceipt(String path, String dir, String fileName) throws Exception {
		OcrScanner ocrScanner = new OcrScanner();
		//see http://tesseract-ocr.googlecode.com/svn-history/trunk/doc/tesseract.1.html#_config_files_and_augmenting_with_user_data
		ocrScanner.init(Assume_a_single_uniform_block_of_text);
		ScannedImage scannedImage = scan(ocrScanner, path, dir, fileName);
		ocrScanner.destroy();
		return scannedImage;
	}
	
	public static ScannedImage scanPartOfReceipt(String path, String dir, String fileName) throws Exception {
		OcrScanner ocrScanner = new OcrScanner();
		ocrScanner.init(Treat_the_image_as_a_single_text_line);
		ScannedImage scannedImage = scan(ocrScanner, path, dir, fileName);
		ocrScanner.destroy();
		return scannedImage;
	}
	
	public static ScannedImage scanInvoice(String path, String dir, String fileName) throws Exception {
		OcrScanner ocrScanner = new OcrScanner();
		//see http://tesseract-ocr.googlecode.com/svn-history/trunk/doc/tesseract.1.html#_config_files_and_augmenting_with_user_data
		ocrScanner.init(Assume_a_single_uniform_block_of_text);
		ScannedImage scannedImage = scan(ocrScanner, path, dir, fileName);
		ocrScanner.destroy();
		return scannedImage;		
	}
	
	public static ScannedImage scan(OcrScanner ocrScanner, String path, String dir, String fileName) throws Exception {
		ScannedImage scannedImage = null;
		Path sourcePath = Paths.get(path, dir);
		Path sourceFile = Paths.get(sourcePath + File.separator + fileName);
		
		scannedImage = new ScannedImage();
		
		String threshHoldFileName = FileUtil.renameFile(fileName, "jpg", "_thresh");
		Path threshHoldFilePath = Paths.get(sourcePath + File.separator + threshHoldFileName);
		
		ocrScanner.doOcr(sourceFile.toString(), threshHoldFilePath.toString());
		
        scannedImage.setFileName(fileName);
        //do not set dir here!
		scannedImage.setScannedText(ocrScanner.getOcrText());
        scannedImage.sethOCRText(ocrScanner.getHocrText());
        scannedImage.setWords(ocrScanner.getWords());
        scannedImage.setThresh(0);
		
		return scannedImage;
	}

}
