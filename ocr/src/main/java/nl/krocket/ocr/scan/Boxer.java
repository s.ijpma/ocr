package nl.krocket.ocr.scan;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;

import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.lept.PIX;
import org.bytedeco.javacpp.tesseract.TessBaseAPI;

import nl.krocket.ocr.backing.ScanException;

public class Boxer {

	private static final String TESS_DATA = ResourceBundle.getBundle("application", Locale.getDefault()).getString("ocr.config.tessdata.path");
	
	public static void doBoxing(String inputFile, String outputFile) {
		TessBaseAPI api = new TessBaseAPI();
		if (api.Init(Paths.get(TESS_DATA).toString(), null) != 0) {
	    	throw new ScanException("Could not initialize tesseract on tessdata path: " + TESS_DATA);
	    }
		PIX image = pixRead(inputFile);
        api.SetImage(image);
        BytePointer p = api.GetBoxText(0);
        lept.PIX pix = new PIX();
        lept.pixWrite(p, pix, lept.BOXA_VERSION_NUMBER);
        lept.pixWrite(outputFile, pix, lept.IFF_JFIF_JPEG);
        api.End();
        pixDestroy(image);
        pixDestroy(pix);
	}
}
